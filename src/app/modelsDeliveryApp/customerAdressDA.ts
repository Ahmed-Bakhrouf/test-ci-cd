import { CustomerDA } from "./customerDA";
import { Country } from "./country";

export class CustomerAdressDA {
  _id: string;
  wording: string;
  customer: CustomerDA | string;
  line1: string;
  line2: string;
  postalCode: string;
  town: string;
  country: Country;
  latt: number;
  long: number;
  status: string;
  status_date: string;

  constructor(adress: any) {
    this._id = adress._id;
    this.wording = adress.wording;
    this.customer = adress.customer;
    this.line1 = adress.line1;
    this.line2 = adress.line2;
    this.postalCode = adress.postalCode;
    this.town = adress.town;
    this.country = adress.country;
    this.latt = adress.latt;
    this.long = adress.long;
    this.status = adress.status;
    this.status_date = adress.status_date;
  }
}
