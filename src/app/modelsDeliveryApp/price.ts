export class Price{
  $numberDecimal: number;

  constructor(price: Price = <Price>{}) {
    if(price.$numberDecimal){
      this.$numberDecimal= price.$numberDecimal;
    }
    else {
    this.$numberDecimal= 0;
    }
  }
}
