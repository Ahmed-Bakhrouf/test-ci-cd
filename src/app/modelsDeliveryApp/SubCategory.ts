export class SubCategory {

  _id:string
  title: string;
  category: string;
  metering: string;
  constructor(category: any = {}) {
    this._id = category._id;
    this.title = category.title;
    this.category = category.subCategory;
    this.metering = category.metering;

  }

}
