import { SubCategory } from "./SubCategory";
import { CategoryDA } from "./categoryDA";
import { DatePipe } from "@angular/common";
import { MerchantDA } from "./merchantDA";

export class ProductDA {
  _id: string;
  reference: string;
  title: string;
  category: CategoryDA;
  subCategory: SubCategory;
  product: string;
  metering: string;
  unit: string;
  unitValue: number;
  description: string;
  price?: number;
  currency: string;
  stock: number;
  stock_status: string;
  brand: string;
  _model: string;
  image: string;
  merchant: string | MerchantDA;
  availability_date: Date;

  constructor(product: any = {} as ProductDA) {
    this._id = product._id;
    this.reference = product.reference;
    this.title = product.title;
    this.category = product.category;
    this.subCategory = product.subCategory;
    this.product = product.product;
    this.metering = product.metering;
    this.unit = product.unit;
    this.unitValue = product.unitValue;
    this.description = product.description;
    this.currency = product.currency;
    this.stock = product.stock;
    this.stock_status = product.stock_status;
    this.brand = product.brand;
    this._model = product._model;
    this.image = product.image;
    this.merchant = product.merchant;
    this.availability_date = new Date(product.availability_date.toString());

    if (product.price && product.price.toString() !== "NaN") {
      this.price = product.price;
    } else {
      this.price = 100;
    }
  }
}
