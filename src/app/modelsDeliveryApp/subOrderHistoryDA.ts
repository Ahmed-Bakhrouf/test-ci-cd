import { SubOrderDA } from "./subOrderDA";

export class SubOrderHistoryDA {
  subOrder: SubOrderDA | string;
  creationDate: Date;
  confirmationDate: Date;
  cancellationDate: Date;
  preparationDate: Date;
  readyDate: Date;
  deliveryDate: Date;
  pickupDate: Date;
  deliveryAcceptanceDate: Date;
  rejectionDate: Date;

  constructor(subOrder: SubOrderHistoryDA = <SubOrderHistoryDA>{}) {
    this.subOrder = subOrder.subOrder;
    this.creationDate = subOrder.creationDate;
    this.confirmationDate = subOrder.confirmationDate;
    this.cancellationDate = subOrder.cancellationDate;
    this.preparationDate = subOrder.preparationDate;
    this.readyDate = subOrder.readyDate;
    this.deliveryDate = subOrder.deliveryDate;
    this.pickupDate = subOrder.pickupDate;
    this.deliveryAcceptanceDate = subOrder.deliveryAcceptanceDate;
    this.rejectionDate = subOrder.rejectionDate;
  }
}
