export class PaymentParamsEtape1 {
    etape = 1;
    email:string;
    pass: string;
    email_a_crediter: string;
    montant: number;
    libelle_devise_montant ='EUR';
    libelle:string;
    code_operation: string;

    constructor(params: PaymentParamsEtape1 = <PaymentParamsEtape1>{}) {
      this.etape = 1;
      this.email = params.email;
      this.montant = params.montant;
      this.pass = params.pass;
      this.email_a_crediter = params.email_a_crediter;
      this.libelle = params.libelle;
      this.code_operation = params.code_operation;
      this.libelle_devise_montant = 'EUR';
    }
  }

