export class CategoryDA {

  _id:	string;
  title:	string;
  subCategory:	string;
  metering:	string;
  unit:	string;
  constructor(category: CategoryDA = <CategoryDA>{}) {
    this._id = category._id;
    this.title = category.title;
    this.subCategory = category.subCategory;
    this.metering = category.metering;
    this.unit = category.unit;

}

}
