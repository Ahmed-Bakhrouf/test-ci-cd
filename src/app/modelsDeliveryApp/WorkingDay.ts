export class WorkingDay {
  workingPeriods: [];
  day: string;
  constructor(workingDay: any = {} as WorkingDay) {
    this.workingPeriods = workingDay.workingPeriods;
    this.day = workingDay.day;
  }
  }
