import {CustomerAdressDA} from './customerAdressDA';

export class CustomerDA {

  _id: string;
  reference: string;
  firstname: string;
  lastname: string;
  status: string;
  mobile_number: number;
  email: string;
  password: string;
  address?: CustomerAdressDA[];


  constructor(user: any) {
    this._id = user._id;
    this.reference = user.reference;
    this.firstname = user.firstname;
    this.lastname = user.lastname;
    this.status = user.status;
    this.mobile_number = user.mobile_number;
    this.email = user.email;
    this.password = user.password;
    this.address = user.address;
  }
}
