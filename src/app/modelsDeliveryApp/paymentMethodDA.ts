import { Country } from "./country";

export class PaymentMethodDA {
  _id: string;
  title?: string;
  country?: Country | string;
  status?: string;

  constructor(paymentMethod: PaymentMethodDA = {} as PaymentMethodDA) {
    this._id = paymentMethod._id;
    this.title = paymentMethod.title;
    this.country = paymentMethod.country;
    this.status = paymentMethod.status;
  }
}
