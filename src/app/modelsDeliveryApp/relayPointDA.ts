export class RelayPointDA {
  _id: string;
  reference: string;
  name: string;
  adresse:string;
  lat: number;
  long: number
  workingDays: any;
  
  constructor(relayPoint: RelayPointDA = {} as RelayPointDA) {

    this._id = relayPoint._id;
    this.reference = relayPoint.reference;
    this.name = relayPoint.name;
    this.lat = relayPoint.lat;
    this.long = relayPoint.long;
    this.adresse = relayPoint.adresse;
    this.workingDays = relayPoint.workingDays;
  }
}
