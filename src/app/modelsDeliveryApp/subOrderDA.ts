import { CustomerDA } from "./customerDA";
import { ProductQuantity } from "../dto";
import { OrderDA } from "./orderDA";
// import {OrderProductDA} from "./orderProductDA";

export class SubOrderDA {
  reference: string;
  merchant: string;
  assigned_to: string; // Deliveryman;
  pickup_adress: string;
  pickup_latt: string;
  pickup_long: string;
  status: string;
  last_status: Date;
  payment: string; // Payment;
  total_product_price: number;
  total_delivery_price: number;
  total_sub_order_price: number;
  discount_percentage?: number;
  discount_amount?: number;
  total_price: number;
  comments: string[];
  origin: string;
  order?: OrderDA;
  subOrderProduct: ProductQuantity[];

  constructor(subOrder: SubOrderDA = <SubOrderDA>{}) {
    this.reference = subOrder.reference;
    this.merchant = subOrder.merchant;
    this.assigned_to = subOrder.assigned_to;
    this.pickup_adress = subOrder.pickup_adress;
    this.pickup_latt = subOrder.pickup_latt;
    this.pickup_long = subOrder.pickup_long;
    this.status = subOrder.status;
    this.last_status = subOrder.last_status;
    this.payment = subOrder.payment;
    this.total_product_price = subOrder.total_product_price;
    this.total_delivery_price = subOrder.total_delivery_price;
    this.total_sub_order_price = subOrder.total_sub_order_price;
    this.discount_percentage = subOrder.discount_percentage;
    this.discount_amount = subOrder.discount_amount;
    this.total_price = subOrder.total_price;
    this.comments = subOrder.comments;
    this.origin = subOrder.origin;
    this.order = subOrder.order;
    this.subOrderProduct = subOrder.subOrderProduct;
  }
}
