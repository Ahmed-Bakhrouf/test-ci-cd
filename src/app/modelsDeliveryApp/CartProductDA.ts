import { ProductDA } from "./productDA";
import { CategoryDA } from "./categoryDA";
import { SubCategory } from "./SubCategory";
import { MerchantDA } from "./merchantDA";

export class CartProductDA {
  _id: string;
  reference: string;
  title: string;
  category: CategoryDA;
  subCategory: SubCategory;
  product: string;
  metering: string;
  unit: string;
  unitValue: number;
  description: string;
  price?: number;
  currency: string;
  stock: number;
  stock_status: string;
  brand: string;
  _model: string;
  image: string;
  Quantity: number;
  merchant: string | MerchantDA;
  availability_date: Date;
  constructor(product: ProductDA = {} as CartProductDA) {
    this._id = product._id;
    this.reference = product.reference;
    this.title = product.title;
    this.category = product.category;
    this.subCategory = product.subCategory;
    this.product = product.product;
    this.metering = product.metering;
    this.unit = product.unit;
    this.unitValue = product.unitValue;
    this.description = product.description;
    this.currency = product.currency;
    this.stock = product.stock;
    this.stock_status = product.stock_status;
    this.brand = product.brand;
    this._model = product._model;
    this.image = product.image;
    this.price = product.price;
    this.merchant = product.merchant;
    this.availability_date = product.availability_date;
  }
}
