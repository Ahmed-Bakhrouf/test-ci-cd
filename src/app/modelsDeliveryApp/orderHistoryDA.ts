import {OrderDA} from "./orderDA";

export class OrderHistoryDA {
  _id: string;
  order: OrderDA;
  creationDate: string;
  confirmationDate: string;
  preparationDate: string;
  deliveryDate: string;
  pickupDate: string;
  deliveryAcceptanceDate: string;
}
