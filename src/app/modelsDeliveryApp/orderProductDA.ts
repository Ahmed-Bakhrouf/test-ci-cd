import {OrderDA} from './orderDA';
import {ProductDA} from './productDA';

export class OrderProductDA {
  _id?:	string;
  order?:	OrderDA;
  product?: ProductDA;
  quantity?:	number;
  productTitle?:	string;
  constructor(orderProduct: OrderProductDA  = {}) {
    this._id = orderProduct._id;
    this.order = orderProduct.order;
    this.product = orderProduct.product;
    this.quantity = orderProduct.quantity;
    this.productTitle = orderProduct.productTitle;
  }
}
