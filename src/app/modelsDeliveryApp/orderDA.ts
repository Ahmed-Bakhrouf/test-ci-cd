import { CustomerDA } from "./customerDA";
import { SubOrderDA } from "./subOrderDA";
import { DeliveryMethodDA } from "./deliveryMethodDA";

export class OrderDA {
  _id: string;
  reference: string;
  currency_code: string;
  // a verifier
  customer: CustomerDA | string;
  payment_method: string;
  payment_mean: string;
  total_product_price: number;
  total_delivery_price: number;
  total_order_price: number;
  discount_percentage: number;
  discount_amount: number;
  total_price: number;
  status: string;
  status_date: string | Date;
  delivery_adress: string;
  delivery_latt: number;
  delivery_long: number;
  origin: string;
  sub_order: SubOrderDA[];
  delivery_mode: DeliveryMethodDA | string;

  constructor(order: OrderDA = <OrderDA>{}) {
    this._id = order._id;
    this.reference = order.reference;
    this.currency_code = order.currency_code;
    this.customer = order.customer;
    this.payment_method = order.payment_method;
    this.payment_mean = order.payment_mean;
    this.total_product_price = order.total_product_price;
    this.total_delivery_price = order.total_delivery_price;
    this.total_order_price = order.total_order_price;
    this.discount_percentage = order.discount_percentage;
    this.discount_amount = order.discount_amount;
    this.total_price = order.total_price;
    this.status = order.status;
    this.status_date = order.status_date;
    this.delivery_adress = order.delivery_adress;
    this.delivery_latt = order.delivery_latt;
    this.delivery_long = order.delivery_long;
    this.origin = order.origin;
    this.sub_order = order.sub_order;
    this.delivery_mode = order.delivery_mode;
  }
}
