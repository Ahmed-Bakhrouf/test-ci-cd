export class AdminDA {

  _id:	string;
  email:	string;
  firstname:	string;
  lastname:	string;

  constructor(admin: AdminDA = <AdminDA>{}) {
    this._id = admin._id;
    this.email = admin.email;
    this.firstname = admin.firstname;
    this.lastname = admin.lastname;

  }

}
