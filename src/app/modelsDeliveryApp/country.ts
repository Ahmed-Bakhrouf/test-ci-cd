export class Country {
  _id : string;
  name : string;
  coutryCodeAlpha2 : string;
  mobileCountryCode : number;
  currencyCode : string;
  currencyFormat : string;
  constructor(country: any) {

    this._id = country._id;
    this.name = country.name;
    this.coutryCodeAlpha2 = country.coutryCodeAlpha2;
    this.mobileCountryCode = country.mobileCountryCode;
    this.currencyCode = country.currencyCode;
    this.currencyFormat = country.currencyFormat;
  }
}
