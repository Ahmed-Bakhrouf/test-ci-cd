export class SearchResultDto {
  type: ResultTypeEnum;

  title: string;

  _id: string;
}

enum ResultTypeEnum {
  MERCHANT,
  PRODUCT,
}
