export interface SubOrderProductModel {
  _id?: string;
  order?: any;
  subOrder?: any;
  product: any;
  quantity: number;
  productTitle: string;
  productPrice: number;
}
