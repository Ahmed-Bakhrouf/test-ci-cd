import { SubOrderProductModel } from "./subOrderProduct.model";

export interface SubOrderModel {
  _id?: string;
  reference: string;
  merchant: any; //todo
  assigned_to: any; // todo
  pickup_adress: string;
  pickup_latt?: string;
  pickup_long?: string;
  status: string;
  last_status: Date;
  total_product_price: number;
  total_delivery_price: number;
  total_sub_order_price: number;
  discount_percentage?: number;
  discount_amount?: number;
  total_price: number;
  comments: string[];
  origin: string;
  order?: any;
  subOrderProduct: SubOrderProductModel[];
}
