import { SubOrderModel } from "./subOrder.model";

export interface OrderModel {
  _id?: string;
  reference: string;
  currency_code?: string;
  customer: any;
  country?: any; //new
  payment_method?: any;
  payment_mean?: string;
  total_product_price: number;
  total_delivery_price: number;
  total_order_price: number;
  discount_percentage?: number;
  discount_amount?: number;
  total_price: number;
  status: string;
  status_date: Date;
  delivery_adress: string;
  delivery_latt?: number;
  delivery_long?: number;
  comments?: string[]; //new
  origin: string;
  sub_order: SubOrderModel[];
}
