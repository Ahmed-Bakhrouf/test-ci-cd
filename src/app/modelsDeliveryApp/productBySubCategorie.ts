import {ProductDA} from './productDA';

export class ProductBySubCategorie {
  subCategoryTitle? = 'all';
  subCategoryId?: string;
  subCategoryProducts?: ProductDA[];

  constructor(productBySubCategorie?: any) {
    if (productBySubCategorie.subCategoryTitle) {
      this.subCategoryTitle = productBySubCategorie.subCategoryTitle;
    } else {
      this.subCategoryTitle = 'all';
    }
    if (productBySubCategorie.subCategoryProducts) {
      this.subCategoryProducts = productBySubCategorie.subCategoryProducts;
    } else {
      this.subCategoryProducts = [];
    }
    if (productBySubCategorie.subCategoryId) {
      this.subCategoryProducts = productBySubCategorie.subCategoryId;
    }

  }
}
