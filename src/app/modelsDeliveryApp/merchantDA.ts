import { WorkingDay } from "./WorkingDay";

export class MerchantDA {
  _id: string;
  merchant: MerchantDA | string;
  reference: string;
  trade_name: string;
  first_Name: string;
  last_Name: string;
  status: string;
  mobile_number;
  country_code: string;
  Integer;
  email_Address: string;
  password: string;
  address: any[];
  logo: string;
  workingDays: WorkingDay[];

  constructor(merchand: MerchantDA = <MerchantDA>{}) {
    this._id = merchand._id;
    this.reference = merchand.reference;
    this.trade_name = merchand.trade_name;
    this.first_Name = merchand.first_Name;
    this.last_Name = merchand.last_Name;
    this.status = merchand.status;
    this.mobile_number = merchand.mobile_number;
    this.country_code = merchand.country_code;
    this.email_Address = merchand.email_Address;
    this.password = merchand.password;
    this.address = merchand.address;
    this.logo = merchand.logo;
    this.workingDays = merchand.workingDays;
  }
}
