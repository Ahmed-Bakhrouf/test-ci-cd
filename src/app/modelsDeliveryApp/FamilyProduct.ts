import {CategoryDA} from './categoryDA';

export class FamilyProduct {
  name: string;
  categories: CategoryDA[];
  image: string;

  constructor(familyProduct: FamilyProduct =  < FamilyProduct> {}) {
    this.name = familyProduct.name;
    this.categories = familyProduct.categories;
    this.image = familyProduct.image;
  }
}
