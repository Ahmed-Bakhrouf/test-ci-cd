export class DeliveryMethodDA {
  _id: string;
  reference: string;
  name: string;
  code:string;
  price?: number;
  
  constructor(deliveryMethod: DeliveryMethodDA = {} as DeliveryMethodDA) {

    this._id = deliveryMethod._id;
    this.reference = deliveryMethod.reference;
    this.name = deliveryMethod.name;
    this.code = deliveryMethod.code;
    this.price = deliveryMethod.price;
  }
}
