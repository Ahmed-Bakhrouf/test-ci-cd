import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AccountPage } from "./pages/account.page";
import { ChangePasswordPage } from "./pages/change-password.page";
import { MobilePhoneConfirmationPage } from "./pages/mobile-phone-confirmation.page";
import { AddressFormPage } from "./pages/address-form.page";
import { PaymentMethodFormPage } from "./pages/payment-method-form.page";
import { OrderDetailPage } from "./pages/order-detail.page";
import { InformationsPage } from "../cart/pages/informations.page";

const routes: Routes = [
  { path: "", component: AccountPage },
  { path: "signin", component: InformationsPage },
  { path: "change-password", component: ChangePasswordPage },
  { path: "mobile-phone-confirmation", component: MobilePhoneConfirmationPage },
  { path: "add-address", component: AddressFormPage, data: { mode: "add" } },
  {
    path: "edit-address/:addressId",
    component: AddressFormPage,
    data: { mode: "edit" },
  },
  {
    path: "add-payment-method",
    component: PaymentMethodFormPage,
    data: { mode: "add" },
  },
  {
    path: "edit-payment-method",
    component: PaymentMethodFormPage,
    data: { mode: "edit" },
  },
  { path: "order-detail", component: OrderDetailPage },
  { path: ":opened", component: AccountPage },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountRoutingModule {}
