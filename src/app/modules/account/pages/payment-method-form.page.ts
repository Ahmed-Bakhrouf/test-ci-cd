import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Data } from "@angular/router";
import * as $ from "jquery";

@Component({
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>

      <app-layout-navbar
        title="Mon compte"
        subtitle="Bienvenue Alix"
        [logout]="true"
      >
      </app-layout-navbar>

      <app-cart-card
        [title]="title"
        [foldable]="false"
        [backLink]="['/account', 'payment-methods']"
      >
        <label *ngIf="mode === 'add'" class="CartRadio CartRadio--off">
          <div class="CartRadio-content">
            <input
              class="CartRadio-content-input"
              type="radio"
              name="payment_method"
              [(ngModel)]="payment_method"
              (ngModelChange)="onPaymentMethodChange()"
              value="paypal"
            />
            <div class="CartRadio-content-radio"></div>
            <div
              class="CartRadio-content-label CartRadio-content-label--withIcon"
            >
              <div class="CartRadio-content-label-icon">
                <img src="assets/images/cart/paypal_logo.svg" alt="Paypal" />
              </div>
              <div class="CartRadio-content-label-content">
                <div class="CartRadio-content-label-content-text">
                  Paypal (prochainement)
                </div>
              </div>
            </div>
          </div>
        </label>
        <label class="CartRadio">
          <div class="CartRadio-content">
            <input
              class="CartRadio-content-input"
              type="radio"
              name="payment_method"
              [(ngModel)]="payment_method"
              (ngModelChange)="onPaymentMethodChange()"
              value="credit_card"
            />
            <div *ngIf="mode === 'add'" class="CartRadio-content-radio"></div>
            <div
              class="CartRadio-content-label CartRadio-content-label--withIcon"
            >
              <div class="CartRadio-content-label-icon">
                <img
                  src="assets/images/cart/credit_card_icon.svg"
                  alt="Paypal"
                />
              </div>
              <div class="CartRadio-content-label-content">
                <div class="CartRadio-content-label-content-text">
                  Carte bancaire
                </div>
              </div>
            </div>
          </div>
          <div class="CartRadio-subContent">
            <div class="PaymentForm">
              <div class="PaymentForm-item">
                <div class="PaymentForm-item-icon">
                  <img
                    src="assets/images/cart/credit_card_icon.svg"
                    alt="Carte bancaire"
                  />
                </div>
                <div class="PaymentForm-item-input">
                  <input
                    type="text"
                    placeholder="XXXX XXXX XXXX XXXX | MM/AA | CVC"
                    name="credit_card"
                  />
                </div>
              </div>
            </div>
          </div>
        </label>
      </app-cart-card>

      <button *ngIf="mode === 'edit'" class="Page-link">
        Supprimer mon moyen de paiement
      </button>

      <div class="Page-button Page-button--stickBottom">
        <button class="Button Button--bigger Button--red">Valider</button>
      </div>
      <div class="Page-button Page-button--placeholder"></div>
    </div>
  `,
})
export class PaymentMethodFormPage implements OnInit {
  title: string;
  mode: "add" | "edit";
  payment_method: string;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.data.subscribe((data: Data) => {
      this.mode = data.mode;
      if (this.mode === "add") {
        this.title = "Ajouter une moyen de paiement";
      } else {
        this.payment_method = "credit_card";
        this.onPaymentMethodChange();
        this.title = "Modifier mon moyen de paiement";
      }
    });
  }

  onPaymentMethodChange() {
    const selector = `input[value="${this.payment_method}"]`;
    $(".CartRadio-subContent").slideUp();
    $(selector).parent().next(".CartRadio-subContent").slideDown();
  }
}
