import { Component } from "@angular/core";

@Component({
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>

      <app-layout-navbar
        title="Mon compte"
        subtitle="Bienvenue Alix"
        [logout]="true"
      >
      </app-layout-navbar>

      <app-cart-card
        title="Confirmation du téléphone"
        [foldable]="false"
        [backLink]="['/account', 'informations']"
      >
        <form class="Form">
          <div class="Form-text">
            Entrez le mot de passe que vous venez de recevoir au
            <strong>+33623456789</strong>
          </div>
          <div class="Form-field">
            <div class="Form-field-input">
              <input type="password" name="password" />
              <label>Mot de passe</label>
            </div>
          </div>
          <div class="Form-links">
            <a href="#" class="Form-links-item">Renvoyez un SMS</a>
            <a href="#" class="Form-links-item">Mauvais numéro</a>
          </div>
        </form>
      </app-cart-card>

      <div class="Page-button Page-button--stickBottom">
        <button class="Button Button--bigger Button--red">Valider</button>
      </div>
      <div class="Page-button Page-button--placeholder"></div>
    </div>
  `,
})
export class MobilePhoneConfirmationPage {}
