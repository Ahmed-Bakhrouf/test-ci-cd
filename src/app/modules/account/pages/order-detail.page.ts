import { Component } from "@angular/core";

@Component({
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>

      <app-layout-navbar
        title="Mon compte"
        subtitle="Bienvenue Alix"
        [logout]="true"
      >
      </app-layout-navbar>

      <app-cart-card
        title="Informations sur la commande"
        [foldable]="false"
        [backLink]="['/account', 'orders']"
      >
        <div class="OrderDetail">
          <div class="OrderDetail-title">
            #SOD-3-30072020153341 chez <strong>PC DOM</strong>
          </div>
          <div class="OrderDetail-card">
            <div class="OrderDetail-card-title">Suivi de la commande</div>
            <div class="OrderDetail-card-steps">
              <div
                class="OrderDetail-card-steps-item OrderDetail-card-steps-item--on"
              >
                <div class="OrderDetail-card-steps-item-icon"></div>
                <div class="OrderDetail-card-steps-item-content">
                  <div class="OrderDetail-card-steps-item-content-title">
                    Commande reçue
                  </div>
                  <div class="OrderDetail-card-steps-item-content-subtitle">
                    10/09/2020
                  </div>
                </div>
              </div>
              <div
                class="OrderDetail-card-steps-item OrderDetail-card-steps-item--on"
              >
                <div class="OrderDetail-card-steps-item-icon"></div>
                <div class="OrderDetail-card-steps-item-content">
                  <div class="OrderDetail-card-steps-item-content-title">
                    Commande confirmée
                  </div>
                  <div class="OrderDetail-card-steps-item-content-subtitle">
                    11/09/2020
                  </div>
                </div>
              </div>
              <div class="OrderDetail-card-steps-item">
                <div class="OrderDetail-card-steps-item-icon"></div>
                <div class="OrderDetail-card-steps-item-content">
                  <div class="OrderDetail-card-steps-item-content-title">
                    Commande expédiée
                  </div>
                </div>
              </div>
              <div class="OrderDetail-card-steps-item">
                <div class="OrderDetail-card-steps-item-icon"></div>
                <div class="OrderDetail-card-steps-item-content">
                  <div class="OrderDetail-card-steps-item-content-title">
                    Commande livrée
                  </div>
                </div>
              </div>
            </div>
            <button class="OrderDetail-card-button">Annuler la commande</button>
          </div>

          <div class="OrderDetail-card">
            <div class="OrderDetail-card-title">Mes informations</div>
            <div class="OrderDetail-card-text">
              Alix Pechmajou <br />
              27 avenue de Fontainbleau <br />
              94270 Le Kremlin-Bicêtre <br />
              Appt 150 <br />
              Interphone 20 <br />
              alix.pechmajou@gmail.com
            </div>
          </div>

          <div class="OrderDetail-card">
            <div class="OrderDetail-card-title">Paiement</div>
            <div class="OrderDetail-card-paymentMethod">
              <div class="OrderDetail-card-paymentMethod-icon">
                <img
                  src="assets/images/cart/credit_card_icon.svg"
                  alt="Carte bancaire"
                />
              </div>
              <div class="OrderDetail-card-paymentMethod-content">
                <div class="OrderDetail-card-paymentMethod-content-title">
                  Alix Pechmajou
                </div>
                <div class="OrderDetail-card-paymentMethod-content-subtitle">
                  12XX XXXX XXXX 4567
                </div>
              </div>
            </div>
          </div>

          <div class="OrderDetail-card">
            <div class="OrderDetail-card-title">Livraison</div>
            <div class="OrderDetail-card-text">Livraison à domicile</div>
          </div>

          <div class="OrderDetail-card OrderDetail-card--noPaddingBottom">
            <div class="OrderDetail-card-title">Total</div>
            <div class="OrderDetail-card-table">
              <div class="OrderDetail-card-table-line">
                <div>Sous-total TTC</div>
                <div>15.00 €</div>
              </div>
              <div class="OrderDetail-card-table-line">
                <div>Livraison</div>
                <div>9.99 €</div>
              </div>
              <div class="OrderDetail-card-table-line">
                <div>Frais de service</div>
                <div>4.00 €</div>
              </div>
              <div
                class="OrderDetail-card-table-line OrderDetail-card-table-line--total"
              >
                <div>Total TTC</div>
                <div>28.99 €</div>
              </div>
            </div>
          </div>
        </div>
      </app-cart-card>
    </div>
  `,
})
export class OrderDetailPage {}
