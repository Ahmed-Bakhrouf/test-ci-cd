import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  ValidatorFn,
  ValidationErrors,
} from "@angular/forms";
import { take, switchMap, catchError } from "rxjs/operators";
import { of, Observable } from "rxjs";
import { Location } from "@angular/common";

import { LoginService } from "src/app/services/loginService/login.service";
import { CustomerService } from "src/app/services/Customer/customer.service";
import { ModalService } from "src/app/services/Shared/modal.service";
import { CustomerDA } from "src/app/modelsDeliveryApp/customerDA";

@Component({
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>

      <app-layout-navbar
        title="Mon compte"
        [subtitle]="'Bienvenue ' + (customer$ | async).firstname"
        [logout]="true"
      >
      </app-layout-navbar>

      <app-cart-card
        title="Changer de mot de passe"
        [foldable]="false"
        [backLink]="['/account', 'informations']"
      >
        <form class="Form" [formGroup]="passwordForm">
          <div class="Form-text">
            Renseignez votre ancien mot de passe <br />
            ainsi que le nouveau
          </div>
          <div class="Form-field">
            <div class="Form-field-input">
              <input type="password" formControlName="oldPassword" />
              <label>Ancien mot de passe</label>
            </div>
          </div>
          <div class="Form-field">
            <div class="Form-field-input">
              <input type="password" formControlName="newPassword" />
              <label>Nouveau mot de passe</label>
            </div>
            <div
              *ngIf="
                passwordForm.errors?.samePassword &&
                (passwordForm.touched || passwordForm.dirty)
              "
              class="Form-field-error"
            >
              SVP utilisez un mot de passe different
            </div>
            <div
              *ngIf="passwordForm.errors?.wrongPassword"
              class="Form-field-error"
            >
              Mot de passe incorrect !
            </div>
          </div>
        </form>
      </app-cart-card>

      <div class="Page-button Page-button--stickBottom">
        <Loading-button
          [disabled]="!passwordForm.valid"
          (onclick)="changePassword()"
          [resolving]="resolving"
          label="Valider"
        ></Loading-button>
      </div>
      <div class="Page-button Page-button--placeholder"></div>
    </div>
  `,
})
export class ChangePasswordPage implements OnInit {
  constructor(
    private modalService: ModalService,
    private _location: Location,
    private fb: FormBuilder,
    private loginService: LoginService,
    private customerService: CustomerService
  ) {}

  customer$: Observable<CustomerDA>;

  passwordForm: FormGroup;

  resolving = false;

  ngOnInit() {
    this.customer$ = this.loginService.customer$;
    this.passwordForm = this.fb.group(
      {
        oldPassword: ["", Validators.required],
        newPassword: ["", Validators.required],
      },
      { validators: samePasswordValidator }
    );
  }

  changePassword() {
    this.resolving = true;
    const { oldPassword, newPassword } = this.passwordForm.value;
    const { mobile_number, _id } = this.loginService.getUser();
    this.loginService
      .getToken(mobile_number.toString(), oldPassword)
      .pipe(
        take(1),
        switchMap(({ token }) => {
          if (token)
            return this.customerService.updatePassword(_id, newPassword);
        }),
        catchError((err) => {
          this.passwordForm.setErrors({ wrongPassword: true });
          return of(null);
        })
      )
      .subscribe((user) => {
        this.resolving = false;
        if (user) this.changePasswordSuccess();
      });
  }

  async changePasswordSuccess() {
    const title = "Félicitations";
    const text = `
            Mot de passe changé avec succès ! <br>
        `;
    const buttonText = "Retour";
    const icon = "assets/images/cart/confirm_icon.svg";
    await this.modalService.alert(title, text, buttonText, null, icon);
    this._location.back();
  }
}

export const samePasswordValidator: ValidatorFn = (
  control: FormGroup
): ValidationErrors | null => {
  const oldPassword = control.get("oldPassword");
  const newPassword = control.get("newPassword");
  return oldPassword && newPassword && oldPassword.value === newPassword.value
    ? { samePassword: true }
    : null;
};
