import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Params } from "@angular/router";
import { CustomerDA } from "src/app/modelsDeliveryApp/customerDA";
import { CustomerService } from "src/app/services/Customer/customer.service";
import { LoginService } from "src/app/services/loginService/login.service";
import { switchMap, take } from "rxjs/operators";

@Component({
  template: `
    <div class="Page" *ngIf="customer">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>

      <app-layout-navbar
        title="Mon compte"
        [subtitle]="'Bienvenue ' + customer.firstname"
        [logout]="true"
        (onLogout)="logout()"
      >
      </app-layout-navbar>

      <app-account-informations
        *ngIf="customer"
        [customer]="customer"
        [opened]="informationsOpened"
      ></app-account-informations>
      <app-account-addresses
        [addressList]="customer.address"
        [opened]="addressesOpened"
      ></app-account-addresses>
      <app-account-payment-methods
        [opened]="paymentMethodsOpened"
      ></app-account-payment-methods>
      <app-account-orders [opened]="ordersOpened"></app-account-orders>

      <button class="Page-link">Supprimer mon compte</button>
    </div>
  `,
})
export class AccountPage implements OnInit {
  informationsOpened: boolean = false;
  addressesOpened: boolean = false;
  paymentMethodsOpened: boolean = false;
  ordersOpened: boolean = false;

  customer: CustomerDA;

  constructor(
    private route: ActivatedRoute,
    private loginService: LoginService,
    private customerService: CustomerService
  ) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      switch (params.opened) {
        case "informations":
          this.informationsOpened = true;
          break;
        case "addresses":
          this.addressesOpened = true;
          break;
        case "payment-methods":
          this.paymentMethodsOpened = true;
          break;
        case "orders":
          this.ordersOpened = true;
          break;
        default:
          break;
      }
    });
    this.getCustomer();
  }

  logout() {
    this.loginService.logout();
  }

  getCustomer() {
    if (this.loginService.isAuth()) {
      this.customerService
        .getCustomer(this.loginService.getUser()._id)
        .pipe(
          take(1),
          switchMap((customer) => {
            this.customer = customer;
            this.customer.address = [];
            return this.customerService.getAdresse(this.customer._id);
          })
        )
        .subscribe((address) => {
          this.customer.address = address;
        });
    }
  }
}
