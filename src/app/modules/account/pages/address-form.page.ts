import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Data } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { CustomerService } from "src/app/services/Customer/customer.service";
import { CustomerAdressDA } from "src/app/modelsDeliveryApp/customerAdressDA";
import { take, map } from "rxjs/operators";
import { Location } from "@angular/common";
import { LoginService } from "src/app/services/loginService/login.service";
import { Country } from "src/app/modelsDeliveryApp/country";
import { Observable } from "rxjs";
import { CustomerDA } from "src/app/modelsDeliveryApp/customerDA";

@Component({
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>

      <app-layout-navbar
        title="Mon compte"
        [subtitle]="'Bienvenue ' + (customer$ | async).firstname"
        [logout]="true"
      >
      </app-layout-navbar>

      <app-cart-card
        [title]="title"
        [foldable]="false"
        [backLink]="['/account', 'addresses']"
      >
        <form class="Form" [formGroup]="editAdressForm">
          <div class="Form-field">
            <div class="Form-field-input">
              <input
                type="text"
                [class]="f.get('wording').value.length > 0 ? 'not_empty' : ''"
                formControlName="wording"
              />
              <label>Intitulé de l'adresse</label>
            </div>
          </div>

          <div class="Form-field">
            <div class="Form-field-input">
              <input
                type="text"
                [class]="f.get('line1').value.length > 0 ? 'not_empty' : ''"
                formControlName="line1"
              />
              <label>Adresse <span>*</span></label>
            </div>
          </div>
          <div class="Form-field">
            <div class="Form-field-input">
              <input
                type="text"
                [class]="f.get('line2').value.length > 0 ? 'not_empty' : ''"
                formControlName="line2"
              />
              <label>Complément d'adresse</label>
            </div>
          </div>
          <div class="Form-field">
            <div class="Form-field-input">
              <input
                type="text"
                [class]="
                  f.get('postalCode').value.length > 0 ? 'not_empty' : ''
                "
                formControlName="postalCode"
              />
              <label>Code postal <span>*</span></label>
            </div>
          </div>
          <div class="Form-field">
            <div class="Form-field-input">
              <input
                type="text"
                [class]="f.get('town').value.length > 0 ? 'not_empty' : ''"
                formControlName="town"
              />
              <label>Ville <span>*</span></label>
            </div>
          </div>
          <div class="Form-field">
            <div class="Form-field-input Form-field-input--withLeftIcon">
              <img src="assets/images/france.svg" alt="France" />
              <input
                type="text"
                [class]="f.get('country').value.length > 0 ? 'not_empty' : ''"
                value="France"
                disabled="disabled"
              />
              <label>Pays <span>*</span></label>
            </div>
          </div>
        </form>
      </app-cart-card>

      <button *ngIf="mode === 'edit'" class="Page-link">
        Supprimer mon adresse
      </button>

      <div class="Page-button Page-button--stickBottom">
        <Loading-button
          [disabled]="!disable"
          (onclick)="submitForm()"
          [resolving]="resolving"
          label="Valider"
        ></Loading-button>
      </div>
      <div class="Page-button Page-button--placeholder"></div>
    </div>
  `,
  styles: [
    `
      ::ng-deep .mat-progress-spinner circle,
      .mat-spinner circle {
        stroke: white;
      }
    `,
  ],
})
export class AddressFormPage implements OnInit {
  title: string;
  mode: "add" | "edit";

  customer$: Observable<CustomerDA>;

  countries$: Observable<Country[]>;

  addressId: string;

  editAdressForm: FormGroup;

  resolving = false;

  get f() {
    return this.editAdressForm;
  }

  get disable() {
    return this.editAdressForm.valid && this.editAdressForm.dirty;
  }

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private customerService: CustomerService,
    private loginService: LoginService,
    private _location: Location
  ) {}

  ngOnInit() {
    this.customer$ = this.loginService.customer$;
    this.route.data.subscribe((data: Data) => {
      this.mode = data.mode;
      if (this.mode === "add") {
        this.title = "Ajouter une nouvelle adresse";
      } else {
        this.title = "Modifier mon adresse";
      }
    });
    this.initForm();
    this.addressId = this.route.snapshot.paramMap.get("addressId");
    if (this.addressId)
      this.customerService
        .getAdresseById(this.addressId)
        .pipe(take(1))
        .subscribe((address) => {
          this.editAdressForm.patchValue(address[0]);
          this.editAdressForm.get("country").setValue(address[0].country._id);
        });
  }

  initForm() {
    this.editAdressForm = this.fb.group({
      wording: [""],
      line1: ["", Validators.required],
      line2: [""],
      postalCode: ["", Validators.required],
      town: ["", Validators.required],
      country: ["", Validators.required],
      customer: [""],
      status: "ACTIVE",
      latt: null,
      long: null,
    });
    this.customerService
      .getCountry()
      .pipe(
        take(1),
        map((countries) => {
          if (countries && countries[0]) {
            this.editAdressForm.get("country").setValue(countries[0]._id);
            return countries;
          } else return [];
        })
      )
      .subscribe();
  }

  submitForm() {
    this.resolving = true;
    let address = new CustomerAdressDA(this.f.value);
    if (this.addressId)
      this.customerService
        .updateAdress(this.f.value, this.addressId)
        .pipe(take(1))
        .subscribe(
          (add) => {
            this._location.back();
          },
          (err) => (this.resolving = false)
        );
    else {
      address.customer = this.loginService.getUser()._id;
      this.customerService.createAddresse(address).subscribe(
        (add) => {
          this._location.back();
        },
        (err) => (this.resolving = false)
      );
    }
  }
}
