import {
  Component,
  HostBinding,
  ElementRef,
  ViewChild,
  Input,
} from "@angular/core";
import * as $ from "jquery";
import { Router } from "@angular/router";

@Component({
  selector: "app-account-order",
  template: `
    <div class="Orders-item-header" (click)="toggleContent()">
      <div class="Orders-item-header-text">
        <div class="Orders-item-header-text-title">
          Commande le <strong>29/07/20 de 29.00 €</strong>
        </div>
        <div class="Orders-item-header-text-reference">
          ORD-2-29072020223455
        </div>
      </div>
      <div class="Orders-item-header-trigger">
        <img src="assets/images/cart/trigger_icon.svg" alt="Trigger" />
      </div>
    </div>
    <div class="Orders-item-subOrders" #content>
      <div class="Orders-item-subOrders-item" (click)="detail()">
        <div class="Orders-item-subOrders-item-title">
          #SOD-3-30072020153341 chez <strong>PC DOM</strong>
        </div>
        <div class="Orders-item-subOrders-item-content">
          <div class="Orders-item-subOrders-item-content-items">
            <div class="Orders-item-subOrders-item-content-items-item">
              <div class="Orders-item-subOrders-item-content-items-item-image">
                <img
                  src="assets/images/product_page/evian1.png"
                  alt="Bouteille d'évian"
                />
              </div>
              <div
                class="Orders-item-subOrders-item-content-items-item-content"
              >
                <div
                  class="Orders-item-subOrders-item-content-items-item-content-title"
                >
                  <span>3 x</span> Bouteille Evian
                </div>
                <div
                  class="Orders-item-subOrders-item-content-items-item-content-quantity"
                >
                  1 litre
                </div>
                <div
                  class="Orders-item-subOrders-item-content-items-item-content-price"
                >
                  3.75 €
                </div>
              </div>
            </div>
            <div class="Orders-item-subOrders-item-content-items-item">
              <div class="Orders-item-subOrders-item-content-items-item-image">
                <img
                  src="assets/images/product_page/evian1.png"
                  alt="Bouteille d'évian"
                />
              </div>
              <div
                class="Orders-item-subOrders-item-content-items-item-content"
              >
                <div
                  class="Orders-item-subOrders-item-content-items-item-content-title"
                >
                  <span>3 x</span> Bouteille Evian
                </div>
                <div
                  class="Orders-item-subOrders-item-content-items-item-content-quantity"
                >
                  1 litre
                </div>
                <div
                  class="Orders-item-subOrders-item-content-items-item-content-price"
                >
                  3.75 €
                </div>
              </div>
            </div>
          </div>
          <div class="Orders-item-subOrders-item-content-arrowButton">
            <img src="assets/images/account/right_arrow.svg" />
          </div>
        </div>
      </div>
    </div>
  `,
})
export class OrderComponent {
  @HostBinding("class") get classes(): string {
    const classes = ["Orders-item"];
    if (this.opened) {
      classes.push("Orders-item--opened");
    }
    return classes.join(" ");
  }

  @ViewChild("content", { static: false }) contentElement: ElementRef;

  opened: boolean = false;

  jQueryContent: any;

  constructor(private router: Router) {}

  ngAfterViewInit() {
    this.jQueryContent = $(this.contentElement.nativeElement);
    if (this.opened) {
      this.jQueryContent.slideDown();
    }
  }

  toggleContent() {
    this.opened = !this.opened;
    if (this.opened) {
      this.jQueryContent.slideDown();
    } else {
      this.jQueryContent.slideUp();
    }
  }

  detail() {
    this.router.navigate(["/account", "order-detail"]);
  }
}
