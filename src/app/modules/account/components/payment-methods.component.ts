import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-account-payment-methods",
  template: `
    <app-cart-card title="Mes moyens de paiement" [opened]="opened">
      <div class="MyPaymentMethods">
        <div class="MyPaymentMethods-item">
          <div class="MyPaymentMethods-item-icon">
            <img
              src="assets/images/cart/credit_card_icon.svg"
              alt="Carte bancaire"
            />
          </div>
          <div class="MyPaymentMethods-item-content">
            <div class="MyPaymentMethods-item-content-creditCardTitle">
              Alix Pechmajou
            </div>
            <div class="MyPaymentMethods-item-content-creditCardNumbers">
              12XX XXXX XXXX 4567
            </div>
          </div>
          <div class="MyPaymentMethods-item-icon" (click)="editPaymentMethod()">
            <img src="assets/images/account/edit_icon.svg" alt="Modifier" />
          </div>
        </div>
        <button class="CartCard-button" (click)="addPaymentMethod()">
          <div class="CartCard-button-text">Ajouter un moyen de paiement</div>
          <div class="CartCard-button-icon">
            <img src="assets/images/cart/add_icon.svg" alt="Add" />
          </div>
        </button>
      </div>
    </app-cart-card>
  `,
})
export class PaymentMethodsComponent {
  @Input() opened: boolean;

  constructor(private router: Router) {}

  addPaymentMethod() {
    this.router.navigate(["/account", "add-payment-method"]);
  }

  editPaymentMethod() {
    this.router.navigate(["/account", "edit-payment-method"]);
  }
}
