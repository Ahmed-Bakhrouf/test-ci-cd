import { Component, Input, OnInit, OnChanges } from "@angular/core";
import { Router } from "@angular/router";
import { CustomerAdressDA } from "src/app/modelsDeliveryApp/customerAdressDA";

@Component({
  selector: "app-account-addresses",
  template: `
    <app-cart-card title="Mes adresses" [opened]="opened">
      <div class="MyAddresses">
        <div class="MyAddresses-item" *ngFor="let address of addressList">
          <div class="MyAddresses-item-content">
            <div class="MyAddresses-item-content-name">
              {{ (address.wording ? address.wording : "") | titlecase }}
            </div>
            <div class="MyAddresses-item-content-address">
              {{ address | address: 1 }} <br />
              {{ address | address: 2 }}
            </div>
          </div>
          <div class="MyAddresses-item-icon" (click)="editAddress(address._id)">
            <img src="assets/images/account/edit_icon.svg" alt="Modifier" />
          </div>
        </div>

        <button class="CartCard-button" (click)="addAddress()">
          <div class="CartCard-button-text">Ajouter une nouvelle adresse</div>
          <div class="CartCard-button-icon">
            <img src="assets/images/cart/add_icon.svg" alt="Add" />
          </div>
        </button>
      </div>
    </app-cart-card>
  `,
})
export class AddressesComponent implements OnInit {
  @Input() opened: boolean;

  @Input() addressList: CustomerAdressDA[] = [];

  constructor(private router: Router) {}

  ngOnInit() {}

  addAddress() {
    this.router.navigate(["/account", "add-address"]);
  }

  editAddress(addressId) {
    this.router.navigate(["/account", "edit-address", addressId]);
  }
}
