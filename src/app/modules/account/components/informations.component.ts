import { Component, Input, OnInit, OnChanges } from "@angular/core";
import { Router } from "@angular/router";
import { CustomerDA } from "src/app/modelsDeliveryApp/customerDA";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CustomerService } from "src/app/services/Customer/customer.service";
import { Location } from "@angular/common";
import { pipe } from "rxjs";
import { take } from "rxjs/operators";
import { MatSnackBar } from "@angular/material";

@Component({
  selector: "app-account-informations",
  template: `
    <app-cart-card title="Mes informations" [opened]="opened" *ngIf="formReady">
      <form class="Form" [formGroup]="informationForm">
        <div class="Form-field">
          <div class="Form-field-input Form-field-input--withLeftIcon">
            <img src="assets/images/france.svg" alt="France" />
            <input
              type="text"
              [class]="
                f.get('mobile_number').value.length > 0 ? 'not_empty' : ''
              "
              formControlName="mobile_number"
            />
            <label>Numéro de mobile <span>*</span></label>
          </div>
        </div>
        <div class="Form-field" (click)="gotoChangePassword()">
          <div class="Form-field-input Form-field-input--withRightIcon">
            <input type="password" />
            <label>Mot de passe <span>*</span></label>
            <img src="assets/images/account/right_arrow.svg" />
          </div>
        </div>
        <div class="Form-field">
          <div class="Form-field-input">
            <input
              type="text"
              [class]="f.get('firstname').value.length > 0 ? 'not_empty' : ''"
              formControlName="firstname"
            />
            <label>Prénom <span>*</span></label>
          </div>
          <div
            *ngIf="
              informationForm.get('firstname').errors?.required &&
              (informationForm.touched || informationForm.dirty)
            "
            class="Form-field-error"
          >
            Le prénom est obligatoire !
          </div>
        </div>
        <div class="Form-field">
          <div class="Form-field-input">
            <input
              type="text"
              [class]="f.get('lastname').value.length > 0 ? 'not_empty' : ''"
              formControlName="lastname"
            />
            <label>Nom <span>*</span></label>
          </div>
          <div
            *ngIf="
              informationForm.get('lastname').errors?.required &&
              (informationForm.touched || informationForm.dirty)
            "
            class="Form-field-error"
          >
            Le nom est obligatoire !
          </div>
        </div>
        <div class="Form-field">
          <div class="Form-field-input">
            <input
              type="text"
              [class]="f.get('email').value.length > 0 ? 'not_empty' : ''"
              formControlName="email"
            />
            <label>Adresse e-mail <span>*</span></label>
          </div>
          <div
            *ngIf="
              informationForm.get('email').errors?.email &&
              (informationForm.touched || informationForm.dirty)
            "
            class="Form-field-error"
          >
            Email incorrect
          </div>
          <div
            *ngIf="
              informationForm.get('email').errors?.required &&
              (informationForm.touched || informationForm.dirty)
            "
            class="Form-field-error"
          >
            Email est obligatoire !
          </div>
        </div>
        <button
          type="submit"
          [disabled]="!informationForm.valid"
          class="Form-button"
          (click)="updateUserInfo()"
        >
          Valider
        </button>
      </form>
    </app-cart-card>
  `,
})
export class InformationsComponent implements OnInit {
  @Input() opened: boolean;

  @Input() customer: CustomerDA;

  formReady = false;
  informationForm: FormGroup;
  get f() {
    return this.informationForm;
  }

  constructor(
    private _location: Location,
    private router: Router,
    private fb: FormBuilder,
    private _snackBar: MatSnackBar,
    private customerService: CustomerService
  ) {}

  ngOnInit() {
    this.initForm();
  }

  gotoChangePassword() {
    this.router.navigate(["/account", "change-password"]);
  }

  //in case of changing the mobile_number
  gotoMobilePhoneConfirmation() {
    this.router.navigate(["/account", "mobile-phone-confirmation"]);
  }

  updateUserInfo() {
    this.customerService
      .update(this.informationForm.value, this.customer._id)
      .pipe(take(1))
      .subscribe(() => {
        this.opened = false;
        // TODO needs validation
        this.updateSuccess("Info utilisateur sont mis à jour", "Cacher");
        //this._location.back();
      });
  }

  initForm() {
    this.informationForm = this.fb.group({
      firstname: ["", Validators.required],
      lastname: ["", Validators.required],
      mobile_number: [{ value: "", disabled: true }, Validators.required],
      email: ["", [Validators.required, Validators.email]],
    });
    this.informationForm.patchValue(this.customer);

    this.formReady = true;
  }

  updateSuccess(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
