import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-account-orders',
    template: `
        <app-cart-card title="Mes commandes" [opened]="opened">
            <div class="Orders">
                <app-account-order></app-account-order>
                <app-account-order></app-account-order>
            </div>
        </app-cart-card>
    `
})

export class OrdersComponent  {

    @Input() opened: boolean;

}