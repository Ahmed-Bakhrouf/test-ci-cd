import { LayoutModule } from "../layout/layout.module";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AccountRoutingModule } from "./account-routing.module";
import { AccountPage } from "./pages/account.page";
import { CartModule } from "../cart/cart.module";
import { PaymentMethodsComponent } from "./components/payment-methods.component";
import { InformationsComponent } from "./components/informations.component";
import { AddressesComponent } from "./components/addresses.component";
import { OrdersComponent } from "./components/orders.components";
import { ChangePasswordPage } from "./pages/change-password.page";
import { MobilePhoneConfirmationPage } from "./pages/mobile-phone-confirmation.page";
import { AddressFormPage } from "./pages/address-form.page";
import { PaymentMethodFormPage } from "./pages/payment-method-form.page";
import { OrderComponent } from "./components/order.component";
import { OrderDetailPage } from "./pages/order-detail.page";
import { SharedModule } from "../shared/shared.module";
import { MatProgressSpinnerModule } from "@angular/material";

@NgModule({
  imports: [
    CommonModule,
    AccountRoutingModule,
    LayoutModule,
    CartModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MatProgressSpinnerModule,
  ],
  declarations: [
    /** Pages  */
    AccountPage,
    ChangePasswordPage,
    MobilePhoneConfirmationPage,
    AddressFormPage,
    PaymentMethodFormPage,
    OrderDetailPage,

    /** Components  */
    InformationsComponent,
    AddressesComponent,
    PaymentMethodsComponent,
    OrdersComponent,
    OrderComponent,
  ],
})
export class AccountModule {}
