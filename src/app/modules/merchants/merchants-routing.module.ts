import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListPage } from './pages/list.page';
import { DetailsPage } from './pages/details.page';
import { MoreDetailsPage } from './pages/more-details.page';

const routes: Routes = [
    { path: '', component: ListPage },
    { path: 'details', component: DetailsPage },
    { path: 'details/more', component: MoreDetailsPage },
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class MerchantsRoutingModule { }
