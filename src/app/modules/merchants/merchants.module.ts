import { LayoutModule } from "../layout/layout.module";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MerchantsRoutingModule } from "./merchants-routing.module";
import { ListPage } from "./pages/list.page";
import { DetailsPage } from "./pages/details.page";
import { MoreDetailsPage } from "./pages/more-details.page";
import { ProductsModule } from "../products/products.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    MerchantsRoutingModule,
    LayoutModule,
    ProductsModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    /** Pages  */
    ListPage,
    DetailsPage,
    MoreDetailsPage,

    /** Components  */
  ],
})
export class MerchantsModule {}
