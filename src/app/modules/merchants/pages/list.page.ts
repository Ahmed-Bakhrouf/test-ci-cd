import { Component, OnInit } from "@angular/core";
import { MerchantDA } from "../../../modelsDeliveryApp/merchantDA";
import { MerchantService } from "../../../services/merchant/merchant.service";
import { Router } from "@angular/router";

@Component({
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!--       <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>

      <app-layout-navbar
        title="Marchands partenaires"
        subtitle="143 partenaires"
        [backLink]="['/products']"
      ></app-layout-navbar>

      <app-banner
        image="assets/images/banner/BANNER02.jpg"
        title="Rejoindre Bokay ?"
        content="Vous êtes commerçant et souhaitez nous rejoindre ?"
        btnText="Contactez-nous"
        textColor="white"
        btnColor="red"
      ></app-banner>

      <div class="MerchantsList" *ngFor="let merchant of merchantList">
        <!--        <a [routerLink]="['/merchants', 'details']" [queryParams]="{id: merchant._id}"class="MerchantsList-item">-->
        <a (click)="showInfoMerchant(merchant._id)" class="MerchantsList-item">
          <div class="MerchantsList-item-image">
            <!--            <img  src="../../../../assets/bokay_logo.png"  height="50" alt="">-->
            <img
              [src]="merchant.logo || 'i../../../../assets/bokay_logo.png'"
              alt="Marchant 1"
            />
          </div>
          <div class="MerchantsList-item-content">
            <div class="MerchantsList-item-content-name">
              {{ merchant.trade_name }}
            </div>
            <div class="MerchantsList-item-content-address">
              <p
                class="marchant-adresse"
                *ngIf="merchant.address && merchant.address.length > 0"
              >
                {{ merchant.address[0].label }}
                {{ merchant.address[0].line1 }}
                {{ merchant.address[0].line2 }}, {{ merchant.address[0].city }}
              </p>
            </div>
            <div class="MerchantsList-item-content-quantity">
              14 produits disponibles
            </div>
          </div>
        </a>
        <!--<a [routerLink]="['/merchants', 'details']"class="MerchantsList-item">
            <div class="MerchantsList-item-image">
                <img src="assets/images/merchants/merchant2.png" alt="Marchant 2">
            </div>
            <div class="MerchantsList-item-content">
                <div class="MerchantsList-item-content-name">
                    Mon petit marché 2
                </div>
                <div class="MerchantsList-item-content-address">
                    27 avenue de Fontainebleau <br>
                    94270 Le Kremlin-Bicêtre
                </div>
                <div class="MerchantsList-item-content-quantity">
                    14 produits disponibles
                </div>
            </div>
        </a>
        <a [routerLink]="['/merchants', 'details']"class="MerchantsList-item">
            <div class="MerchantsList-item-image">
                <img src="assets/images/merchants/merchant1.png" alt="Marchant 3">
            </div>
            <div class="MerchantsList-item-content">
                <div class="MerchantsList-item-content-name">
                    Mon petit marché 3
                </div>
                <div class="MerchantsList-item-content-address">
                    27 avenue de Fontainebleau <br>
                    94270 Le Kremlin-Bicêtre
                </div>
                <div class="MerchantsList-item-content-quantity">
                    14 produits disponibles
                </div>
            </div>
        </a>-->
      </div>
    </div>
  `,
})
export class ListPage implements OnInit {
  merchantList: MerchantDA[] = [];
  constructor(
    private merchantService: MerchantService,
    private router: Router
  ) {
    this.merchantService.getMerchandList().subscribe((data) => {
      this.merchantList = data;
    });
  }
  ngOnInit() {
    this.merchantService.searchMerchantSubject.subscribe((data) => {
      if (typeof data.items !== "undefined") {
        this.merchantList = data.items;
      } else {
        this.merchantList = data;
      }
    });
  }

  showInfoMerchant(merchantId) {
    const merchant = this.merchantList.find(
      (element) => element._id == merchantId
    );
    localStorage.setItem("Merchant", JSON.stringify(merchant));
    // this.router.navigate(['/merchant','merchant-details', merchantId ]);
    this.router.navigate(["/merchants", "details"], merchantId);
  }
}
