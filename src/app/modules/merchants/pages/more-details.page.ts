import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { RelayPointService } from "../../../services/relayPoint/relay-point.service";
import { MerchantDA } from "../../../modelsDeliveryApp/merchantDA";
import { isUndefined } from "util";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>

      <app-layout-navbar
        title="{{ merchant.trade_name }}"
        subtitle="14 produits"
        [backLink]="['/merchants', 'details']"
      ></app-layout-navbar>

      <div class="MerchantDetails">
        <div
          *ngIf="displayMap"
          class="MerchantDetails-map"
          #mapContainer
          id="map"
        ></div>
        <div class="MerchantDetails-moreDetails">
          <div class="MerchantDetails-moreDetails-title">Informations</div>
          <div class="MerchantDetails-moreDetails-item">
            <div class="MerchantDetails-moreDetails-item-icon">
              <img src="assets/images/merchants/marker_icon.svg" />
            </div>
            <div
              class="MerchantDetails-moreDetails-item-text MerchantDetails-moreDetails-item-text--black"
            >
              <p
                class="marchant-adresse  "
                *ngIf="merchant.address && merchant.address.length > 0"
              >
                {{ merchant.address[0].label }}
                {{ merchant.address[0].line1 }}
                {{ merchant.address[0].line2 }}, {{ merchant.address[0].city }}
              </p>
            </div>
          </div>
          <div class="MerchantDetails-moreDetails-item">
            <div class="MerchantDetails-moreDetails-item-icon">
              <img src="assets/images/merchants/phone_icon.svg" />
            </div>
            <div
              class="MerchantDetails-moreDetails-item-text MerchantDetails-moreDetails-item-text--black"
              disabled
            >
              <div
                class="Form-field-input Form-field-input--withLeftIcon"
                style="background-color: inherit; align-items: center;"
              >
                <span
                  *ngIf="merchant.country_code"
                  class="flag"
                  [ngClass]="merchant.country_code.toUpperCase()"
                ></span>
                <label>{{ merchant.mobile_number }}</label>
              </div>
            </div>
          </div>
        </div>
        <div class="MerchantDetails-moreDetails">
          <div class="MerchantDetails-moreDetails-title">Horaires</div>
          <div *ngFor="let workingDay of merchant.workingDays">
            <div class="MerchantDetails-moreDetails-item">
              <div class="MerchantDetails-moreDetails-item-icon">
                <img src="assets/images/merchants/schedule_icon.svg" />
              </div>
              <div class="MerchantDetails-moreDetails-item-text">
                {{ workingDay.day }}:
                <strong
                  >{{ workingDay.workingPeriods[0].start }}-{{
                    workingDay.workingPeriods[0].end
                  }}
                </strong>
              </div>
            </div>
          </div>
          <!-- <div class="MerchantDetails-moreDetails-item">
                        <div class="MerchantDetails-moreDetails-item-icon">
                            <img src="assets/images/merchants/schedule_icon.svg">
                        </div>
                        <div class="MerchantDetails-moreDetails-item-text">
                            Samedi : <strong>10h00 - 19h00</strong>
                        </div>
                    </div>
                    <div class="MerchantDetails-moreDetails-item">
                        <div class="MerchantDetails-moreDetails-item-icon">
                            <img src="assets/images/merchants/schedule_icon.svg">
                        </div>
                        <div class="MerchantDetails-moreDetails-item-text">
                            Dimanche : <strong>Fermé</strong>
                        </div>
                    </div>-->
        </div>
      </div>
    </div>
  `,
})
// tslint:disable-next-line:component-class-suffix
export class MoreDetailsPage implements AfterViewInit {
  //selectedCountryISO = CountryISO.France;
  cssClass = "angular-tel-input";
  displayMap = true;
  //tooltipField = TooltipLabel.Name;
  primaryAddress: any;
  secondaryAddress: any;
  merchant: MerchantDA;
  // preferredCountries: CountryISO[] = [
  //   CountryISO.France,
  //   CountryISO.FrenchGuiana,
  //   CountryISO.Guadeloupe,
  //   CountryISO.Martinique,
  // ];
  map: google.maps.Map;
  lat = 48.8534;
  lng = 2.3488;
  service: google.maps.places.PlacesService;
  coordinates = new google.maps.LatLng(this.lat, this.lng);
  mapOptions: google.maps.MapOptions = {
    center: this.coordinates,
    zoom: 10,
  };
  marker = new google.maps.Marker({
    position: this.coordinates,
    map: this.map,
  });
  @ViewChild("mapContainer", { static: false }) gmap: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private relayPointService: RelayPointService,
    private formBuilder: FormBuilder
  ) {
    this.merchant = JSON.parse(localStorage.getItem("Merchant"));
    const merchante = JSON.parse(localStorage.getItem("Merchant"));
    try {
      // tslint:disable-next-line:max-line-length
      this.primaryAddress =
        merchante.address[0].label +
        " " +
        merchante.address[0].line1 +
        " " +
        merchante.address[0].line2 +
        " " +
        merchante.address[0].city;
      this.secondaryAddress =
        merchante.address[0].line1 + " " + merchante.address[0].city;
    } catch (error) {}
    if (!isUndefined(this.primaryAddress)) {
      setTimeout(() => {
        this.getPositionByPrimaryAdresse();
      }, 300);
    }
  }
  ngAfterViewInit() {
    this.marker = new google.maps.Marker({
      position: this.coordinates,
      map: this.map,
    });
    this.mapOptions = {
      center: this.coordinates,
      zoom: 15,
    };

    if (!isUndefined(this.primaryAddress)) {
      this.mapInitializer();
    }
  }
  mapInitializer() {
    this.map = new google.maps.Map(this.gmap.nativeElement, this.mapOptions);
    this.marker.setMap(this.map);
  }
  getPositionByPrimaryAdresse() {
    const request = {
      query: this.primaryAddress,
      fields: ["name", "geometry"],
    };
    this.service = new google.maps.places.PlacesService(this.map);
    this.service.findPlaceFromQuery(request, (results, status) => {
      if (status === google.maps.places.PlacesServiceStatus.OK) {
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < results.length; i++) {
          this.lat = results[i].geometry.location.lat();
          this.lng = results[i].geometry.location.lng();
        }

        this.coordinates = new google.maps.LatLng(this.lat, this.lng);
        this.marker = new google.maps.Marker({
          position: this.coordinates,
          map: this.map,
        });
        this.mapOptions = {
          center: this.coordinates,
          zoom: 15,
        };

        this.mapInitializer();
      } else {
        console.log("primary address not founds");
        this.getPositionBySecondaryAdresse();
      }
    });
  }
  getPositionBySecondaryAdresse() {
    const request = {
      query: this.secondaryAddress,
      fields: ["name", "geometry"],
    };
    this.service = new google.maps.places.PlacesService(this.map);
    this.service.findPlaceFromQuery(request, (results, status) => {
      if (status === google.maps.places.PlacesServiceStatus.OK) {
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < results.length; i++) {
          this.lat = results[i].geometry.location.lat();
          this.lng = results[i].geometry.location.lng();
        }

        this.coordinates = new google.maps.LatLng(this.lat, this.lng);
        this.marker = new google.maps.Marker({
          position: this.coordinates,
          map: this.map,
        });
        this.mapOptions = {
          center: this.coordinates,
          zoom: 15,
        };

        this.mapInitializer();
      } else {
        console.log("address not founds");
        this.displayMap = false;
      }
    });
  }
}
