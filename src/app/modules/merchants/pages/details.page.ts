import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { MerchantDA } from "../../../modelsDeliveryApp/merchantDA";
import { Product } from "../../../models/product";
import { environment } from "../../../../environments/environment";
import { ProductService } from "../../../services/Product/product.service";
import { SubCategory } from "../../../modelsDeliveryApp/SubCategory";
import { CategoryDA } from "../../../modelsDeliveryApp/categoryDA";
import { ProductDA } from "../../../modelsDeliveryApp/productDA";
import { ProductBySubCategorie } from "../../../modelsDeliveryApp/productBySubCategorie";

@Component({
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>

      <app-layout-navbar
        title="{{ merchant.trade_name }}"
        subtitle="14 produits"
        [backLink]="['/merchants']"
      ></app-layout-navbar>

      <div class="MerchantDetails">
        <div
          class="MerchantDetails-banner"
          style="background-image: url(assets/images/merchants/merchant_banner.png)"
        >
          <div class="MerchantDetails-banner-logo">
            <img
              [src]="merchant.logo"
              onerror="this.src='assets/images/cart/modal_icon.svg';"
              alt="Mon petit marché"
            />
          </div>
        </div>
        <div class="MerchantDetails-title"></div>
        <div class="MerchantDetails-infos">
          <div class="MerchantDetails-infos-price">€€</div>
          <div class="MerchantDetails-infos-separator">-</div>
          <div class="MerchantDetails-infos-stars">
            <img src="assets/images/merchants/star_on.svg" />
            <img src="assets/images/merchants/star_on.svg" />
            <img src="assets/images/merchants/star_on.svg" />
            <img src="assets/images/merchants/star_on.svg" />
            <img src="assets/images/merchants/star_off.svg" />
          </div>
          <div class="MerchantDetails-infos-text">(115 avis)</div>
        </div>
        <a
          [routerLink]="['/merchants', 'details', 'more']"
          class="MerchantDetails-moreButton"
        >
          <div class="MerchantDetails-moreButton-icon">
            <img src="assets/images/merchants/detail_icon.svg" />
          </div>
          <div class="MerchantDetails-moreButton-text">
            Plus d'informations sur le marchand
          </div>
          <div class="MerchantDetails-moreButton-icon">
            <img src="assets/images/merchants/right_arrow_icon.svg" />
          </div>
        </a>
      </div>

      <!--  <app-products-products-list
                [scrollable]="true"
                title="Alimentation"
                [showAllLink]="['/products', 'family']"
            ></app-products-products-list>
            <app-products-products-list
                [scrollable]="true"
                title="Santé & Bien-être"
                [showAllLink]="['/products', 'family']"
            ></app-products-products-list>-->
      <app-products-products-list
        *ngFor="let subCategory of listsubCategories"
        [scrollable]="true"
        [title]="subCategory.subCategoryTitle"
        [subCategoryId]="subCategory.subCategoryId"
        [merchantId]="merchant._id"
        [showAllLink]="['/products', 'sub-category', subCategory.subCategoryId]"
      ></app-products-products-list>
    </div>
  `,
})
export class DetailsPage implements OnInit {
  merchant: MerchantDA;
  currentCategory: CategoryDA;
  productList: ProductDA[];
  PRODUCT_COUNT: number;
  CURRENT_PAGE: number;
  PER_PAGE: number;
  subCategorie: ProductBySubCategorie;
  listsubCategories: ProductBySubCategorie[] = [];
  subCats: SubCategory[];
  constructor(
    private activatedRoute: ActivatedRoute,
    private productService: ProductService
  ) {
    this.merchant = JSON.parse(localStorage.getItem("Merchant"));
  }
  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      let id = params["id"];
    });
    this.getProductByCategory();
  }
  getProductByCategory() {
    this.productList = null;
    this.productService
      .getProductsMerchant(
        this.merchant._id,
        this.CURRENT_PAGE - 1,
        environment.productPerPage
      )
      .subscribe((data) => {
        this.productList = data.items;
        this.PRODUCT_COUNT = data.total;
        this.CURRENT_PAGE = data.page + 1;
        this.PER_PAGE = environment.productPerPage;
        this.organizeBySubCategory();
      });
  }
  public organizeBySubCategory() {
    // initialisation des listes
    this.listsubCategories = [];
    this.subCats = [];
    // recuperation des subCategories existantes
    if (this.productList.length > 0) {
      this.currentCategory = this.productList[0].category;
      this.productList.forEach((item) => {
        if (this.myIndexOf(item.subCategory, this.subCats) === -1) {
          this.subCats.push(item.subCategory);
        }
      });
      this.subCats.forEach((subCat) => {
        this.subCategorie = {
          subCategoryTitle: "init",
          subCategoryProducts: [],
        };
        this.subCategorie.subCategoryTitle = subCat.title;
        this.subCategorie.subCategoryId = subCat._id;
        this.subCategorie.subCategoryProducts = this.productList.filter(
          (product) => product.subCategory === subCat
        );
        this.listsubCategories.push(this.subCategorie);
      });
    }
  }
  myIndexOf(o: SubCategory, arr: SubCategory[]) {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].title === o.title) {
        return i;
      }
    }
    return -1;
  }
}
