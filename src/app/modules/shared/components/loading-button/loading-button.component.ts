import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  OnChanges,
} from "@angular/core";

@Component({
  selector: "Loading-button",
  template: `
    <button
      [disabled]="disabled || resolving"
      (click)="onclick.next()"
      class="Btn-center-content Button Button--bigger Button--red"
      [ngClass]="{ 'Button--off': disabled }"
    >
      <mat-spinner
        *ngIf="resolving"
        [color]="'primary'"
        [diameter]="30"
      ></mat-spinner>
      {{ resolving ? "" : label }}
    </button>
  `,
  styles: [
    `
      .Btn-center-content {
        display: flex;
        align-items: center;
        justify-content: center;
      }
      ::ng-deep .mat-progress-spinner circle,
      .mat-spinner circle {
        stroke: white;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadingButtonComponent {
  @Input() disabled = false;
  @Input() resolving = false;
  @Input() label = "";
  @Output() onclick: EventEmitter<void> = new EventEmitter();
}
