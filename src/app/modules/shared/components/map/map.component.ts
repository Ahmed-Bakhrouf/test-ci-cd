import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  AfterViewInit,
  Input,
  OnChanges,
  Output,
  EventEmitter,
} from "@angular/core";
import { RelayPointDA } from "src/app/modelsDeliveryApp/relayPointDA";

@Component({
  selector: "app-map",
  template: `<div style="height: 100%;width: 100%;" #mapContainer></div>`,
})
export class MapComponent implements OnInit, AfterViewInit, OnChanges {
  @Input("relayPoints") relayPoints: RelayPointDA[] = [];
  @Input("readOnly") readonly: boolean = true;

  @Output() selectedRelayPoint: EventEmitter<RelayPointDA> = new EventEmitter<
    RelayPointDA
  >();

  constructor() {}

  map: google.maps.Map;
  mapOptions: google.maps.MapOptions;
  coordinates: google.maps.LatLng;

  markers: google.maps.Marker[] = [];

  @ViewChild("mapContainer", { static: false }) gmap: ElementRef;

  ngOnInit() {}
  ngOnChanges() {
    if (this.relayPoints && this.relayPoints.length > 0) {
      this.markers = this.setupMarkers(this.relayPoints);
      this.coordinates = new google.maps.LatLng(
        this.relayPoints[0].lat,
        this.relayPoints[0].long
      );
      this.mapOptions = {
        center: this.coordinates,
        zoom: 10,
        streetViewControl: false,
        mapTypeControl: false,
      };
      this.setupMap();
    }
  }

  ngAfterViewInit() {}

  setupMap() {
    this.map = new google.maps.Map(this.gmap.nativeElement, this.mapOptions);
    this.markers.forEach((element) => {
      element.setMap(this.map);
      if (!this.readonly)
        element.addListener("click", (marker) => {
          this.relayPoints.forEach((relayP) => {
            const mark = JSON.parse(JSON.stringify(marker));
            if (
              mark.latLng.lat == relayP.lat &&
              mark.latLng.lng == relayP.long
            ) {
              //this.selectedRelayPoint = relayP;
              this.selectedRelayPoint.next(relayP);
            }
          });
        });
    });
  }

  setupMarkers(relayPoints: RelayPointDA[]) {
    return relayPoints.map(
      (point) =>
        new google.maps.Marker({
          position: {
            lat: point.lat,
            lng: point.long,
          },
          title: point.name,
        })
    );
  }
}
