import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  MatProgressSpinnerModule,
  MatSnackBarModule,
  MatSnackBar,
} from "@angular/material";
import { NgxMatIntlTelInputModule } from "ngx-mat-intl-tel-input";
import { LoadingButtonComponent } from "./components/loading-button/loading-button.component";
import { MapComponent } from "./components/map/map.component";
import { AddressPipe } from "src/app/pipes/address.pipe";
import { PaymentPipe } from "src/app/pipes/payment.pipe";

@NgModule({
  declarations: [
    LoadingButtonComponent,
    MapComponent,
    AddressPipe,
    PaymentPipe,
  ],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    NgxMatIntlTelInputModule,
  ],
  providers: [MatSnackBar],
  exports: [LoadingButtonComponent, MapComponent, AddressPipe, PaymentPipe],
})
export class SharedModule {}
