import { Component, Output, EventEmitter } from "@angular/core";
import { ModalService } from "../../../services/Shared/modal.service";
import { AbstractModalComponent } from "../../layout/abstract-modal.component";

@Component({
  selector: "app-cart-forgot-password-modal",
  template: `
    <div class="Modal" [ngClass]="{ 'Modal--visible': visible }">
      <div
        class="Layout-overlay"
        [ngClass]="{ 'Layout-overlay--visible': visible }"
        (click)="onCancel.emit()"
      ></div>
      <div class="Modal-content">
        <div class="Modal-content-logo">
          <img src="assets/images/cart/forgot_password.svg" alt="Logo Bokai" />
        </div>
        <div class="Modal-content-title">Mot de passe oublié ?</div>
        <div class="Modal-content-subtitle">
          Entrez votre numéro de mobile pour recevoir un nouveau mot de passe
          par sms
        </div>
        <div class="Modal-content-input">
          <div class="Modal-content-input-icon">
            <img src="assets/images/france.svg" alt="France" />
          </div>
          <div class="Modal-content-input-input">
            <input
              #input
              [(ngModel)]="mobile_phone"
              type="text"
              placeholder="Numéro de mobile"
            />
          </div>
        </div>
        <div class="Modal-content-spacer"></div>
        <div class="Modal-content-button">
          <button class="Button Button--modal Button--red" (click)="submit()">
            Valider
          </button>
        </div>
        <div class="Modal-content-buttons Modal-content-buttons--center">
          <button class="Modal-content-buttons-item" (click)="onCancel.emit()">
            Retour
          </button>
        </div>
      </div>
    </div>
  `,
})
export class ForgotPasswordModalComponent extends AbstractModalComponent {
  @Output() onSubmit: EventEmitter<string> = new EventEmitter();
  @Output() onCancel: EventEmitter<void> = new EventEmitter();

  public mobile_phone: string;

  constructor(modalService: ModalService) {
    super(modalService);
  }

  submit() {
    this.onSubmit.emit(this.mobile_phone);
  }
}
