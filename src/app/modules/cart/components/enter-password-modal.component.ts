import {
  Component,
  Output,
  EventEmitter,
  Input,
  OnInit,
  AfterViewInit,
  OnChanges,
} from "@angular/core";
import { ModalService } from "src/app/services/Shared/modal.service";
import { AbstractModalComponent } from "../../layout/abstract-modal.component";
import { SmsService } from "src/app/services/Sms/sms.service";
import { CustomerService } from "src/app/services/Customer/customer.service";

@Component({
  selector: "app-cart-forgot-password-modal",
  template: `
    <div class="Modal" [ngClass]="{ 'Modal--visible': visible }">
      <div
        class="Layout-overlay"
        [ngClass]="{ 'Layout-overlay--visible': visible }"
        (click)="onCancel.emit()"
      ></div>
      <div class="Modal-content">
        <div class="Modal-content-logo">
          <img src="assets/images/cart/forgot_password.svg" alt="Logo Bokai" />
        </div>
        <div class="Modal-content-title">Entrez le mot de passe</div>
        <div class="Modal-content-subtitle">
          Entrez le mot de passe que nous venons de vous envoyer par SMS au
          <strong>{{ mobile_phone }}</strong>
        </div>
        <div class="Modal-content-input">
          <div class="Modal-content-input-input">
            <input
              #input
              [(ngModel)]="password"
              type="password"
              placeholder="Mot de passe"
            />
          </div>
        </div>
        <div class="Modal-content-spacer"></div>
        <div class="Modal-content-button">
          <button
            class="Button Button--modal Button--red"
            (click)="check(password)"
          >
            Valider
          </button>
        </div>
        <div class="Modal-content-buttons">
          <button
            class="Modal-content-buttons-item"
            (click)="sendPassword(mobile_phone)"
          >
            Renvoyer un SMS
          </button>
          <button class="Modal-content-buttons-item" (click)="onCancel.emit()">
            Retour
          </button>
        </div>
      </div>
    </div>
  `,
})
export class EnterPasswordModalComponent extends AbstractModalComponent {
  @Input() mobile_phone: string;

  @Output() onSubmit: EventEmitter<string> = new EventEmitter();
  @Output() onCancel: EventEmitter<void> = new EventEmitter();

  public password: string;

  generatedPassword;
  passwordLength = 5;
  statusEnum = VerificationStatus;
  verificationStatus = VerificationStatus.NOT_VERIFYED;

  constructor(modalService: ModalService, private smsService: SmsService) {
    super(modalService);
  }

  ngOnInit() {
    this.sendPassword(this.mobile_phone);
  }

  submit() {
    this.onSubmit.emit(this.password);
  }

  sendPassword(phoneNumber: string) {
    this.verificationStatus = this.statusEnum.SENDING_PASSWORD;
    let password =
      this.generatedPassword || this.generatePassword(this.passwordLength);
    if (password.length < this.passwordLength)
      password = this.generatePassword(this.passwordLength);
    this.smsService.sendPassword(phoneNumber, password).subscribe((res) => {
      //this.sentCodes.push(verifyCode);
      this.generatedPassword = password;

      this.verificationStatus = this.statusEnum.PASSWORD_SENT;
    });
  }

  verifyPassword(password: string): boolean {
    if (!this.generatedPassword) return false;
    return this.generatedPassword === password;
  }

  check(password: string) {
    if (password.length == 5) {
      this.verificationStatus = this.statusEnum.CHECKING;
      let verif = this.verifyPassword(password);
      this.verificationStatus = verif
        ? this.statusEnum.CHECKING_SUCCESS
        : this.statusEnum.CHECKING_FAILED;
      if (verif) this.onSubmit.emit(this.password);
    }
  }

  generatePassword(numberChar: number) {
    return this.smsService.generatePassword(numberChar);
  }
}

enum VerificationStatus {
  NOT_VERIFYED,
  SENDING_PASSWORD,
  PASSWORD_SENT,
  CHECKING,
  CHECKING_SUCCESS,
  CHECKING_FAILED,
}
