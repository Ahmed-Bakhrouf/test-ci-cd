import {
  Component,
  Input,
  ViewChild,
  ElementRef,
  OnInit,
  AfterViewInit,
} from "@angular/core";
import * as $ from "jquery";
import { Location } from "@angular/common";

@Component({
  selector: "app-cart-card",
  template: `
    <div class="CartCard" [ngClass]="{ 'CartCard--opened': opened }">
      <div
        *ngIf="title"
        class="CartCard-header"
        (click)="foldable && toggleContent()"
        [ngClass]="{ 'CartCard-header--withBackLink': backLink }"
      >
        <a *ngIf="backLink" class="CartCard-header-backLink" (click)="back()">
          <img src="assets/images/navback_icon.svg" alt="Retour" />
        </a>
        <div class="CartCard-header-title">{{ title }}</div>
        <div *ngIf="foldable" class="CartCard-header-trigger">
          <img src="assets/images/cart/trigger_icon.svg" alt="Trigger" />
        </div>
      </div>
      <div class="CartCard-content" #content>
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class CartCardComponent implements AfterViewInit {
  constructor(private _location: Location) {}

  @ViewChild("content", { static: false }) contentElement: ElementRef;

  @Input() title?: string;
  @Input() opened?: boolean = false;
  @Input() foldable?: boolean = true;
  @Input() backLink?: string[];

  jQueryContent: any;

  ngAfterViewInit() {
    this.jQueryContent = $(this.contentElement.nativeElement);
    if (this.opened || !this.title || !this.foldable) {
      this.jQueryContent.slideDown();
    }
  }

  toggleContent() {
    this.opened = !this.opened;
    if (this.opened) {
      this.jQueryContent.slideDown();
    } else {
      this.jQueryContent.slideUp();
    }
  }

  back() {
    this._location.back();
  }
}
