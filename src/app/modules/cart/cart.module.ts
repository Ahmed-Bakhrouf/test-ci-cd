import { LayoutModule } from "./../layout/layout.module";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CartRoutingModule } from "./cart-routing.module";
import { SummaryPage } from "./pages/summary.page";
import { InformationsPage } from "./pages/informations.page";
import { ShippingPage } from "./pages/shipping.page";
import { PaymentPage } from "./pages/payment.page";
import { CartCardComponent } from "./components/cart-card.component";
import { ForgotPasswordModalComponent } from "./components/forgot-password-modal.component";
import { SharedModule } from "../shared/shared.module";
import { NgxMatIntlTelInputModule } from "ngx-mat-intl-tel-input";
import { CartService } from "src/app/services/cart.service";
import { AddressPipe } from "src/app/pipes/address.pipe";
import { PaymentPipe } from "src/app/pipes/payment.pipe";

@NgModule({
  imports: [
    CommonModule,
    CartRoutingModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgxMatIntlTelInputModule,
  ],
  declarations: [
    /** Pages  */
    SummaryPage,
    InformationsPage,
    ShippingPage,
    PaymentPage,

    /** Components  */
    CartCardComponent,
  ],
  providers: [CartService, AddressPipe, PaymentPipe],
  exports: [CartCardComponent],
})
export class CartModule {}
