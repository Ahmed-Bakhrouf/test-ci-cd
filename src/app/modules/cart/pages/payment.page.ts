import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import * as $ from "jquery";
import { Router } from "@angular/router";
import { ModalService } from "src/app/services/Shared/modal.service";
import { PaymentMethodService } from "src/app/services/paymentMethod/payment-method.service";
import { Observable } from "rxjs";
import { take, map } from "rxjs/operators";
import { PaymentMethodDA } from "src/app/modelsDeliveryApp/paymentMethodDA";
import { CartService } from "src/app/services/cart.service";
import { DataService } from "src/app/services/Shared/data.service";
import { OrderDA } from "src/app/modelsDeliveryApp/orderDA";

@Component({
  selector: "app-cart-payment-page",
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>
      <app-layout-navbar
        icon="assets/images/cart/payment_icon.svg"
        title="Paiement"
        subtitle="Vous êtes bientôt au bout"
        [backLink]="['/cart', 'shipping']"
      ></app-layout-navbar>

      <app-cart-card title="Moyen de paiement" [opened]="true">
        <div class="PaymentMethodSelector">
          <!--
                    <div class="PaymentMethodSelector-title">Vos moyens de paiement</div>
                    <div class="PaymentMethodSelector-options">
                        <label class="CartRadio">
                            <div class="CartRadio-content">
                                <input class="CartRadio-content-input" type="radio" name="payment_method" [(ngModel)]="payment_method" (ngModelChange)="onPaymentMethodChange()" value="cb_1">
                                <div class="CartRadio-content-radio"></div>
                                <div class="CartRadio-content-label CartRadio-content-label--withIcon">
                                    <div class="CartRadio-content-label-icon">
                                        <img src="assets/images/cart/credit_card_icon.svg" alt="Carte bancaire">
                                    </div>
                                    <div class="CartRadio-content-label-content">
                                        <div class="CartRadio-content-label-content-creditCardTitle">Alix Pechmajou</div>
                                        <div class="CartRadio-content-label-content-creditCardNumbers">12XX XXXX XXXX 4567</div>
                                    </div>
                                </div>
                            </div>
                        </label>
                    </div>
                    <a [routerLink]="['/account', 'add-payment-method']" class="CartCard-button">
                        <div class="CartCard-button-text">
                            Ajouter un moyen de paiement
                        </div>
                        <div class="CartCard-button-icon">
                            <img src="assets/images/cart/add_icon.svg" alt="Add">
                        </div>
                    </a>
                    -->
          <div class="PaymentMethodSelector-options">
            <label
              class="CartRadio"
              *ngFor="let paymentMethod of paymentMethods$ | async"
              [ngClass]="{ 'CartRadio--off': paymentMethod.status == 'onhold' }"
            >
              <div class="CartRadio-content">
                <input
                  class="CartRadio-content-input"
                  type="radio"
                  name="payment_method"
                  [(ngModel)]="payment_method"
                  (ngModelChange)="onPaymentMethodChange(paymentMethod)"
                  [value]="paymentMethod._id"
                />
                <div class="CartRadio-content-radio"></div>
                <div
                  class="CartRadio-content-label CartRadio-content-label--withIcon"
                >
                  <div class="CartRadio-content-label-icon">
                    <img
                      [src]="
                        'assets/images/cart/' +
                        (paymentMethod.title == 'PAYPAL'
                          ? 'paypal_logo.svg'
                          : 'credit_card_icon.svg')
                      "
                      alt="Paypal"
                    />
                  </div>
                  <div class="CartRadio-content-label-content">
                    <div class="CartRadio-content-label-content-text">
                      {{ paymentMethod.title | payment }}
                    </div>
                  </div>
                </div>
              </div>

              <div
                class="CartRadio-subContent"
                *ngIf="paymentMethod.title == 'CREDITCARD'"
                [class]="
                  payment_method == paymentMethod._id
                    ? 'show-subContent'
                    : 'hide-subContent'
                "
              >
                <div class="PaymentForm">
                  <div class="PaymentForm-item">
                    <div class="PaymentForm-item-icon"></div>
                    <div class="PaymentForm-item-input" #cardInfo>
                      <!-- <input
                        type="text"
                        placeholder="XXXX XXXX XXXX XXXX | MM/AA | CVC"
                        name="credit_card"
                      /> -->
                    </div>
                  </div>
                </div>
              </div>
            </label>
          </div>
        </div>
      </app-cart-card>

      <div class="CartCard" *ngIf="currentOrder$ | async; let currentOrder">
        <div class="CartCard-table">
          <div class="CartCard-table-line">
            <div>Sous-total TTC</div>
            <div>
              {{ currentOrder.total_product_price | number: "1.2-2" }} €
            </div>
          </div>
          <div class="CartCard-table-line">
            <div>Livraison</div>
            <div>
              {{ currentOrder.total_delivery_price | number: "1.2-2" }} €
            </div>
          </div>
          <div class="CartCard-table-line">
            <div>Frais de service</div>
            <div>0.00 €</div>
          </div>
          <div class="CartCard-table-line CartCard-table-line--total">
            <div>Total TTC</div>
            <div>{{ currentOrder.total_order_price | number: "1.2-2" }} €</div>
          </div>
        </div>

        <div class="StripeInfos">
          <div class="StripeInfos-text">
            Tous les paiements sont sécurisés et traités par notre partenaire
            Stripe
          </div>
          <div class="StripeInfos-logo">
            <img src="assets/images/cart/stripe_logo.png" alt="Stripe" />
          </div>
        </div>

        <div class="Page-button Page-button--stickBottom">
          <Loading-button
            [disabled]="disableButton"
            (onclick)="submit()"
            [resolving]="resolving"
            label="Payer"
          ></Loading-button>
        </div>
        <div class="Page-button Page-button--placeholder"></div>
      </div>
    </div>
  `,
  styles: [
    `
      .show-subContent {
        display: block;
      }
      .hide-subContent {
        display: none;
      }
    `,
  ],
})
export class PaymentPage implements OnInit {
  payment_method: string;
  disableButton: boolean = false;

  paymentMethods$: Observable<PaymentMethodDA[]>;

  currentOrder$: Observable<OrderDA>;

  card: any;

  resolving = false;

  constructor(
    private router: Router,
    private modalService: ModalService,
    private paymentMethodeService: PaymentMethodService,
    private cartService: CartService,
    private dataService: DataService
  ) {}

  @ViewChild("cardInfo", { static: false }) set content(element: ElementRef) {
    if (element) this.cardInfo = element;
  }
  cardInfo: ElementRef;
  ngOnInit() {
    if (this.payment_method) {
      this.onPaymentMethodChange(null);
    }

    this.currentOrder$ = this.cartService.order$;
    this.paymentMethods$ = this.paymentMethodeService
      .getPaymentMethodsAPI()
      .pipe(
        take(1),
        map((list) => {
          return list.filter((pay) => pay.status != "disabled");
        })
      );
  }

  async ngAfterViewInit() {
    this.card = await this.paymentMethodeService.createCard({});
  }

  async mountCard() {
    this.card.mount(this.cardInfo.nativeElement);
  }

  onPaymentMethodChange(paymentMethod: PaymentMethodDA) {
    if (paymentMethod.title == "CREDITCARD") this.mountCard();
    else this.card.unmount();
    const selector = `input[value="${this.payment_method}"]`;
    $(".CartRadio-subContent").slideUp();
    $(selector).parent().next(".CartRadio-subContent").slideDown();
  }

  async submit() {
    this.resolving = true;
    const clientSecret = this.cartService.code;
    let paymentResult = await this.paymentMethodeService.confirmPayment(
      this.card,
      clientSecret
    );
    if (paymentResult.paymentIntent) {
      // payment success
      this.resolving = false;
      await this.orderSuccess();
      this.cartService.resetOrder();
      this.dataService.emptyCart();
    } else {
      // TODO show error to the customer
      console.log("payment failed");
    }
  }

  async orderSuccess() {
    const title = "Félicitations";
    const text = `
            Nous avons bien reçu votre commande ! <br>
        `;
    const buttonText = "Suivre ma commande";
    const icon = "assets/images/cart/confirm_icon.svg";
    const target = await this.modalService.alert(
      title,
      text,
      buttonText,
      "Retour",
      icon
    );
    if (target === "bigButton") {
      this.router.navigate(["/account", "order-detail"]);
    } else if (target === "cancelButton") {
      this.router.navigate(["/products"]);
    }
  }
}
