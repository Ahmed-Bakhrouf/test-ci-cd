import { Component, AfterViewInit, OnDestroy, OnInit } from "@angular/core";
import * as $ from "jquery";
import {
  ModalService,
  SelectOption,
} from "src/app/services/Shared/modal.service";
import { Router } from "@angular/router";
import { LoginService } from "src/app/services/loginService/login.service";
import { CustomerService } from "src/app/services/Customer/customer.service";
import { take, switchMap } from "rxjs/operators";
import { CustomerDA } from "src/app/modelsDeliveryApp/customerDA";
import { DeliveryMethodService } from "src/app/services/deliveryMethod/delivery-method.service";
import { DeliveryMethodDA } from "src/app/modelsDeliveryApp/deliveryMethodDA";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { CartService } from "src/app/services/cart.service";
import { DataService } from "src/app/services/Shared/data.service";
import { RelayPointService } from "src/app/services/relayPoint/relay-point.service";
import { RelayPointDA } from "src/app/modelsDeliveryApp/relayPointDA";
import { AddressPipe } from "src/app/pipes/address.pipe";

@Component({
  selector: "app-cart-shipping-page",
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>
      <app-layout-navbar
        icon="assets/images/cart/shipping_icon.svg"
        title="Livraison"
        subtitle="Vous êtes bientôt au bout"
        [backLink]="['/cart', 'informations']"
      ></app-layout-navbar>

      <app-cart-card title="Adresse de livraison" [opened]="false">
        <div class="ShippingAddressSelector">
          <div class="ShippingAddressSelector-list" *ngIf="customer">
            <!-- *** -->
            <label class="CartRadio" *ngFor="let address of customer.address">
              <div class="CartRadio-content">
                <input
                  class="CartRadio-content-input"
                  type="radio"
                  name="shipping_address"
                  [(ngModel)]="shipping_address"
                  (ngModelChange)="onShippingAddressChange()"
                  [value]="address._id"
                />
                <div class="CartRadio-content-radio"></div>
                <div class="CartRadio-content-label">
                  <div class="CartRadio-content-label-name">
                    {{ (address.wording ? address.wording : "") | titlecase }}
                  </div>
                  <div class="CartRadio-content-label-visibleAddress">
                    {{ address | address: 1 }} <br />
                    {{ address | address: 2 }}
                  </div>

                  <div class="CartRadio-content-label-hiddenAddress">
                    Appt 150 <br />
                    Interphone 20 <br />
                    {{ customer.email }}
                  </div>
                </div>
              </div>
            </label>
          </div>
          <a
            [routerLink]="['/account', 'add-address']"
            class="ShippingAddressSelector-button"
          >
            <div class="ShippingAddressSelector-button-text">
              Ajouter une nouvelle adresse
            </div>
            <div class="ShippingAddressSelector-button-icon">
              <img src="assets/images/cart/add_icon.svg" alt="Add" />
            </div>
          </a>
        </div>
      </app-cart-card>

      <app-cart-card title="Méthode de livraison" [opened]="true">
        <!-- *** Livraison dans 1h *** -->
        <label
          class="CartRadio"
          *ngFor="let deliveyMode of deliveyModes$ | async"
        >
          <div class="CartRadio-content">
            <input
              #selectedShpping
              class="CartRadio-content-input"
              type="radio"
              name="shipping_method"
              [(ngModel)]="shipping_method"
              (ngModelChange)="
                selectShippingMethod(deliveyMode, selectedShpping)
              "
              [value]="deliveyMode._id"
            />
            <div class="CartRadio-content-radio"></div>
            <div
              class="CartRadio-content-label CartRadio-content-label--withIcon"
            >
              <div class="CartRadio-content-label-icon">
                <img
                  [src]="
                    'assets/images/cart/' +
                    (deliveyMode.code === 'EXPRESS_DELIVERY'
                      ? 'shipping_method1.svg'
                      : deliveyMode.code === 'HOME'
                      ? 'shipping_method2.svg'
                      : 'shipping_method3.svg')
                  "
                  [alt]="deliveyMode.name"
                />
              </div>
              <div class="CartRadio-content-label-content">
                <div class="CartRadio-content-label-content-text">
                  {{ deliveyMode.name }}
                </div>
                <div class="CartRadio-content-label-content-price">
                  {{ deliveyMode.price | number: "1.2-2" }} €
                </div>
              </div>
            </div>
          </div>

          <!-- *** Relay point *** -->
          <div
            class="CartRadio-subContent"
            *ngIf="deliveyMode.code === 'RELAY_POINT'"
          >
            <div class="RelayPointSelector">
              <div class="RelayPointSelector-map">
                <app-map
                  [relayPoints]="relayPoints$ | async"
                  [readOnly]="false"
                  (selectedRelayPoint)="selectShippingAdd($event)"
                ></app-map>
              </div>
              <div class="RelayPointSelector-relay" *ngIf="selectedRelayPoint">
                <div class="RelayPointSelector-relay-title">
                  Point relai {{ selectedRelayPoint.name | titlecase }}
                </div>
                <div class="RelayPointSelector-relay-address">
                  {{ selectedRelayPoint.adresse }}
                </div>
                <div class="RelayPointSelector-relay-schedule">
                  <ng-container
                    *ngFor="let workingDay of selectedRelayPoint.workingDays"
                  >
                    {{ workingDay.day }} de {{ workingDay.start }} à
                    {{ workingDay.end }}
                    <br />
                  </ng-container>
                </div>
                <button
                  class="RelayPointSelector-relay-button"
                  (click)="confirmRelayAddress()"
                >
                  Choisir ce point relai
                </button>
              </div>
            </div>
          </div>

          <!-- *** Planned delivery *** -->

          <div class="CartRadio-subContent" *ngIf="deliveyMode.code === 'HOME'">
            <div class="ShippingScheduleSelector">
              <div class="ShippingScheduleSelector-top">
                Sélectionnez votre créneau de livraison
              </div>
              <div class="ShippingScheduleSelector-bottom">
                <div
                  class="ShippingScheduleSelector-bottom-item"
                  (click)="selectDay()"
                >
                  <div class="ShippingScheduleSelector-bottom-item-top">
                    Jour
                  </div>
                  <div class="ShippingScheduleSelector-bottom-item-bottom">
                    {{ shipping_day ? shipping_day.label : "-" }}
                  </div>
                </div>
                <div
                  class="ShippingScheduleSelector-bottom-item"
                  (click)="selectSlot()"
                >
                  <div class="ShippingScheduleSelector-bottom-item-top">
                    Créneau
                  </div>
                  <div class="ShippingScheduleSelector-bottom-item-bottom">
                    {{ shipping_slot ? shipping_slot.label : "-" }}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </label>

        <!-- *** Planifier ma livraison *** 
        <label class="CartRadio">
          <div class="CartRadio-content">
            <input
              class="CartRadio-content-input"
              type="radio"
              name="shipping_method"
              [(ngModel)]="shipping_method"
              (ngModelChange)="onShippingMethodChange()"
              value="method_2"
            />
            <div class="CartRadio-content-radio"></div>
            <div
              class="CartRadio-content-label CartRadio-content-label--withIcon"
            >
              <div class="CartRadio-content-label-icon">
                <img
                  src="assets/images/cart/shipping_method2.svg"
                  alt="Planifier ma livraison"
                />
              </div>
              <div class="CartRadio-content-label-content">
                <div class="CartRadio-content-label-content-text">
                  Planifier ma livraison
                </div>
                <div class="CartRadio-content-label-content-price">4.99 €</div>
              </div>
            </div>
          </div>
          <div class="CartRadio-subContent">
            <div class="ShippingScheduleSelector">
              <div class="ShippingScheduleSelector-top">
                Sélectionnez votre créneau de livraison
              </div>
              <div class="ShippingScheduleSelector-bottom">
                <div
                  class="ShippingScheduleSelector-bottom-item"
                  (click)="selectDay()"
                >
                  <div class="ShippingScheduleSelector-bottom-item-top">
                    Jour
                  </div>
                  <div class="ShippingScheduleSelector-bottom-item-bottom">
                    {{ shipping_day ? shipping_day.label : "-" }}
                  </div>
                </div>
                <div
                  class="ShippingScheduleSelector-bottom-item"
                  (click)="selectSlot()"
                >
                  <div class="ShippingScheduleSelector-bottom-item-top">
                    Créneau
                  </div>
                  <div class="ShippingScheduleSelector-bottom-item-bottom">
                    {{ shipping_slot ? shipping_slot.label : "-" }}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </label>
        -->
        <!-- 
        <label class="CartRadio">
          <div class="CartRadio-content">
            <input
              class="CartRadio-content-input"
              type="radio"
              name="shipping_method"
              [(ngModel)]="shipping_method"
              (ngModelChange)="onShippingMethodChange()"
              value="method_3"
            />
            <div class="CartRadio-content-radio"></div>
            <div
              class="CartRadio-content-label CartRadio-content-label--withIcon"
            >
              <div class="CartRadio-content-label-icon">
                <img
                  src="assets/images/cart/shipping_method3.svg"
                  alt="Livraison point relai"
                />
              </div>
              <div class="CartRadio-content-label-content">
                <div class="CartRadio-content-label-content-text">
                  Livraison point relai
                </div>
                <div class="CartRadio-content-label-content-price">4.99 €</div>
              </div>
            </div>
          </div>
          <div class="CartRadio-subContent">
            <div class="RelayPointSelector">
              <div class="RelayPointSelector-map"></div>
              <div class="RelayPointSelector-relay">
                <div class="RelayPointSelector-relay-title">
                  Point relai Les Oliviers
                </div>
                <div class="RelayPointSelector-relay-address">
                  27 avenue de Fontainebleau <br />
                  94270 Le Kremlin-Bicêtre
                </div>
                <div class="RelayPointSelector-relay-schedule">
                  Du lundi au vendredi de 10h à 20h <br />
                  Le samedi de 14h à 18h
                </div>
                <button class="RelayPointSelector-relay-button">
                  Choisir ce point relai
                </button>
              </div>
            </div>
          </div>
        </label> -->
      </app-cart-card>

      <div class="Page-button Page-button--stickBottom">
        <Loading-button
          [disabled]="!disable"
          (onclick)="next()"
          [resolving]="resolving"
          label="Suivant"
        ></Loading-button>
      </div>
      <div class="Page-button Page-button--placeholder"></div>
    </div>
  `,
})
export class ShippingPage implements OnInit, AfterViewInit, OnDestroy {
  customer: CustomerDA;
  shipping_address: any;
  shipping_method: string;

  deliveyModes$: Observable<DeliveryMethodDA[]>;
  deliveyMode: DeliveryMethodDA;

  relayPoints$: Observable<RelayPointDA[]>;
  selectedRelayPoint: RelayPointDA;
  selectedRelayAddress: string;

  shipping_day: SelectOption = null;
  shipping_slot: SelectOption;

  resolving = false;

  get disable() {
    return (
      (this.deliveyMode &&
        this.deliveyMode.code == "RELAY_POINT" &&
        this.selectedRelayAddress) ||
      (this.deliveyMode && this.shipping_address)
    );
  }

  constructor(
    private modalService: ModalService,
    private router: Router,
    private loginService: LoginService,
    private customerService: CustomerService,
    private deliveryMethodeService: DeliveryMethodService,
    private cartService: CartService,
    private dataService: DataService,
    private relayPointService: RelayPointService,
    private addressPipe: AddressPipe
  ) {}

  ngOnInit() {
    this.getCustomer();
    this.deliveyModes$ = this.deliveryMethodeService.getDeliveryMethodList();
    this.relayPoints$ = this.relayPointService.getRelayPointList();
  }

  ngAfterViewInit() {
    $(".CartRadio-content-label-hiddenAddress, .CartRadio-subContent").on(
      "classChanged",
      function () {
        if ($(this).hasClass("slide_down")) {
          $(this).slideDown();
          $(this).removeClass("slide_down");
        } else {
          $(this).slideUp();
        }
      }
    );
    $(".ShippingScheduleSelector-bottom-item").on("click", function () {
      $(".ShippingScheduleSelector-bottom-item > select").click();
    });
  }

  ngOnDestroy() {
    $(".ShippingScheduleSelector-bottom-item").off("click");
  }

  onShippingAddressChange() {
    const selector = `input[value="${this.shipping_address}"] ~ .CartRadio-content-label > .CartRadio-content-label-hiddenAddress`;
    $(".CartRadio-content-label-hiddenAddress").slideUp();
    $(selector).slideDown();
  }

  selectShippingMethod(deliveyMode, radioRef) {
    this.deliveyMode = deliveyMode;
    this.onShippingMethodChange(radioRef);
  }

  onShippingMethodChange(radioRef) {
    const selector = $(radioRef);
    $(".CartRadio-subContent").slideUp();
    $(selector).parent().next(".CartRadio-subContent").slideDown();
  }

  selectShippingAdd(relayPoint: RelayPointDA) {
    if (
      !this.selectedRelayPoint ||
      relayPoint._id != this.selectedRelayPoint._id
    ) {
      this.selectedRelayPoint = relayPoint;
      this.selectedRelayAddress = null;
    }
  }

  confirmRelayAddress() {
    this.selectedRelayAddress = this.selectedRelayPoint.adresse;
  }

  async selectDay() {
    const options: SelectOption[] = [
      { label: "24/08", value: "2408" },
      { label: "25/08", value: "2508" },
      { label: "26/08", value: "2608" },
      { label: "27/08", value: "2708" },
    ];
    const title = "Sélectionnez un jour de livraison";
    const day = await this.modalService.select(
      options,
      this.shipping_day,
      title
    );
    if (day) {
      this.shipping_day = day;
    }
  }

  async selectSlot() {
    const options: SelectOption[] = [
      { label: "10-12H", value: "1012" },
      { label: "12-14H", value: "1214" },
      { label: "14-16H", value: "1416" },
    ];
    const title = "Sélectionnez un créneau horaire";
    const slot = await this.modalService.select(
      options,
      this.shipping_slot,
      title
    );
    if (slot) {
      this.shipping_slot = slot;
    }
  }

  next() {
    this.resolving = true;
    let delivery_adress: string;

    if (this.deliveyMode.code == "RELAY_POINT" && this.selectedRelayAddress)
      delivery_adress = this.selectedRelayAddress;
    else if (this.shipping_method && this.shipping_address) {
      const address = this.customer.address.find(
        (add) => add._id == this.shipping_address
      );
      delivery_adress = this.addressPipe.transform(address);
    }

    this.cartService.setOrderCustomer(this.customer._id);
    this.cartService.setOrderDeliveryMode(this.deliveyMode);
    this.cartService.setOrderAddress(delivery_adress);
    this.cartService.passOrder().subscribe(({ code }) => {
      if (code.includes("pi_")) {
        this.dataService.emptyCart();
        this.router.navigate(["/cart", "payment"]);
      }
      // TODO show error to the customer
      else console.log("error with passing order || creating payment intent");
      this.resolving = false;
    });
  }

  getCustomer() {
    if (this.loginService.isAuth()) {
      this.customerService
        .getCustomer(this.loginService.getUser()._id)
        .pipe(
          take(1),
          switchMap((customer) => {
            this.customer = customer;
            this.customer.address = [];
            return this.customerService.getAdresse(this.customer._id);
          })
        )
        .subscribe((address) => {
          this.customer.address = address;
        });
    }
  }
}
