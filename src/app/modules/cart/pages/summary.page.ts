import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { DataService } from "src/app/services/Shared/data.service";
import { CartProductDA } from "src/app/modelsDeliveryApp/CartProductDA";
import { LoginService } from "src/app/services/loginService/login.service";
import { Observable } from "rxjs";

@Component({
  selector: "app-cart-summary-page",
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>
      <app-layout-navbar
        icon="assets/images/cart/my_cart_icon.svg"
        title="Mon panier"
        [subtitle]="(cartProductNumber$ | async) + ' produits'"
        [backLink]="['/products']"
      ></app-layout-navbar>

      <div class="CartSummary">
        <!-- *** -->
        <div class="CartSummary-item" *ngFor="let item of cart$ | async">
          <div class="CartSummary-item-image" style="text-align: center;">
            <img [src]="item.image" [alt]="item.title" />
          </div>
          <div class="CartSummary-item-content">
            <div class="CartSummary-item-content-top">
              <div
                class="CartSummary-item-content-top-title"
                style="white-space: inherit;"
              >
                {{ item.title }}
              </div>
              <button
                class="CartSummary-item-content-top-remove"
                (click)="onRemoveProductsFromCart(item._id)"
              >
                <img src="assets/images/cart/trash_icon.svg" alt="Trash" />
              </button>
            </div>
            <div class="CartSummary-item-content-subtitle">
              {{
                item.unitValue + " " + (item.unit ? item.unit.title : "pièce")
              }}
            </div>
            <div class="CartSummary-item-content-merchant">
              Vendu par Epicerie toto
            </div>
            <div class="CartSummary-item-content-bottom">
              <div class="CartSummary-item-content-bottom-quantity">
                <div
                  class="CartSummary-item-content-bottom-quantity-action"
                  (click)="onUpdateQuantity(0, item._id)"
                >
                  <img src="assets/images/cart/minus_icon.svg" alt="Minus" />
                </div>
                <div class="CartSummary-item-content-bottom-quantity-value">
                  {{ item.Quantity }}
                </div>
                <div
                  class="CartSummary-item-content-bottom-quantity-action"
                  (click)="onUpdateQuantity(1, item._id)"
                >
                  <img src="assets/images/cart/plus_icon.svg" alt="Plus" />
                </div>
              </div>
              <div class="CartSummary-item-content-bottom-price">
                {{ item.price * item.Quantity | number: "1.2-2" }} €
              </div>
            </div>
          </div>
        </div>
        <!-- *** -->

        <!-- *** Total *** -->
        <div class="CartSummary-total">
          <div class="CartSummary-total-caption">Total</div>
          <div class="CartSummary-total-value">
            {{ cartPrice$ | async | number: "1.2-2" }} € TTC
          </div>
        </div>
      </div>

      <div class="Page-button Page-button--stickBottom">
        <button
          [disabled]="(cartProductNumber$ | async) == 0"
          (click)="next()"
          class="Button Button--bigger Button--red"
          [ngClass]="{ 'Button--off': (cartProductNumber$ | async) == 0 }"
        >
          Suivant
        </button>
      </div>
      <div class="Page-button Page-button--placeholder"></div>
    </div>
  `,
})
export class SummaryPage implements OnInit {
  disableButton: boolean = false;

  currency = "EUR";

  cart$: Observable<CartProductDA[]>;
  cartPrice$: Observable<number>;
  cartProductNumber$: Observable<number>;

  constructor(
    private router: Router,
    private dataService: DataService,
    private loginService: LoginService
  ) {}

  ngOnInit() {
    this.currency = "EUR";
    this.cartProductNumber$ = this.dataService.cartProductNumber$;
    this.cart$ = this.dataService.cart;
    this.cartPrice$ = this.dataService.cartPrice$;
  }

  onRemoveProductsFromCart(productId: string) {
    this.dataService.removeProductsFromCart(productId);
  }

  onUpdateQuantity(type, productId) {
    this.dataService.updateQuantity(type, productId);
  }

  next() {
    if (this.loginService.getUser())
      this.router.navigate(["/cart", "shipping"]);
    else this.router.navigate(["/cart", "informations"]);
  }
}
