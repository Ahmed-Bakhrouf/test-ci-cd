import { Component, OnInit, AfterContentInit } from "@angular/core";
import { Router } from "@angular/router";
import { ModalService } from "src/app/services/Shared/modal.service";
import { ForgotPasswordModalComponent } from "../components/forgot-password-modal.component";
import { EnterPasswordModalComponent } from "../components/enter-password-modal.component";
import { LoginService } from "src/app/services/loginService/login.service";
import { take, map } from "rxjs/operators";
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidatorFn,
  ValidationErrors,
} from "@angular/forms";
import { CustomerService } from "src/app/services/Customer/customer.service";
import { parsePhoneNumberFromString } from "libphonenumber-js";
import { CreateCustomerDto } from "../../../dto/createCustomerDto";
import { CreateCustomerResponse } from "src/app/dto/createCustomerResponse";
import { Location } from "@angular/common";
import { Country } from "src/app/modelsDeliveryApp/country";
import { Observable } from "rxjs";

@Component({
  selector: "app-cart-informations-page",
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>
      <app-layout-navbar
        icon="assets/images/cart/informations_icon.svg"
        title="Mes informations"
        subtitle="La sécurité avant tout"
        [backLink]="['/cart']"
      ></app-layout-navbar>

      <app-cart-card title="Déjà client ?" [opened]="false">
        <form class="Form">
          <div class="Form-field">
            <div class="Form-field-input Form-field-input--withLeftIcon">
              <img src="assets/images/france.svg" alt="France" />
              <input
                type="text"
                name="mobile_phone"
                [(ngModel)]="mobile_phone"
              />
              <label>Numéro de mobile</label>
            </div>
          </div>
          <div class="Form-field">
            <div class="Form-field-input">
              <input type="password" name="password" [(ngModel)]="password" />
              <label>Mot de passe</label>
            </div>
          </div>
          <div class="Form-links">
            <button (click)="forgotPassword()" class="Form-links-item">
              Mot de passe oublié ?
            </button>
            <a href="#" class="Form-links-item">Pas encore inscrit ?</a>
          </div>
        </form>
      </app-cart-card>

      <!-- register form -->

      <app-cart-card title="Nouveau client ?" [opened]="false">
        <form class="Form" [formGroup]="signUpForm">
          <div class="Form-field Form-field-row">
            <div class="Form-field-input row-input">
              <input type="text" formControlName="firstname" />
              <label>Prénom <span>*</span></label>
            </div>
            <div class="Form-field-input row-input">
              <input type="text" formControlName="lastname" />
              <label>Nom <span>*</span></label>
            </div>
          </div>
          <ng-container formGroupName="address">
            <div class="Form-field">
              <div class="Form-field-input">
                <input type="text" formControlName="line1" />
                <label>Adresse <span>*</span></label>
              </div>
            </div>
            <div class="Form-field">
              <div class="Form-field-input">
                <input type="text" formControlName="line2" />
                <label>Complément d'adresse</label>
              </div>
            </div>
            <div class="Form-field Form-field-row">
              <div class="Form-field-input row-input">
                <input type="text" formControlName="postalCode" />
                <label>Code postal <span>*</span></label>
              </div>
              <div class="Form-field-input row-input">
                <input type="text" formControlName="town" />
                <label>Ville <span>*</span></label>
              </div>
            </div>
            <div class="Form-field">
              <div class="Form-field-input">
                <input
                  type="text"
                  [class]="'not_empty'"
                  value="France"
                  disabled="disabled"
                  name="country"
                />
                <label>Pays <span>*</span></label>
              </div>
            </div>
          </ng-container>

          <div class="Form-field">
            <div class="Form-field-input">
              <!-- <img src="assets/images/france.svg" alt="France" />
               <input type="text" formControlName="mobile_number" /> 
               <label>Numéro de mobile <span>*</span></label>
               -->
              <ngx-mat-intl-tel-input
                [onlyCountries]="['fr', 'mq', 'gf', 'gp']"
                name="phone"
                cssClass="tel-input"
                (ngModelChange)="onPhoneNumberChange($event)"
                [(ngModel)]="phoneNumber"
                [ngModelOptions]="{ standalone: true }"
              ></ngx-mat-intl-tel-input>
            </div>
          </div>
          <div class="Form-field">
            <div class="Form-field-input">
              <input type="text" formControlName="email" />
              <label>Adresse e-mail <span>*</span></label>
            </div>
            <div class="Form-field-error">Exemple de message d'erreur</div>
          </div>
          <div class="Form-field">
            <label class="Checkbox">
              <input
                class="Checkbox-input"
                type="checkbox"
                name="conditions"
                [(ngModel)]="conditionsAccepted"
                [ngModelOptions]="{ standalone: true }"
              />
              <div class="Checkbox-checkbox"></div>
              <div class="Checkbox-label">
                J'accepte les conditions générales de vente
              </div>
            </label>
          </div>
          <div class="Form-field">
            <label class="Checkbox">
              <input
                class="Checkbox-input"
                [(ngModel)]="other_billing_address"
                [ngModelOptions]="{ standalone: true }"
                type="checkbox"
                name="other_billing_address"
              />
              <div class="Checkbox-checkbox"></div>
              <div class="Checkbox-label">Autre adresse de facturation</div>
            </label>
          </div>
        </form>
      </app-cart-card>

      <app-cart-card
        *ngIf="other_billing_address"
        title="Adresse de facturation"
        [opened]="true"
      >
        <form class="Form">
          <div class="Form-field">
            <div class="Form-field-input">
              <input type="text" name="first_name" />
              <label>Prénom <span>*</span></label>
            </div>
          </div>
          <div class="Form-field">
            <div class="Form-field-input">
              <input type="text" name="last_name" />
              <label>Nom <span>*</span></label>
            </div>
          </div>
          <div class="Form-field">
            <div class="Form-field-input">
              <input type="text" name="address1" />
              <label>Adresse <span>*</span></label>
            </div>
          </div>
          <div class="Form-field">
            <div class="Form-field-input">
              <input type="text" name="address2" />
              <label>Complément d'adresse</label>
            </div>
          </div>
          <div class="Form-field">
            <div class="Form-field-input">
              <input type="text" name="zip_code" />
              <label>Code postal <span>*</span></label>
            </div>
          </div>
          <div class="Form-field">
            <div class="Form-field-input">
              <input type="text" name="city" />
              <label>Ville <span>*</span></label>
            </div>
          </div>
          <div class="Form-field">
            <div class="Form-field-input">
              <input type="text" name="country" />
              <label>Pays <span>*</span></label>
            </div>
          </div>
        </form>
      </app-cart-card>

      <div class="Page-button Page-button--stickBottom">
        <Loading-button
          [disabled]="disable"
          (onclick)="next()"
          [resolving]="resolving"
          label="Suivant"
        ></Loading-button>
      </div>
      <div class="Page-button Page-button--placeholder"></div>
    </div>
  `,
  styles: [
    `
      .row-input {
        flex: 0.48;
      }
      .Form-field-row {
        flex-direction: row;
        justify-content: space-between;
      }
      ::ng-deep .ngx-mat-tel-input-container {
        height: 100%;
      }
      ::ng-deep .tel-input {
        flex: 1 !important;
        color: #061a30 !important;
        font-size: 1.5rem !important;
        font-weight: 600 !important;
        background-color: transparent !important;
        border: 0 !important;
        padding: 0 6px 0 90px !important;
        height: 100% !important;
        width: 100% !important;
      }
      ::ng-deep .country-selector {
        opacity: 1 !important;
      }
      ::ng-deep .country-selector-code {
        color: #061a30 !important;
        font-size: 1.5rem !important;
        font-weight: 600 !important;
      }
    `,
  ],
})
export class InformationsPage implements OnInit {
  mobile_phone: string;
  password: string;

  conditionsAccepted = false;
  other_billing_address: boolean = false;

  countries$: Observable<Country[]>;

  get disable() {
    return !(
      (this.mobile_phone && this.password && this.password.length > 4) ||
      (this.conditionsAccepted &&
        this.signUpForm.errors &&
        this.signUpForm.errors.validButNoPassword)
    );
  }

  resolving = false;

  phoneNumber;

  constructor(
    private fb: FormBuilder,
    private modalService: ModalService,
    private router: Router,
    private _location: Location,
    private loginService: LoginService,
    private customerService: CustomerService
  ) {}

  signUpForm: FormGroup;
  get f() {
    return this.signUpForm.controls;
  }

  onPhoneNumberChange(c: string) {
    if (c != null && c.length > 0) {
      const phoneNumber = parsePhoneNumberFromString(c);
      const num = {
        nationalNumber: phoneNumber.formatNational().replace(/ /g, ""),
        number: phoneNumber
          .formatInternational()
          .replace("+", "")
          .replace(/ /g, ""),
        dialCode: "+" + phoneNumber.countryCallingCode,
      };
      if (phoneNumber.isValid()) {
        this.f.mobile_number.setValue(num.nationalNumber);
        this.f.mobile_country_code.setValue(num.dialCode);
        this.f.full_mobile_number.setValue(num.number);
      }
    }
  }

  ngOnInit() {
    this.signUpForm = this.fb.group(
      {
        firstname: [
          "",
          [Validators.required, Validators.pattern("[a-zA-Z ]*")],
        ],
        lastname: ["", [Validators.required, Validators.pattern("[a-zA-Z ]*")]],
        mobile_number: ["", [Validators.required]],
        mobile_country_code: ["", Validators.required],
        full_mobile_number: ["", Validators.required],
        email: ["", [Validators.required, Validators.email]],
        password: ["", Validators.required],
        address: this.fb.group({
          line1: ["", Validators.required],
          line2: [""],
          postalCode: ["", Validators.required],
          town: ["", Validators.required],
          country: ["", Validators.required],
        }),
      },
      { validators: registerValidation }
    );
    this.customerService
      .getCountry()
      .pipe(
        take(1),
        map((countries) => {
          if (countries && countries[0]) {
            (<FormGroup>this.signUpForm.get("address"))
              .get("country")
              .setValue(countries[0]._id);
            return countries;
          } else return [];
        })
      )
      .subscribe();
  }

  forgotPassword() {
    const instance: ForgotPasswordModalComponent = this.modalService.open(
      ForgotPasswordModalComponent
    );
    instance.onSubmit.subscribe(async (mobile_phone: string) => {
      await instance.close();
      this.enterPassword(mobile_phone);
    });
    instance.onCancel.subscribe(() => {
      instance.close();
    });
  }

  async enterPassword(mobile_phone: string): Promise<boolean> {
    return new Promise((resolve) => {
      const instance: EnterPasswordModalComponent = this.modalService.open(
        EnterPasswordModalComponent,
        { mobile_phone }
      );
      instance.onSubmit.subscribe((password: string) => {
        this.f.password.setValue(password);
        instance.close();
        resolve(true);
      });
      instance.onCancel.subscribe(() => {
        instance.close();
        resolve(false);
      });
    });
  }

  next() {
    if (this.mobile_phone && this.password) {
      this.login(this.mobile_phone, this.password);
    } else if (
      this.signUpForm.errors &&
      this.signUpForm.errors.validButNoPassword &&
      this.conditionsAccepted
    ) {
      this.checkUniqueness();
    }
  }

  wrongPassword() {
    const icon = "assets/images/cart/modal_icon.svg";
    const title = "Erreur";
    const text = `Numéro de mobile ou mot de passe incorrect !`;
    const buttonText = "OK";
    this.modalService.alert(title, text, buttonText, null, icon);
  }

  accountAlreadyExists() {
    const icon = "assets/images/cart/modal_icon.svg";
    const title = "Erreur";
    const text = ` Un client avec ce numéro de mobile est déjà incsrit !`;
    const buttonText = "OK";
    this.modalService.alert(title, text, buttonText, null, icon);
  }

  checkUniqueness() {
    const { full_mobile_number } = this.signUpForm.value;
    this.customerService
      .verifyUniqueness(full_mobile_number)
      .subscribe((unique) => {
        if (!unique.exists) {
          this.enterPassword("+" + full_mobile_number).then((success) => {
            if (success) this.signUp();
          });
        } else {
          this.accountAlreadyExists();
        }
      });
  }

  login(mobileNumber: string, password: string) {
    this.resolving = true;
    this.loginService
      .login(mobileNumber, password)
      .pipe(take(1))
      .subscribe(
        (user) => {
          this.resolving = false;
          this.proceed();
        },
        (err) => {
          this.password = "";
          this.wrongPassword();
          this.resolving = false;
        }
      );
  }

  async signUp() {
    this.resolving = true;
    const customer = new CreateCustomerDto(this.signUpForm.value);
    const response: CreateCustomerResponse = await this.customerService.create(
      customer
    );
    if (response && response.customer) {
      this.login(customer.mobile_number, customer.password);
    }
    this.resolving = false;
  }

  proceed() {
    if (this.router.url.includes("signin")) this._location.back();
    else this.router.navigate(["/cart", "shipping"]);
  }
}

export const registerValidation: ValidatorFn = (
  control: FormGroup
): ValidationErrors | null => {
  const address = control.get("address");
  const firstname = control.get("firstname");
  const lastname = control.get("lastname");
  const mobile_number = control.get("mobile_number");
  const mobile_country_code = control.get("mobile_country_code");
  const full_mobile_number = control.get("full_mobile_number");
  const email = control.get("email");
  return address &&
    firstname &&
    lastname &&
    mobile_number &&
    mobile_country_code &&
    full_mobile_number &&
    email &&
    firstname.valid &&
    lastname.valid &&
    mobile_number.valid &&
    mobile_country_code.valid &&
    full_mobile_number.valid &&
    email.valid &&
    address.valid
    ? { validButNoPassword: true }
    : null;
};
