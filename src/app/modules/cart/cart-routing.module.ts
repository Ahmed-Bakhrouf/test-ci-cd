import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SummaryPage } from './pages/summary.page';
import { InformationsPage } from './pages/informations.page';
import { ShippingPage } from './pages/shipping.page';
import { PaymentPage } from './pages/payment.page';

const routes: Routes = [
    { path: '', component: SummaryPage },
    { path: 'informations', component: InformationsPage },
    { path: 'shipping', component: ShippingPage },
    { path: 'payment', component: PaymentPage },
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class CartRoutingModule { }
