import { CategoryItemComponent } from "./components/category-item.component";
import { LayoutModule } from "./../layout/layout.module";
import { BannerComponent } from "./components/banner.component";
import { ProductsListComponent } from "./components/products-list.component";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ProductsRoutingModule } from "./products-routing.module";
import { HomePage } from "./pages/home.page";
import { FamilyPage } from "./pages/family.page";
import { CategoryPage } from "./pages/category.page";
import { SubCategoryPage } from "././pages/sub-category.page";
import { ProductPage } from "./pages/product.page";
import { CategoriesComponent } from "./components/categories.component";
import { ProductItemComponent } from "./components/product-item.component";
import { FilterPanelComponent } from "./components/filter-panel.component";
import { SearchResultsPage } from "./pages/search-results.page";
import { FormsModule } from "@angular/forms";
import { FooterComponent } from "../layout/footer.component";

@NgModule({
  imports: [CommonModule, ProductsRoutingModule, LayoutModule, FormsModule],
  declarations: [
    /** Pages  */
    HomePage,
    FamilyPage,
    CategoryPage,
    SubCategoryPage,
    ProductPage,
    SearchResultsPage,

    /** Components  */
    FilterPanelComponent,
    ProductsListComponent,
    CategoriesComponent,
    BannerComponent,
    CategoriesComponent,
    CategoryItemComponent,
    ProductItemComponent,
    FooterComponent,
  ],
  exports: [BannerComponent, ProductsListComponent],
})
export class ProductsModule {}
