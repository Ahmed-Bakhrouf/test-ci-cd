import { Component, OnInit, HostListener } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { ProductService } from "src/app/services/Product/product.service";
import { Observable } from "rxjs";
import { ProductDA } from "src/app/modelsDeliveryApp/productDA";
import { pluck, tap } from "rxjs/operators";
import { CategoryService } from "src/app/services/Category/category.service";

@Component({
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>

      <app-layout-navbar
        [title]="subCategoryTitle"
        numberOfProducts="1"
        [filter]="true"
        [backLink]="['/products', 'category']"
        (onFilter)="showFilterPanel = true"
      ></app-layout-navbar>

      <app-products-filter-panel
        [visible]="showFilterPanel"
        (onFilter)="onFilter()"
        (onCancel)="showFilterPanel = false"
      ></app-products-filter-panel>

      <app-products-products-list
        [loading]="loading"
        [products]="products$ | async"
      ></app-products-products-list>
    </div>
  `,
})
export class SubCategoryPage implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private categoryService: CategoryService
  ) {}

  showFilterPanel: boolean = false;

  loading = true;

  products$: Observable<ProductDA[]>;

  subCategoryTitle: string = "";

  onFilter() {
    this.showFilterPanel = false;
  }

  ngOnInit() {
    let id = this.route.snapshot.paramMap.get("sub-categoryId");
    console.log("fam id", id);
    this.products$ = this.productService
      .getProductBySubcategoryAPI(0, 1000, id)
      .pipe(
        tap(() => (this.loading = false)),
        pluck("items")
      );
    this.categoryService.getSubCategories(id).subscribe((subCategory) => {
      this.subCategoryTitle = subCategory[0].title;
    });
  }
}
