import {
  Component,
  AfterViewInit,
  HostListener,
  OnDestroy,
  OnInit,
  ElementRef,
  ViewChild,
} from "@angular/core";
import * as $ from "jquery";
import { ActivatedRoute } from "@angular/router";
import { ProductService } from "src/app/services/Product/product.service";
import { CartProductDA } from "src/app/modelsDeliveryApp/CartProductDA";
import { ProductDA } from "src/app/modelsDeliveryApp/productDA";
import { DataService } from "src/app/services/Shared/data.service";

@Component({
  template: `
    <div class="Page" *ngIf="product">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>

      <app-layout-navbar
        [title]="product.title"
        [backLink]="['/products', 'category']"
        [stock]="product.stock"
        price1="{{ product.price | number: '1.2-2' }} €"
        price2="{{ product.price | number: '1.2-2' }} € / {{
          product.unitValue +
            ' ' +
            (product.unit ? product.unit.title : 'pièce')
        }}"
      ></app-layout-navbar>

      <div class="ProductCard">
        <div class="ProductCard-image">
          <img
            #imageObject
            [src]="product.image"
            [alt]="product.title"
            style="max-width:50%"
          />
        </div>
      </div>

      <div class="Page-button">
        <button
          class="Button Button--bigger Button--red"
          (click)="addProductToCart()"
        >
          Ajouter au panier
        </button>
      </div>
      <div
        class="Page-button Page-button--stickBottom"
        [ngClass]="{ 'Page-button--stickBottom--hidden': !stickBottom }"
      >
        <button
          class="Button Button--bigger Button--red"
          (click)="addProductToCart()"
        >
          Ajouter au panier
        </button>
      </div>

      <div class="ProductCard">
        <div class="ProductCard-title">
          <div class="ProductCard-title-icon">
            <img
              src="assets/images/product_page/ic_question.svg"
              alt="Question"
            />
          </div>
          <div class="ProductCard-title-text">Descriptif du produit</div>
        </div>
        <div class="ProductCard-contentText">{{ product.description }}.</div>
      </div>

      <div class="ProductCard">
        <div class="ProductCard-title">
          <div class="ProductCard-title-icon">
            <img src="assets/images/product_page/ic_info.svg" alt="Info" />
          </div>
          <div class="ProductCard-title-text">Informations sur le produit</div>
        </div>
        <div class="ProductCard-contentText">{{ product.title }}.</div>
      </div>

      <div class="ProductCard">
        <div class="ProductCard-title">
          <div class="ProductCard-title-icon">
            <img src="assets/images/product_page/ic_cog.svg" alt="Cog" />
          </div>
          <div class="ProductCard-title-text">Caractéristiques du produit</div>
        </div>
        <div class="ProductCard-contentTable">
          <div
            class="ProductCard-contentTable-item"
            *ngIf="product.merchant?.trade_name"
          >
            <div class="ProductCard-contentTable-item-left">Vendeur</div>
            <div class="ProductCard-contentTable-item-right">
              <a href="#">{{ product.merchant.trade_name }}</a>
            </div>
          </div>
          <div class="ProductCard-contentTable-item" *ngIf="product.brand">
            <div class="ProductCard-contentTable-item-left">Marque</div>
            <div class="ProductCard-contentTable-item-right">
              {{ product.brand }}
            </div>
          </div>
          <div class="ProductCard-contentTable-item">
            <div class="ProductCard-contentTable-item-left">Quantité</div>
            <div class="ProductCard-contentTable-item-right">
              {{
                product.unitValue +
                  " " +
                  (product.unit ? product.unit.title : "pièce")
              }}
            </div>
          </div>
        </div>
      </div>

      <div
        *ngIf="stickBottom"
        class="Page-button Page-button--placeholder"
      ></div>
    </div>
  `,
})
export class ProductPage implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild("imageObject", { static: false }) imageObject: ElementRef;
  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private dataService: DataService
  ) {}

  stickBottom: boolean = false;
  page: any;

  product: ProductDA;
  ngOnInit() {
    let productID = this.route.snapshot.paramMap.get("productID");
    this.productService.getProductAPI(productID).subscribe((p) => {
      this.product = p.items[0];
    });
  }

  ngAfterViewInit() {
    this.page = $(".ng-sidebar__content");
    this.page.on("scroll", () => {
      const scroll = this.page.scrollTop();
      this.stickBottom = scroll > 0;
    });
  }

  ngOnDestroy() {
    this.page.off("scroll");
  }

  addProductToCart() {
    this.dataService.addProductToCart(this.product);
    this.addToCart();
  }
  addToCart() {
    const image = $(this.imageObject.nativeElement);
    const clone = image.clone();
    clone.css({ transition: "all .8s ease" });
    let { top: topStart, left: leftStart } = image.offset();
    const cssStart = {
      width: image.width(),
      position: "fixed",
      top: topStart,
      left: leftStart,
    };
    clone.css(cssStart);

    $("body").append(clone);

    const header_cart = $("#header_cart");

    const { top: topEnd, left: leftEnd } = header_cart.offset();
    const cssEnd = {
      width: 0,
      top: topEnd,
      left: leftEnd,
    };
    clone.css(cssEnd);

    setTimeout(() => {
      clone.css({ opacity: 0 });
    }, 600);
  }
}
