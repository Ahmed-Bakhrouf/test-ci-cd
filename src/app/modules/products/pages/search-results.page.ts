import { Component, OnInit, HostListener, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { distinctUntilChanged, filter, pluck, switchMap } from "rxjs/operators";
import { of, Subscription } from "rxjs";
import { ProductService } from "../../../services/Product/product.service";
import { environment } from "../../../../environments/environment";

@Component({
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>

      <app-layout-navbar
        searchQuery="{{ word }}"
        numberOfProducts="9"
        [backLink]="['/products']"
      ></app-layout-navbar>

      <app-products-products-list
        *ngIf="productList"
        [products]="productList"
        [loading]="false"
      ></app-products-products-list>
    </div>
  `,
})
export class SearchResultsPage implements OnInit {
  word: string = "";
  productList;
  constructor(
    private route: ActivatedRoute,
    private productService: ProductService
  ) {
    this.productService.searchProductSubject.subscribe((data) => {
      this.productList = data.items;
      console.log(this.productList);
    });
  }
  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.word = params["word"];
    });
  }
}
