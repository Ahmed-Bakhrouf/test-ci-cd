import { Component, OnInit, HostListener, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import {
  CategoryService,
  Family,
} from "src/app/services/Category/category.service";
import { Observable, of, Subscription } from "rxjs";
import { switchMap, pluck, distinctUntilChanged, filter } from "rxjs/operators";

@Component({
  template: `
    <div class="Page" *ngIf="family$ | async; let family">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>

      <app-layout-navbar
        [title]="family.title"
        numberOfProducts="1"
        [icon]="'assets/images/familles/' + (family.icon ? family.icon : '')"
        [filter]="true"
        [backLink]="['/products']"
        (onFilter)="showFilterPanel = true"
      ></app-layout-navbar>

      <app-products-filter-panel
        [visible]="showFilterPanel"
        (onFilter)="onFilter()"
        (onCancel)="showFilterPanel = false"
      ></app-products-filter-panel>

      <app-products-categories
        [categories]="family.categories"
      ></app-products-categories>

      <ng-container>
        <app-products-products-list
          *ngFor="let category of family.categories"
          [scrollable]="true"
          [title]="category.title"
          [categoryId]="category._id"
          [showAllLink]="['/products', 'category', category._id]"
        ></app-products-products-list>
      </ng-container>
    </div>
  `,
})
export class FamilyPage implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private categoryService: CategoryService
  ) {}

  showFilterPanel: boolean = false;

  family$: Observable<Family>;

  onFilter() {
    this.showFilterPanel = false;
  }

  ngOnInit() {
    this.family$ = this.route.params.pipe(
      pluck("familyId"),
      distinctUntilChanged(),
      switchMap((familyId) => this.categoryService.getFamilyById(familyId))
    );
  }
}
