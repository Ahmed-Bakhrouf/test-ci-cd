import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import {
  CategoryService,
  Family,
} from "src/app/services/Category/category.service";
import { Observable, of, Subscription } from "rxjs";
import {
  pluck,
  filter,
  distinctUntilChanged,
  switchMap,
  map,
  take,
} from "rxjs/operators";

@Component({
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>

      <app-recherche></app-recherche>

      <!-- <app-banner
                image="assets/images/banner/BANNER01.jpg"
                [content]="bannerContent"
                btnText="Nos produits"
                textColor="black"
                btnColor="blue"
            ></app-banner> -->

      <!-- <app-banner
                image="assets/images/banner/BANNER02.jpg"
                [content]="bannerContent"
                btnText="Nos produits"
                textColor="white"
                btnColor="blue"
            ></app-banner> -->

      <!-- <app-banner
                image="assets/images/banner/BANNER03.jpg"
                [content]="bannerContent"
                btnText="Nos produits"
                textColor="black"
                btnColor="red"
            ></app-banner> -->

      <app-banner
        image="assets/images/banner/BANNER04.jpg"
        [content]="bannerContent"
        btnText="Nos produits"
        textColor="white"
        btnColor="red "
      ></app-banner>

      <app-products-categories
        [categories]="categories"
      ></app-products-categories>

      <!--  <app-products-products-list
        [scrollable]="true"
        title="Alimentation"
        [showAllLink]="['/integration', 'products', 'family', 'food']"
      ></app-products-products-list>
      <app-products-products-list
        [scrollable]="true"
        title="Santé & Bien-être"
        [showAllLink]="['/integration', 'products', 'family', 'tech']"
      ></app-products-products-list> -->

      <ng-container *ngIf="!subCategories">
        <!--  <app-products-products-list
          *ngFor="let category of categories"
          [scrollable]="true"
          [title]="category.title"
          [categoryId]="category._id"
          [showAllLink]="['/integration', 'products', 'family', 'teck']"
        ></app-products-products-list> -->
        <app-products-products-list
          *ngFor="let family of families$ | async"
          [scrollable]="true"
          [title]="family.title"
          [loading]="false"
          [categoryId]="family.categories[0]._id"
          [showAllLink]="['/products', 'family', family._id]"
        ></app-products-products-list>
      </ng-container>

      <ng-container *ngIf="subCategories">
        <app-products-products-list
          *ngFor="let subCategory of subCategories"
          [scrollable]="true"
          [title]="subCategory.title"
          [subCategoryId]="subCategory._id"
          [showAllLink]="[
            '/products',
            'sub-category',
            subCategory._id
          ]"
        ></app-products-products-list>
      </ng-container>
    </div>
  `,
})
export class HomePage implements OnInit, OnDestroy {
  stick: boolean = false;
  bannerContent: string;

  constructor(
    private route: ActivatedRoute,
    private categoryService: CategoryService
  ) {
    // this.getProducts();
    this.bannerContent = `
            Faites vos courses simplement
            en quelques clics, livrés chez vous
        `;
  }

  families$: Observable<Family[]>;
  categories;

  selectedCategory;

  subCategories;

  _paramsSubscription: Subscription;
  ngOnInit() {
    this.getAllCategories();
    this.families$ = this.categoryService.getFamilies();
    this._paramsSubscription = this.route.queryParams
      .pipe(
        pluck("cat"),
        filter((cat) => cat),
        distinctUntilChanged(),
        switchMap((cat) => {
          if (!this.selectedCategory || this.selectedCategory != cat) {
            this.selectedCategory = cat;
            return this.categoryService.getSubCategoriesByCatergoryID(cat);
          } else return of(null);
        })
      )
      .subscribe((subCategories) => {
        this.subCategories = subCategories;
      });
  }

  ngOnDestroy() {
    this._paramsSubscription.unsubscribe();
  }

  getAllCategories() {
    this.categoryService
      .getAllCategories()
      .pipe(take(1))
      .subscribe((categories) => {
        this.categories = categories;
      });
  }
/*  getProducts() {
    this.productsService.getHomeProducts().subscribe(data => {
      this.products = data;
      console.log(this.products);
      this.families$.forEach(f => {
       // const title= f.title;
      });
     // this.products
    });
  }*/
}
