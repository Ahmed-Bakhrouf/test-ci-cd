import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { CategoryService } from "src/app/services/Category/category.service";
import { Observable } from "rxjs";
import { take } from "rxjs/operators";

@Component({
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>

      <app-layout-navbar
        *ngIf="category"
        [title]="category?.title || ''"
        numberOfProducts="1"
        [icon]="
          'assets/images/Categories/' +
          (category.icon ? category.icon : 'categorie-fruit.svg')
        "
        [filter]="true"
        [backLink]="['/products', 'family']"
        (onFilter)="showFilterPanel = true"
      ></app-layout-navbar>

      <app-products-filter-panel
        [visible]="showFilterPanel"
        (onFilter)="onFilter()"
        (onCancel)="showFilterPanel = false"
      ></app-products-filter-panel>

      <app-products-products-list
        *ngFor="let subCategory of subCategories$ | async"
        [scrollable]="true"
        [title]="subCategory.title"
        [subCategoryId]="subCategory._id"
        [showAllLink]="['/products', 'sub-category', subCategory._id]"
      ></app-products-products-list>
    </div>
  `,
})
export class CategoryPage {
  constructor(
    private route: ActivatedRoute,
    private categoryService: CategoryService
  ) {}

  subCategories$: Observable<any[]>;

  category;

  showFilterPanel: boolean = false;

  onFilter() {
    this.showFilterPanel = false;
  }

  ngOnInit() {
    this.route.queryParams.subscribe(({ cat, fam, sub }) => {
      //console.log("params in home", fam, cat, sub);
      //  this.subCategories$= this.categoryService
      //     .getSubCategoriesByCatergoryID(cat);
    });
    let id = this.route.snapshot.paramMap.get("categoryId");
    this.categoryService
      .getCategoryById(id)
      .pipe(take(1))
      .subscribe((category) => (this.category = category));
    this.subCategories$ = this.categoryService.getSubCategoriesByCatergoryID(
      id
    );
  }
}
