import {
  Component,
  Input,
  HostBinding,
  ElementRef,
  ViewChild,
} from "@angular/core";
import * as $ from "jquery";
import { ProductDA } from "src/app/modelsDeliveryApp/productDA";
import { CartProductDA } from "src/app/modelsDeliveryApp/CartProductDA";
import { DataService } from "src/app/services/Shared/data.service";

@Component({
  selector: "app-products-product-item",
  template: `
    <ng-container *ngIf="skeletton">
      <div class="ProductsList-item-skeletton1"></div>
      <div class="ProductsList-item-skeletton2"></div>
      <div class="ProductsList-item-skeletton3"></div>
      <div class="ProductsList-item-skeletton4"></div>
      <div class="ProductsList-item-skeletton5"></div>
    </ng-container>
    <ng-container *ngIf="!skeletton">
      <div
        class="ProductsList-item-addToCart"
        (click)="onAddProductToCart()"
        [ngClass]="{ 'ProductsList-item-addToCart--off': product.stock < 1 }"
      >
        <img src="assets/images/plus_icon.svg" alt="Ajouter au panier" />
      </div>
      <div class="ProductsList-item-price">
        {{ product.price | number: "1.2-2" }} €
      </div>
      <a [routerLink]="link" class="ProductsList-item-image">
        <img #imageObject [src]="product.image" [alt]="product.title" />
      </a>
      <a [routerLink]="link" class="ProductsList-item-designation">{{
        product.title
      }}</a>
      <a [routerLink]="link" class="ProductsList-item-quantity">{{
        product.unitValue + " " + (product.unit ? product.unit.title : "pièce")
      }}</a>
      <a href="#" class="ProductsList-item-merchant"
        >Vendu par {{ product.merchant?.trade_name }}</a
      >
    </ng-container>
  `,
})
export class ProductItemComponent {
  constructor(private dataService: DataService) {}

  @ViewChild("imageObject", { static: false }) imageObject: ElementRef;
  @HostBinding("class") get classes(): string {
    const classes = ["ProductsList-item"];
    if (this.skeletton) {
      classes.push("ProductsList-item--skeletton");
    }
    return classes.join(" ");
  }

  @Input() skeletton: boolean = false;
  @Input() link: string[];
  @Input() price: number[];
  @Input() image: string;
  @Input() inStock: boolean;
  @Input() designation: string;
  @Input() quantity: string;
  @Input() merchant: string;

  @Input() product: ProductDA;

  onAddProductToCart() {
    this.dataService.addProductToCart(this.product);
    this.addToCart();
  }
  addToCart() {
    const image = $(this.imageObject.nativeElement);
    const clone = image.clone();
    clone.css({ transition: "all .8s ease" });
    let { top: topStart, left: leftStart } = image.offset();
    const cssStart = {
      width: image.width(),
      position: "fixed",
      top: topStart,
      left: leftStart,
    };
    clone.css(cssStart);

    $("body").append(clone);

    const header_cart = $("#header_cart");

    const { top: topEnd, left: leftEnd } = header_cart.offset();
    const cssEnd = {
      width: 0,
      top: topEnd,
      left: leftEnd,
    };
    clone.css(cssEnd);

    setTimeout(() => {
      clone.css({ opacity: 0 });
    }, 600);
  }
}
