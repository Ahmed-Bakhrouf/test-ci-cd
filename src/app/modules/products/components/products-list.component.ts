import { AfterViewInit, Component, Input, OnInit } from "@angular/core";
import { CartProductDA } from "src/app/modelsDeliveryApp/CartProductDA";
import { ProductDA } from "src/app/modelsDeliveryApp/productDA";
import { Observable } from "rxjs";
import { ProductService } from "src/app/services/Product/product.service";
import { pluck, tap } from "rxjs/operators";
import { environment } from "../../../../environments/environment";

@Component({
  selector: "app-products-products-list",
  template: `
    <div
      class="ProductsList"
      [ngClass]="{ 'ProductsList--scrollable': scrollable }"
    >
      <div class="ProductsList-header" *ngIf="title">
        <div class="ProductsList-header-titre">{{ title }}</div>
        <a class="ProductsList-header-showAll" [routerLink]="showAllLink">
          Voir tout
          <img src="assets/images/arrow_icon.svg" alt="Arrow" />
        </a>
      </div>
      <div class="ProductsList-items">
        <ng-container *ngIf="loading">
          <app-products-product-item
            [skeletton]="true"
          ></app-products-product-item>
          <app-products-product-item
            [skeletton]="true"
          ></app-products-product-item>
          <app-products-product-item
            [skeletton]="true"
          ></app-products-product-item>
          <app-products-product-item
            [skeletton]="true"
          ></app-products-product-item>
          <app-products-product-item
            [skeletton]="true"
          ></app-products-product-item>
          <app-products-product-item
            [skeletton]="true"
          ></app-products-product-item>
        </ng-container>
        <ng-container *ngIf="!subCategoryId">
          <app-products-product-item
            *ngFor="let prod of products"
            [link]="['/products', prod._id]"
            image="assets/images/produits/produit1.png"
            merchant="Epicerie Titi"
            [product]="prod"
          >
          </app-products-product-item>
        </ng-container>
        <ng-container *ngIf="products$">
          <app-products-product-item
            *ngFor="let prod of products$ | async"
            [link]="['/products', prod._id]"
            image="assets/images/produits/produit1.png"
            merchant="Epicerie Titi"
            [product]="prod"
          >
          </app-products-product-item>
        </ng-container>
        <div *ngIf="scrollable" class="ProductsList-items-spacer"></div>
      </div>
    </div>
  `,
})
export class ProductsListComponent implements OnInit {
  constructor(private productService: ProductService) {}

  @Input() scrollable: boolean = false;
  @Input() title?: string;
  @Input() showAllLink?: string[];

  @Input() products: ProductDA[] = [];

  @Input() subCategoryId: string;
  @Input() merchantId: string;
  @Input() searchInput: string;

  @Input() categoryId: string;
  products$: Observable<ProductDA[]>;

  @Input()
  loading: boolean = true;

  ngOnInit() {
    if (this.merchantId && this.subCategoryId) {
      this.products$ = this.productService
        .getProductsMerchantSubCategory(
          this.merchantId,
          this.subCategoryId,
          0,
          6
        )
        .pipe(
          tap(() => (this.loading = false)),
          pluck("items")
        );
    } else if (this.subCategoryId) {
      this.products$ = this.productService
        .getProductBySubcategoryAPI(0, 1000, this.subCategoryId)
        .pipe(
          tap(() => (this.loading = false)),
          pluck("items")
        );
    } else if (this.categoryId) {
      this.products$ = this.productService
        .getProductByCategoryAPI(0, 6, this.categoryId)
        .pipe(
          tap(() => (this.loading = false)),
          pluck("items")
        );
    }
  }
}
