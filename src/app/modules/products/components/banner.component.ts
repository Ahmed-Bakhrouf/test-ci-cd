import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { Component, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
    selector: 'app-banner',
    template: `
        <div class="Banner">
            <div class="Banner-image">
                <img [src]="image" alt="Bokai">
            </div>
            <div class="Banner-content">
                <div *ngIf="title" [class]="'Banner-content-title Banner-content-title--' + textColor">
                    {{ title }}
                </div>
                <div [class]="'Banner-content-text Banner-content-text--' + textColor">
                    {{ content }}
                </div>
                <div class="Banner-content-button">
                    <a *ngIf="btnLink"
                        [routerLink]="btnLink"
                        [class]="'Button Button--' + btnColor"
                    >
                        {{ btnText }}
                    </a>
                    <button *ngIf="!btnLink"
                        (click)="onBtnClick.emit()"
                        [class]="'Button Button--' + btnColor"
                    >
                        {{ btnText }}
                    </button>
                </div>
            </div>
        </div>
    `
})
export class BannerComponent implements OnChanges {

    @Input() title?: string;
    @Input() image: string;
    @Input() content: string;
    @Input() btnText: string;
    @Input() btnLink?: string[];
    @Input() textColor: 'black|white';
    @Input() btnColor: 'red|blue';

    @Output() onBtnClick: EventEmitter<void> = new EventEmitter();

    backgroundImage: SafeStyle;

    constructor(private sanitization: DomSanitizer) { }

    ngOnChanges() {
        this.backgroundImage = this.sanitization.bypassSecurityTrustStyle(`url(${this.image}`);
    }

}
