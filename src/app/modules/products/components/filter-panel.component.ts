import { Component, Output, EventEmitter, Input, OnInit, OnChanges } from '@angular/core';
import { BroadcasterService } from 'src/app/services/Shared/broadcaster.service';

@Component({
    selector: 'app-products-filter-panel',
    template: `
        <div class="Layout-overlay"
            [ngClass]="{'Layout-overlay--visible': visible}"
            (click)="onCancel.emit()"
        ></div>

        <div class="FilterPanel" [ngClass]="{'FilterPanel--visible': visible}">
            <div class="FilterPanel-title">Filtrer</div>
            <div class="FilterPanel-separator"></div>
            <div class="FilterPanel-list">
                <div class="FilterPanel-list-designation">
                    Trier par
                </div>
                <div class="FilterPanel-list-item">
                    <label class="Radio">
                        <input class="Radio-input" type="radio" name="sort" [(ngModel)]="sort" (ngModelChange)="onChange('sort', $event)" value="asc">
                        <div class="Radio-radio"></div>
                        <div class="Radio-label">Prix (croissant)</div>
                    </label>
                </div>
                <div class="FilterPanel-list-item">
                    <label class="Radio">
                        <input class="Radio-input" type="radio" name="sort" [(ngModel)]="sort" (ngModelChange)="onChange('sort', $event)" value="desc">
                        <div class="Radio-radio"></div>
                        <div class="Radio-label">Prix (décroissant)</div>
                    </label>
                </div>
            </div>
            <div class="FilterPanel-separator"></div>
            <div class="FilterPanel-list">
                <div class="FilterPanel-list-designation">
                    Sous catégories
                </div>
                <div *ngFor="let sub_category of sub_categories" class="FilterPanel-list-item">
                    <label class="Radio">
                        <input class="Radio-input" type="radio" name="sub_category" [(ngModel)]="sub_category_id" (ngModelChange)="onChange('sub_category', $event)" [value]="sub_category._id">
                        <div class="Radio-radio"></div>
                        <div class="Radio-label">{{ sub_category.name }}</div>
                    </label>
                    <div class="FilterPanel-list-item-badge">{{ sub_category.total }}</div>
                </div>
            </div>
            <div class="FilterPanel-separator"></div>
            <div class="FilterPanel-list">
                <div class="FilterPanel-list-designation">
                    Marchands
                </div>
                <div *ngFor="let merchant of merchants" class="FilterPanel-list-item">
                    <label class="Checkbox">
                        <input class="Checkbox-input" type="checkbox" name="merchants" [(ngModel)]="merchant_ids[merchant._id]" (ngModelChange)="onChange('merchants[' + merchant._id + ']', $event)" [value]="merchant._id">
                        <div class="Checkbox-checkbox"></div>
                        <div class="Checkbox-label Checkbox-label--bigger">{{ merchant.name }}</div>
                    </label>
                    <div class="FilterPanel-list-item-badge">{{ merchant.total }}</div>
                </div>
            </div>
        </div>
    `
})
export class FilterPanelComponent implements OnInit, OnChanges {

    @Input() visible: boolean = false;
    @Output() onFilter: EventEmitter<void> = new EventEmitter;
    @Output() onCancel: EventEmitter<void> = new EventEmitter;

    sort: 'asc'|'desc';
    sub_category_id: string;
    merchant_ids: { [_id: string]: boolean } = {};

    merchants: { _id: string, name: string, total: number }[];
    sub_categories: { _id: string, name: string, total: number }[];

    constructor(private broadcaster: BroadcasterService) { }

    ngOnInit() {
        this.merchants = [
            { _id: 'aaa', name: 'Marchand 1', total: 1122 },
            { _id: 'bbb', name: 'Marchand 2', total: 559 },
        ];
        this.sub_categories = [
            { _id: 'aaa', name: 'Sans sucre', total: 123 },
            { _id: 'bbb', name: 'Light', total: 45 },
            { _id: 'ccc', name: 'Sans caféine', total: 1 },
            { _id: 'ddd', name: 'Thé', total: 456 },
            { _id: 'eee', name: 'Café', total: 9 },
        ];
    }

    ngOnChanges() {
        if (this.visible) {
            this.broadcaster.broadcast('disableScroll');
        }
        else {
            this.broadcaster.broadcast('enableScroll');
        }
    }

    onChange(name, event) {
        this.onFilter.emit();
    }

}
