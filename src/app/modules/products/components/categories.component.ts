import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-products-categories",
  template: `
    <div class="Categories">
      <ng-container *ngIf="!categories || categories.length == 0">
        <app-products-category-item
          [skeletton]="true"
        ></app-products-category-item>
        <app-products-category-item
          [skeletton]="true"
        ></app-products-category-item>
        <app-products-category-item
          [skeletton]="true"
        ></app-products-category-item>
        <app-products-category-item
          [skeletton]="true"
        ></app-products-category-item>
        <app-products-category-item
          [skeletton]="true"
        ></app-products-category-item>
        <app-products-category-item
          [skeletton]="true"
        ></app-products-category-item>
        <app-products-category-item
          [skeletton]="true"
        ></app-products-category-item>
        <app-products-category-item
          [skeletton]="true"
        ></app-products-category-item>
        <app-products-category-item
          [skeletton]="true"
        ></app-products-category-item>
        <app-products-category-item
          [skeletton]="true"
        ></app-products-category-item>
      </ng-container>
      <ng-container *ngIf="categories && categories.length > 0">
        <app-products-category-item
          *ngFor="let category of categories"
          [categoryId]="category._id"
          [designation]="category.title"
          [defaultIcon]="
            'assets/images/Categories/' +
            (category.icon ? category.icon : 'categorie-fruit.svg')
          "
          [activeIcon]="
            'assets/images/Categories/' +
            (category.icon_active
              ? category.icon_active
              : 'categorie-fruit.svg')
          "
        >
        </app-products-category-item>
      </ng-container>
      <div class="Categories-spacer"></div>
    </div>
  `,
})
export class CategoriesComponent implements OnInit {
  constructor() {}

  @Input()
  categories: any[] = [];

  loading: boolean = true;

  ngOnInit() {}
}
