import { Component, Input } from "@angular/core";

@Component({
  selector: "app-products-category-item",
  template: `
    <div *ngIf="skeletton" class="Categories-item Categories-item--skeletton">
      <div class="Categories-item-skeletton1"></div>
      <div class="Categories-item-skeletton2"></div>
    </div>
    <a
      *ngIf="!skeletton"
      class="Categories-item"
      [routerLink]="['/products', 'category', categoryId]"
    >
      <div class="Categories-item-icon">
        <img
          class="Categories-item-icon-defaultIcon"
          [src]="defaultIcon"
          [alt]="designation"
        />
        <img
          class="Categories-item-icon-activeIcon"
          [src]="activeIcon"
          [alt]="designation"
        />
      </div>
      <div class="Categories-item-text">
        {{ designation }}
      </div>
    </a>
  `,
})
export class CategoryItemComponent {
  @Input() skeletton: boolean = false;
  @Input() categoryId: string;
  @Input() designation: string;
  @Input() defaultIcon: string;
  @Input() activeIcon: string;
}
