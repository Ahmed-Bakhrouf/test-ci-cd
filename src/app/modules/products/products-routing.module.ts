import { SubCategoryPage } from "././pages/sub-category.page";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomePage } from "./pages/home.page";
import { SearchResultsPage } from "./pages/search-results.page";
import { FamilyPage } from "./pages/family.page";
import { CategoryPage } from "./pages/category.page";
import { ProductPage } from "./pages/product.page";

const routes: Routes = [
  { path: "", component: HomePage },
  { path: "family/:familyId", component: FamilyPage },
  { path: "category/:categoryId", component: CategoryPage },
  { path: "sub-category/:sub-categoryId", component: SubCategoryPage },
  { path: "search-results/:word", component: SearchResultsPage },
  { path: ":productID", component: ProductPage },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsRoutingModule {}
