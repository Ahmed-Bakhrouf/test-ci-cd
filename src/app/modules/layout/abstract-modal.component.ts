import { ModalService } from "src/app/services/Shared/modal.service";
import {
  OnInit,
  ElementRef,
  OnDestroy,
  ViewChild,
  AfterViewInit,
} from "@angular/core";
import * as $ from "jquery";

export abstract class AbstractModalComponent
  implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild("input", { static: false }) inputElement: ElementRef;

  visible: boolean = false;
  jQueryInputElement: any;
  jQueryModalElement: any;
  stickTop: boolean = false;
  defaultMarginTop: string;

  constructor(private modalService: ModalService) {}

  ngAfterViewInit() {
    setTimeout(() => {
      this.jQueryModalElement = $(".Modal-content");
      this.defaultMarginTop = this.jQueryModalElement.css("marginTop");
      this.centerModal();
      this.visible = true;

      if (this.inputElement) {
        this.jQueryInputElement = $(this.inputElement.nativeElement);
        this.jQueryInputElement.on("focus", () => {
          const { top, left } = this.jQueryInputElement.offset();
          const marginTop = `calc(-${top}px + ${this.jQueryModalElement.css('marginTop')} + ${this.defaultMarginTop} + 30px)`;
          this.jQueryModalElement.css({ marginTop });
        });
        this.jQueryInputElement.on("blur", () => {
          console.log("blur");
          this.centerModal();
        });
      }
    });
  }

  ngOnInit() {}

  ngOnDestroy() {
    if (this.jQueryInputElement) {
      this.jQueryInputElement.off("focus");
      this.jQueryInputElement.off("blur");
    }
  }

  centerModal() {
    const modalHeight = this.jQueryModalElement.outerHeight();
    const windowHeight = $(window).height();
    if (modalHeight < windowHeight) {
      const marginTop = `calc(${(windowHeight - modalHeight) / 2}px - ${
        this.defaultMarginTop
      })`;
      this.jQueryModalElement.css({ marginTop });
    }
  }

  async close(): Promise<void> {
    return new Promise((resolve) => {
      this.visible = false;
      setTimeout(() => {
        this.modalService.close();
        resolve();
      }, 1000);
    });
  }
}
