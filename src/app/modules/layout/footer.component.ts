import { Component } from '@angular/core';

@Component({
    selector: "app-layout-footer",
    template: `
        <div class="Footer">
            <div class="Footer-social">
                <div class="Footer-social-title">Rejoignez la communauté Bokay</div>
                <div class="Footer-social-links">
                    <a href="#" target="_blank" class="Footer-social-links-item">
                        <img src="assets/images/layout/ic_facebook.svg" alt="Facebook">
                    </a>
                    <a href="#" target="_blank" class="Footer-social-links-item">
                        <img src="assets/images/layout/ic_instagram.svg" alt="Instagram">
                    </a>
                    <a href="#" target="_blank" class="Footer-social-links-item">
                        <img src="assets/images/layout/ic_linkedin.svg" alt="LinkedIn">
                    </a>
                </div>
            </div>
            <div class="Footer-partner">
                <img src="assets/images/layout/logo_bokay_white.png" class="Footer-partner-logo" alt="Bokay">
                <div class="Footer-partner-title">
                    Vous souhaitez devenir un marchant partenaire ?
                </div>
                <a class="Footer-partner-button" href="#">
                    Vendre sur Bokay
                </a>
                <div class="Footer-partner-separator"></div>
            </div>
            <div class="Footer-links">
                <div class="Footer-links-top">
                    <a class="Footer-links-top-item" href="#">
                        <img src="assets/images/layout/ic_marker.svg" alt="Marker">
                        Où êtes-vous situé ?
                    </a>
                    <a class="Footer-links-top-item" href="#">
                        <img src="assets/images/layout/ic_question.svg" alt="Question">
                        Besoin d'aide ?
                    </a>
                </div>
                <div class="Footer-links-bottom">
                    <a class="Footer-links-bottom-item" href="#">Locations</a>
                    <a class="Footer-links-bottom-item" href="#">Partenaires</a>
                    <a class="Footer-links-bottom-item" href="#">C.G.U.</a>
                    <a class="Footer-links-bottom-item" href="#">Carrières</a>
                    <a class="Footer-links-bottom-item" href="#">Presse</a>
                    <a class="Footer-links-bottom-item" href="#">Informations</a>
                    <a class="Footer-links-bottom-item" href="#">C.G.V.</a>
                </div>
            </div>
        </div>
    `,
})
export class FooterComponent {
}
