import { Component, OnInit, OnDestroy, ViewContainerRef } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import * as $ from "jquery";
import { BroadcasterService } from "src/app/services/Shared/broadcaster.service";
import { ModalService } from "src/app/services/Shared/modal.service";

@Component({
  selector: "app-layout",
  // templateUrl: './layout.component.html',
  template: `
    <ng-sidebar-container>
      <ng-sidebar position="left" mode="slide" [(opened)]="sidebarOpened">
        <app-layout-sidebar
          (closeSidebar)="toggleSidebar()"
        ></app-layout-sidebar>
      </ng-sidebar>
      <div id="main" ng-sidebar-content>
        <div class="Layout">
          <div
            class="Layout-overlay"
            (click)="closeSidebar()"
            [ngClass]="{ 'Layout-overlay--visible': sidebarOpened }"
          ></div>
          <div class="Layout-header">
            <app-layout-header
              [sidebarOpened]="sidebarOpened"
              (toggleSidebar)="toggleSidebar()"
              (search)="showSearchBar = true"
            ></app-layout-header>
          </div>
          <div class="Layout-content">
            <app-recherche
              [topbarVisible]="showSearchBar"
              [topbarMode]="true"
              (closeSearch)="showSearchBar = false"
            ></app-recherche>
            <router-outlet></router-outlet>
          </div>
        </div>
      </div>
    </ng-sidebar-container>
  `,
})
export class LayoutComponent implements OnInit, OnDestroy {
  sidebarOpened: boolean = false;
  showSearchBar: boolean = false;

  content: any;

  constructor(
    private router: Router,
    private broadcaster: BroadcasterService,
    private modalService: ModalService,
    private viewContainerRef: ViewContainerRef
  ) {}

  ngOnInit() {
    this.content = $(".ng-sidebar__content");
    this.router.events.subscribe((event) => {
      if (!(event instanceof NavigationEnd)) {
        return;
      }
      this.content.scrollTop(0);
    });
    this.broadcaster
      .on<void>("disableScroll")
      .subscribe(() => this.disableScroll());
    this.broadcaster
      .on<void>("enableScroll")
      .subscribe(() => this.enableScroll());

    this.modalService.setViewContainerRef(this.viewContainerRef);

    $("input").on("change", function (event) {
      const value = $(this).val();
      if (value) {
        $(this).addClass("not_empty");
      } else {
        $(this).removeClass("not_empty");
      }
    });
  }

  ngOnDestroy() {
    $("input").off("change");
  }

  toggleSidebar() {
    this.sidebarOpened = !this.sidebarOpened;
    this.sidebarOpened ? this.disableScroll() : this.enableScroll();
  }

  closeSidebar() {
    this.sidebarOpened = false;
  }

  disableScroll() {
    this.content.css({ overflow: "hidden" });
  }

  enableScroll() {
    this.content.css({ overflow: "auto" });
  }
}
