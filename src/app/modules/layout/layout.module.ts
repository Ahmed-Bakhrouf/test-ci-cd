import { NavbarComponent } from "./navbar.component";
import { CommonModule } from "@angular/common";
import { SidebarModule } from "ng-sidebar";
import { NgModule } from "@angular/core";
import { LayoutComponent } from "./layout.component";
import { HeaderComponent } from "./header.component";
import { SidebarComponent } from "./sidebar.component";
import { RouterModule } from "@angular/router";
import { SearchBarComponent } from "./search-bar.component";
import { AddressModalComponent } from "./address-modal.component";
import { SelectModalComponent } from "./select-modal.component";
import { FormsModule } from "@angular/forms";
import { AlertModalComponent } from "./alert-modal.component";
import { ForgotPasswordModalComponent } from "../cart/components/forgot-password-modal.component";
import { EnterPasswordModalComponent } from "../cart/components/enter-password-modal.component";
import { MatAutocompleteModule } from "@angular/material";
//import { FooterComponent } from './footer.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SidebarModule,
    FormsModule,
    MatAutocompleteModule,
  ],
  declarations: [
    LayoutComponent,
    HeaderComponent,
    //FooterComponent,
    SidebarComponent,
    SearchBarComponent,
    NavbarComponent,
    AddressModalComponent,
    SelectModalComponent,
    AlertModalComponent,
    ForgotPasswordModalComponent,
    EnterPasswordModalComponent,
  ],
  entryComponents: [
    AddressModalComponent,
    SelectModalComponent,
    AlertModalComponent,
    ForgotPasswordModalComponent,
    EnterPasswordModalComponent,
  ],
  exports: [LayoutComponent, SearchBarComponent, NavbarComponent],
})
export class LayoutModule {}
