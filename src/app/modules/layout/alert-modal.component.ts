import { Component, Output, EventEmitter, OnInit, Input } from "@angular/core";
import { ModalService } from 'src/app/services/Shared/modal.service';
import { AbstractModalComponent } from './abstract-modal.component';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

@Component({
    selector: 'app-layout-alert-modal',
    template: `
        <div class="Modal" [ngClass]="{'Modal--visible': visible}">
            <div class="Layout-overlay"
                [ngClass]="{'Layout-overlay--visible': visible}"
                (click)="onCancel.emit()"
            ></div>
            <div class="Modal-content">
                <div *ngIf="icon" class="Modal-content-logo">
                    <img [src]="icon" alt="Icon">
                </div>
                <div *ngIf="title" class="Modal-content-title">
                    {{ title }}
                </div>
                <div class="Modal-content-text" [innerHTML]="sanitizedText"></div>
                <div class="Modal-content-button">
                    <button class="Button Button--modal Button--red" (click)="onBigButtonClick.emit()">
                        {{ bigButtonText }}
                    </button>
                </div>
                <div *ngIf="cancelButtonText" class="Modal-content-buttons Modal-content-buttons--center">
                    <button class="Modal-content-buttons-item" (click)="onCancel.emit()">
                        {{ cancelButtonText }}
                    </button>
                </div>
            </div>
        </div>
    `
})
export class AlertModalComponent extends AbstractModalComponent implements OnInit {

    @Input() icon?: string;
    @Input() title?: string;
    @Input() text: string;
    @Input() bigButtonText: string;
    @Input() cancelButtonText: string;

    @Output() onBigButtonClick: EventEmitter<void> = new EventEmitter;
    @Output() onCancel: EventEmitter<void> = new EventEmitter;

    sanitizedText: SafeHtml;

    constructor(modalService: ModalService, private sanitizer: DomSanitizer) {
        super(modalService);
    }

    ngOnInit() {
        // super.ngOnInit();
        this.sanitizedText = this.sanitizer.bypassSecurityTrustHtml(this.text);
    }

}