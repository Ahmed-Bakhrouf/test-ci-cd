import { Router } from "@angular/router";
import { Component, Output, EventEmitter, OnInit } from "@angular/core";
import {
  CategoryService,
  Family,
} from "src/app/services/Category/category.service";
import { Observable } from "rxjs";
import { LoginService } from "src/app/services/loginService/login.service";

@Component({
  selector: "app-layout-sidebar",
  template: `
    <div class="Sidebar">
      <!-- ** Alimentation ** -->
      <div
        *ngFor="let family of families$ | async"
        [routerLink]="['/products/family', family._id]"
        class="Sidebar-item"
      >
        <div class="Sidebar-item-icon Sidebar-item-icon--famille">
          <img
            [src]="'assets/images/familles/' + family.icon"
            [alt]="family.title"
          />
        </div>
        <div class="Sidebar-item-text">
          {{ family.title }}
        </div>
        <div class="Sidebar-item-arrow">
          <img src="assets/images/sidebar/arrow_icon.svg" alt="Arrow" />
        </div>
      </div>

      <div class="Sidebar-spacer"></div>

      <!-- ** Mon compte ** -->
      <div
        *ngIf="authenticated$ | async"
        class="Sidebar-item Sidebar-item--withBorderTop"
        (click)="gotoMyAccount()"
      >
        <div class="Sidebar-item-icon">
          <img
            src="assets/images/sidebar/mon_compte_icon.svg"
            alt="Mon compte"
          />
        </div>
        <div class="Sidebar-item-text">Mon compte</div>
      </div>

      <!-- ** SignUp/SignIn ** -->
      <div
        *ngIf="!(authenticated$ | async)"
        class="Sidebar-item Sidebar-item--withBorderTop"
        (click)="gotoSignin()"
      >
        <div class="Sidebar-item-icon">
          <img
            src="assets/images/sidebar/mon_compte_icon.svg"
            alt="Mon compte"
          />
        </div>
        <div class="Sidebar-item-text">Se connecter</div>
      </div>

      <!-- ** Marchands ** -->
      <div class="Sidebar-item" (click)="gotoMerchants()">
        <div class="Sidebar-item-icon">
          <img src="assets/images/sidebar/marchands_icon.svg" alt="Marchands" />
        </div>
        <div class="Sidebar-item-text">Marchands</div>
      </div>

      <!-- ** Aide ** -->
      <div class="Sidebar-item">
        <div class="Sidebar-item-icon">
          <img src="assets/images/sidebar/aide_icon.svg" alt="Aide" />
        </div>
        <div class="Sidebar-item-text">Aide</div>
      </div>

      <!-- ** A propos ** -->
      <div class="Sidebar-item">
        <div class="Sidebar-item-icon">
          <img src="assets/images/sidebar/aide_icon.svg" alt="A propos" />
        </div>
        <div class="Sidebar-item-text">A propos</div>
      </div>

      <!-- ** Devenir partenaire ** -->
      <div href="#" class="Sidebar-partenaire">
        <img src="assets/images/sidebar/partenaire_icon.svg" alt="Partenaire" />
        Devenir partenaire
      </div>
    </div>
  `,
})
export class SidebarComponent implements OnInit {
  @Output() closeSidebar: EventEmitter<void> = new EventEmitter();

  constructor(
    private router: Router,
    private categoryService: CategoryService,
    private authService: LoginService
  ) {}

  families$: Observable<Family[]>;

  authenticated$: Observable<boolean>;

  ngOnInit() {
    this.authenticated$ = this.authService.authenticated$;
    this.families$ = this.categoryService.getFamilies();
  }

  gotoMerchants() {
    this.router.navigate(["/merchants"]);
    this.closeSidebar.emit();
  }

  gotoMyAccount() {
    this.router.navigate(["/account"]);
    this.closeSidebar.emit();
  }

  gotoFamilyPage() {
    this.router.navigate(["/products", "family"]);
    this.closeSidebar.emit();
  }

  gotoSignin() {
    this.router.navigate(["/account", "signin"]);
    this.closeSidebar.emit();
  }
}
