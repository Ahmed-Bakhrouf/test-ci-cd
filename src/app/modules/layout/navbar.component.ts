import {
  Component,
  Input,
  Output,
  EventEmitter,
  ElementRef,
  ViewChild,
  AfterViewInit,
  OnDestroy,
} from "@angular/core";
import * as $ from "jquery";
import { Location } from "@angular/common";

@Component({
  selector: "app-layout-navbar",
  template: `
    <div #navbar class="Navbar" [ngClass]="{ 'Navbar--stickTop': stickTop }">
      <a (click)="back()" class="Navbar-left">
        <img src="assets/images/navback_icon.svg" alt="Retour" />
      </a>
      <div class="Navbar-right">
        <div *ngIf="icon" class="Navbar-right-icon">
          <img [src]="icon" alt="Icone navigation" />
        </div>
        <div
          class="Navbar-right-content"
          [ngClass]="{ 'Navbar-right-content--noIcon': !!searchQuery || !icon }"
        >
          <div *ngIf="title" class="Navbar-right-content-title">
            {{ title }}
          </div>
          <div *ngIf="searchQuery" class="Navbar-right-content-title">
            Recherche : <span>{{ searchQuery }}</span>
          </div>

          <div class="Navbar-right-content-subtitle">
            <ng-template [ngIf]="numberOfProducts == 0"
              >Aucun produit</ng-template
            >
            <ng-template [ngIf]="numberOfProducts == 1">1 produit</ng-template>
            <ng-template [ngIf]="numberOfProducts > 1"
              >{{ numberOfProducts }} produits</ng-template
            >
          </div>

          <div *ngIf="subtitle" class="Navbar-right-content-subtitle">
            {{ subtitle }}
          </div>
          <div
            *ngIf="stock !== null"
            class="Navbar-right-content-stock"
            [ngClass]="{ 'Navbar-right-content-stock--rupture': stock < 10 }"
          >
            <div class="Navbar-right-content-stock-icon"></div>
            <ng-template [ngIf]="stock > 9">En stock</ng-template>
            <ng-template [ngIf]="stock == 0">Rupture de stock</ng-template>
            <ng-template [ngIf]="stock != 0 && stock < 10"
              >Bientôt épuisé</ng-template
            >
          </div>
        </div>
        <div
          *ngIf="filter"
          (click)="onFilter.emit()"
          class="Navbar-right-filter"
        >
          <img src="assets/images/filter_icon.svg" alt="Filter" />
        </div>
        <div *ngIf="price1 || price2" class="Navbar-right-price">
          <div class="Navbar-right-price-price1">{{ price1 }}</div>
          <div class="Navbar-right-price-price2">{{ price2 }}</div>
        </div>
        <div *ngIf="logout" class="Navbar-right-logout" (click)="logoutClick()">
          <img src="assets/images/account/logout_icon.svg" />
        </div>
      </div>
    </div>

    <div *ngIf="stickTop" class="Navbar Navbar--placeholder"></div>
  `,
})
export class NavbarComponent implements AfterViewInit, OnDestroy {
  @Input() title?: string;
  @Input() subtitle?: string;
  @Input() searchQuery?: string;
  @Input() numberOfProducts: number = null;
  @Input() icon?: string;
  @Input() filter: boolean = false;
  @Input() backLink: string[];
  @Input() stock?: number = null;
  @Input() logout?: boolean = false;

  @Input() price1?: string;
  @Input() price2?: string;

  @Output() onFilter: EventEmitter<void> = new EventEmitter();
  @Output() onLogout: EventEmitter<void> = new EventEmitter();

  @ViewChild("navbar", { static: false }) navbarElement: ElementRef;

  stickTop: boolean = false;
  page: any;

  constructor(private elRef: ElementRef, private _location: Location) {}

  ngAfterViewInit() {
    setTimeout(() => {
      const elementPosition = this.navbarElement.nativeElement.offsetTop;
      this.page = $(".ng-sidebar__content");
      this.page.on("scroll", () => {
        const scroll = this.page.scrollTop();
        this.stickTop = scroll >= elementPosition - 15;
      });
    }, 100);
  }

  ngOnDestroy() {
    if (this.page) {
      this.page.off("scroll");
    }
  }

  logoutClick() {
    this.onLogout.next();
  }

  back() {
    this._location.back();
  }
}
