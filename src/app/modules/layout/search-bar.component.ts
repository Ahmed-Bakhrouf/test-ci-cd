import {
  Component,
  ElementRef,
  ViewChild,
  AfterViewInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy,
  OnInit,
} from "@angular/core";
import * as $ from "jquery";
import { fromEvent, Observable } from "rxjs";
import { ExistsResult } from "../../models/existResultDA";
import { SearchResultDto } from "../../modelsDeliveryApp/search-resultDA";
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  switchMap,
} from "rxjs/operators";
import { SearchService } from "../../services/Shared/search.service";
import { Router } from "@angular/router";
import { ProductService } from "../../services/Product/product.service";

@Component({
  selector: "app-recherche",
  template: `
    <div
      #searchBar
      class="SearchBar"
      [ngClass]="{
        'SearchBar--stickTop': stickTop,
        'SearchBar--topbarMode': topbarMode,
        'SearchBar--topbarVisible': topbarVisible
      }"
    >
      <div class="SearchBar-content">
        <div class="SearchBar-content-icon">
          <img src="assets/images/recherche_icon.svg" alt="Recherche" />
        </div>
        <div class="SearchBar-content-input">
          <input
            #input
            type="text"
            placeholder="Que desirez-vous ?"
            [matAutocomplete]="auto"
            (keyup.enter)="displaySearchResult($event.target.value)"
          />
        </div>
        <mat-autocomplete #auto="matAutocomplete">
          <mat-option
            *ngFor="let resultItem of (searchResult$ | async)?.items"
            (click)="displaySearchResult(resultItem.title)"
          >
            {{ resultItem.title }}
          </mat-option>
        </mat-autocomplete>
      </div>
      <div
        *ngIf="topbarMode"
        (click)="closeSearch.emit()"
        class="SearchBar-close"
      >
        <img src="assets/images/close_icon.png" alt="Fermer" />
      </div>
    </div>
    <div *ngIf="stickTop" class="SearchBar SearchBar--placeholder"></div>
  `,
})
export class SearchBarComponent implements AfterViewInit, OnDestroy {
  @Input() topbarMode: boolean = false;
  @Input() topbarVisible: boolean = false;
  @Output() closeSearch: EventEmitter<void> = new EventEmitter();

  @ViewChild("searchBar", { static: false }) searchBarElement: ElementRef;
  @ViewChild("input", { static: false }) inputElement: ElementRef;
  stickTop: boolean = false;
  topbarModeVisible: boolean = false;
  page: any;
  searchResult$: Observable<ExistsResult<SearchResultDto>>;
  options: string[] = ["One", "Two", "Three"];
  isSearching: boolean;
  constructor(
    private searchService: SearchService,
    private router: Router,
    private productService: ProductService
  ) {}
  ngAfterViewInit() {
    if (!this.topbarMode) {
      setTimeout(() => {
        const elementPosition = this.searchBarElement.nativeElement.offsetTop;
        this.page = $(".ng-sidebar__content");
        this.page.on("scroll", () => {
          const scroll = this.page.scrollTop();
          this.stickTop = scroll >= elementPosition - 15;
        });
      }, 100);
    } else {
      $(this.inputElement.nativeElement).focus();
    }
    this.searchResult$ = fromEvent(
      this.inputElement.nativeElement,
      "keyup"
    ).pipe(
      map((event: any) => {
        return event.target.value;
      }),
      // if character length greater then 2
      filter((res) => res.length > 2),
      debounceTime(500),
      distinctUntilChanged(),
      switchMap((text: string) => {
        this.isSearching = true;
        return this.searchService.searchByKeyword(text);
      })
    );
  }
  displaySearchResult(result) {
    this.productService.applySearch(result);
    this.router.navigate(["/products", "search-results", result]);
  }
  ngOnDestroy() {
    if (this.page) {
      this.page.off("scroll");
    }
  }
}
