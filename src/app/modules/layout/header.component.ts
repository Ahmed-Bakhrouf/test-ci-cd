import { Component, Output, EventEmitter, Input, OnInit } from "@angular/core";
import { ModalService } from "src/app/services/Shared/modal.service";
import { AddressModalComponent } from "./address-modal.component";
import {
  Router,
  ActivatedRoute,
  UrlSegment,
  Event,
  NavigationStart,
  ResolveEnd,
} from "@angular/router";
import { DataService } from "src/app/services/Shared/data.service";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Component({
  selector: "app-layout-header",
  template: `
    <div class="Header">
      <div
        class="Header-menuToggle"
        [ngClass]="{ 'Header-menuToggle--opened': sidebarOpened }"
        (click)="toggleSidebar.emit()"
      >
        <span></span>
        <span></span>
        <span></span>
      </div>
      <div class="Header-addressChoice" (click)="changeAddress()">
        <div class="Header-addressChoice-caption">Mon adresse</div>
        <div class="Header-addressChoice-addressWrapper">
          <div class="Header-addressChoice-addressWrapper-address">
            94270, Le Kremlin-Bicêtre
          </div>
          <div class="Header-addressChoice-addressWrapper-icon">
            <img src="assets/images/layout/address_icon.svg" alt="Address" />
          </div>
        </div>
      </div>
      <div class="Header-toolbar">
        <div
          class="Header-toolbar-item"
          (click)="search.emit()"
          *ngIf="showSearchButton"
        >
          <img src="assets/images/layout/search_icon.svg" alt="Search" />
        </div>
        <div class="Header-toolbar-item" (click)="gotoCart()" id="header_cart">
          <img src="assets/images/layout/cart_icon.svg" alt="Cart" />
          <div
            class="Header-toolbar-item-badge"
            *ngIf="cartProductNumber$ | async"
          >
            {{ cartProductNumber }}
          </div>
        </div>
      </div>
    </div>
  `,
})
export class HeaderComponent implements OnInit {
  @Input() sidebarOpened: boolean;

  @Output() toggleSidebar = new EventEmitter();
  @Output() search = new EventEmitter();

  showSearchButton: boolean;

  cartProductNumber$: Observable<number>;
  cartProductNumber: number;

  constructor(
    private modalService: ModalService,
    private router: Router,
    private route: ActivatedRoute,
    private dataService: DataService
  ) {}

  ngOnInit() {
    const homePageUrl = "/products";

    this.cartProductNumber$ = this.dataService.cartProductNumber$.pipe(
      map((number) => {
        this.cartProductNumber = number;
        return number;
      })
    );
    this.showSearchButton = this.router.url !== homePageUrl;
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationStart) {
        this.showSearchButton = event.url !== homePageUrl;
      }
    });
  }

  changeAddress() {
    const instance: AddressModalComponent = this.modalService.open(
      AddressModalComponent
    );
    instance.onSubmit.subscribe(() => {
      instance.close();
    });
    instance.onCancel.subscribe(() => {
      instance.close();
    });
  }

  gotoCart() {
    if (this.cartProductNumber > 0) this.router.navigate(["/cart"]);
    else this.openEmptyCartModal();
  }

  openEmptyCartModal() {
    const icon = "assets/images/cart/modal_icon.svg";
    const title = "Votre panier est vide";
    const text = `Il ne vous reste plus qu'à faire vos courses et revenir !`;
    const buttonText = "Retour aux produits";
    this.modalService.alert(title, text, buttonText, null, icon);
  }
}
