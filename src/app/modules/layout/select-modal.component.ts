import { Component, Output, EventEmitter, Input, OnChanges, OnInit } from "@angular/core";
import { ModalService, SelectOption } from 'src/app/services/Shared/modal.service';
import { AbstractModalComponent } from './abstract-modal.component';

@Component({
    selector: 'app-layout-select-modal',
    template: `
        <div class="Modal" [ngClass]="{'Modal--visible': visible}">
            <div class="Layout-overlay"
                [ngClass]="{'Layout-overlay--visible': visible}"
                (click)="onCancel.emit()"
            ></div>
            <div class="Modal-content Modal-content--white">
                <div *ngIf="title" class="Modal-content-title Modal-content-title--small">
                    {{ title }}
                </div>
                <div class="Modal-content-selectList">
                    <label *ngFor="let option of options" class="Modal-content-selectList-item">
                        <input class="Modal-content-selectList-item-input" type="radio" name="sort" [(ngModel)]="value" [value]="option">
                        <div class="Modal-content-selectList-item-radio"></div>
                        <div class="Modal-content-selectList-item-label">{{ option.label }}</div>
                    </label>
                </div>
                <div class="Modal-content-button">
                    <button class="Button Button--modal Button--red" (click)="submit()">
                        Valider
                    </button>
                </div>
            </div>
        </div>
    `
})
export class SelectModalComponent extends AbstractModalComponent implements OnInit {

    @Input() title?: string;
    @Input() options: SelectOption[];
    @Input() selected: SelectOption;

    @Output() onSubmit: EventEmitter<SelectOption> = new EventEmitter;
    @Output() onCancel: EventEmitter<void> = new EventEmitter;

    private value: SelectOption;

    constructor(modalService: ModalService) {
        super(modalService);
    }

    ngOnInit() {
        // super.ngOnInit();
        if (this.selected) {
            this.value = this.options.find(item => item.value === this.selected.value);
        }
    }

    submit() {
        this.onSubmit.emit(this.value);
    }

}