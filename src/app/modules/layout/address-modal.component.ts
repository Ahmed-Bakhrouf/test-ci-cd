import {
  Component,
  Output,
  EventEmitter,
  OnInit,
  ViewChild,
  ElementRef,
  NgZone,
  ChangeDetectorRef,
} from "@angular/core";
import { ModalService } from "src/app/services/Shared/modal.service";
import { AbstractModalComponent } from "./abstract-modal.component";

@Component({
  selector: "app-layout-address-modal",
  template: `
    <div class="Modal" [ngClass]="{ 'Modal--visible': visible }">
      <div
        class="Layout-overlay"
        [ngClass]="{ 'Layout-overlay--visible': visible }"
        (click)="onCancel.emit()"
      ></div>
      <div class="Modal-content">
        <div class="Modal-content-logo">
          <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai" />
        </div>
        <div class="Modal-content-title">
          Bienvenue sur Bokay, <br />
          l'appli de courses
        </div>
        <div class="Modal-content-input">
          <div class="Modal-content-input-icon">
            <img
              src="assets/images/marker_icon.svg"
              alt="Marker"
              (click)="getPositionByLocalisation()"
            />
          </div>
          <div class="Modal-content-input-input">
            <!-- Ne pas supprimer le #input svp, il est utilisé dans la classe parente -->
            <input
              type="text"
              [(ngModel)]="autocompleteInput"
              placeholder="Entrez votre adresse/code postal"
              #input
              #addresstext
            />
          </div>
        </div>
        <div class="Modal-content-text">
          Entrez votre code postal pour découvrir les produits proches de chez
          vous
        </div>
        <!--
                <div class="Modal-content-text Modal-content-text--error">
                    Désolé, votre ville n'est pas encore desservie par nos services
                </div>
                -->
        <div class="Modal-content-button">
          <button class="Button Button--modal Button--red" (click)="submit()">
            Valider
          </button>
        </div>
      </div>
    </div>
  `,
})
export class AddressModalComponent extends AbstractModalComponent {
  @ViewChild("addresstext", { static: true }) addresstext: ElementRef;
  lat: any;
  lng: any;
  address: any;
  private geoCoder;
  autocompleteInput: string;
  service: google.maps.places.PlacesService;
  // tslint:disable-next-line:new-parens
  @Output() onSubmit: EventEmitter<void> = new EventEmitter();
  // tslint:disable-next-line:new-parens
  @Output() onCancel: EventEmitter<void> = new EventEmitter();
  constructor(
    modalService: ModalService,
    public zone: NgZone,
    private chRef: ChangeDetectorRef
  ) {
    super(modalService);
    // tslint:disable-next-line:new-parens
  }
  // tslint:disable-next-line:use-lifecycle-interface
  ngAfterViewInit() {
    // tslint:disable-next-line:new-parens
    this.geoCoder = new google.maps.Geocoder();
    this.getPlaceAutocomplete();
    super.ngAfterViewInit();
  }

  submit() {
    this.onSubmit.emit();
  }

  cancel(event: any) {
    if (event.target.className.split(" ").indexOf("Modal") !== -1) {
      this.onCancel.emit();
    }
  }

  getPositionByLocalisation() {
    if (navigator) {
      navigator.geolocation.getCurrentPosition((pos) => {
        this.lng = +pos.coords.longitude;
        this.lat = +pos.coords.latitude;
        this.savePosition(this.lat, this.lng);
        this.getAddress(this.lat, this.lng);
      });
    }
  }
  private getPlaceAutocomplete() {
    const autocomplete = new google.maps.places.Autocomplete(
      this.addresstext.nativeElement,
      {
        componentRestrictions: { country: ["TN", "fr", "cm"] },
        types: ["geocode"], // 'establishment' / 'address' / 'geocode'
      }
    );
    google.maps.event.addListener(autocomplete, "place_changed", () => {
      const place = autocomplete.getPlace();
      this.savePosition(
        place.geometry.location.lat(),
        place.geometry.location.lng()
      );
    });
  }

  savePosition(lat, lng) {
    localStorage.setItem(
      "place",
      JSON.stringify("lat :" + lat + " long : " + lng)
    );
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode(
      { location: { lat: latitude, lng: longitude } },
      (results, status) => {
        console.log(results);
        if (status === "OK") {
          if (results[0]) {
            this.address = results[0].formatted_address;
            this.autocompleteInput = this.address;
            this.chRef.detectChanges();
            console.log(results[0].formatted_address);
          } else {
            window.alert("No results found");
          }
        } else {
          console.log("Geocoder failed due to: " + status);
        }
      }
    );
  }
}
