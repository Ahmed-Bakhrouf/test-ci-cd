import { NgModule } from '@angular/core';
import { SamplePage } from './pages/sample.page';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: 'sample', component: SamplePage },
];

@NgModule({
    imports: [ RouterModule.forChild(routes) ],
    exports: [ RouterModule ]
})
export class ContentRoutingModule { }
