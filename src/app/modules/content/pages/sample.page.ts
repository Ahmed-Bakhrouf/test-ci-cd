import { Component } from "@angular/core";

@Component({
  template: `
    <div class="Page">
      <a [routerLink]="['/products']" class="Page-logo">
        <!-- <img src="assets/images/logo-bokay-regular.png" alt="Logo Bokai"> -->
        <img
          src="assets/images/logo-bokay-variantehorizontal.svg"
          alt="Logo Bokai"
        />
      </a>

      <app-layout-navbar title="Nos C.G.V"></app-layout-navbar>

      <div class="ContentCard">
        <h1>Nos C.G.V.</h1>
        <div class="ContentCard-separator"></div>
        <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and...
        </p>
        <h2>Titre H2</h2>
        <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and...
        </p>
        <h3>Titre H3</h3>
        <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and...
        </p>
        <h4>Titre H4</h4>
        <p>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and...
        </p>
        <ul>
          <li>Puce 1</li>
          <li>Puce 2</li>
          <li>Puce 3</li>
        </ul>
        <img
          src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS1sm5jBd4AM2bMZDmPAQijaQoPS37V6AEvCQ&usqp=CAU"
        />
      </div>
    </div>
  `,
})
export class SamplePage {}
