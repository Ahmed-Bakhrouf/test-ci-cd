import { LayoutModule } from '../layout/layout.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SamplePage } from './pages/sample.page';
import { ContentRoutingModule } from './content-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ContentRoutingModule,
    LayoutModule,
  ],
    declarations: [
        /** Pages  */
        SamplePage,

        /** Components  */
    ]
})
export class ContentModule { }
