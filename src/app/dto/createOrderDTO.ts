import { OrderDA } from "../modelsDeliveryApp/orderDA";
import { ProductDA } from "../modelsDeliveryApp/productDA";

export class CreateOrderDTO {
  order: OrderDA;
  // products: ProductQuantity[];
  products: ProductQuantity[];
}

export class ProductQuantity {
  _id?: string;
  product: any;
  quantity: number;
  productTitle: string;
  productPrice?: number;

  constructor(product: ProductDA, quantity: number) {
    this.product = product._id;
    this.quantity = quantity;
    this.productTitle = product.title;
  }
}
