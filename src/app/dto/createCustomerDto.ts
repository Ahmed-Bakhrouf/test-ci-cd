export class CreateCustomerDto {
  mobile_number: string;
  mobile_country_code: string;
  full_mobile_number: string;
  email: string;
  reference: string;
  password: string;
  firstname: string;
  lastname: string;
  address: CustomerAddress;

  constructor(user: any) {
    this.firstname = user.firstname;
    this.lastname = user.lastname;
    this.email = user.email;
    this.mobile_number = user.mobile_number.replace(/ /g, "");
    this.address = user.address;
    this.password = user.password;
    this.mobile_country_code = user.mobile_country_code;
    this.full_mobile_number = user.full_mobile_number
      .replace(/ /g, "")
      .replace("+", "");
  }
}

class CustomerAddress {
  line1: string;
  line2: string;
  postalCode: string;
  town: string;
  country: string;
}
