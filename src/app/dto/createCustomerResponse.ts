import {CustomerDA} from '../modelsDeliveryApp/customerDA';

export class CreateCustomerResponse {
  customer: CustomerDA; // with address
  token: string;
}
