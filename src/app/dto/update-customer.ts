export class UpdateCustomer {
  country?: string;
  email?: string;
  firstname?: string;
  lastname?: string;
  mobile_country_code?: string;
  mobile_number?: string;

  constructor(user: any) {
    this.country = user.country;
    this.firstname = user.firstname;
    this.lastname = user.lastname;
    this.mobile_number = user.mobile_number;
    this.email = user.email;
    this.mobile_country_code = user.mobile_country_code;
  }
}
