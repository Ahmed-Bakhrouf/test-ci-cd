import { Component } from "@angular/core";
import { SidenavService } from "./services/SideNav/sidenavService";
import { Router } from "@angular/router";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  title = "BOKAY";
  constructor(private sidenavService: SidenavService, private router: Router) {}

  getModeMenu() {
    //return this.sidenavService.getModeMenu();
    return "Movile";
  }

  // isOpened() {
  //   return this.sidenavService.isOpened();
  // }

  // setOpened(opened) {
  //   this.sidenavService.setOpened(opened);
  // }
}
