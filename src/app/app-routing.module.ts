import { LayoutComponent as LayoutIntregationComponent } from "./modules/layout/layout.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    component: LayoutIntregationComponent,
    children: [
      {
        path: "products",
        loadChildren: "./modules/products/products.module#ProductsModule",
      },
      { path: "cart", loadChildren: "./modules/cart/cart.module#CartModule" },
      {
        path: "account",
        loadChildren: "./modules/account/account.module#AccountModule",
      },
      {
        path: "merchants",
        loadChildren: "./modules/merchants/merchants.module#MerchantsModule",
      },
      {
        path: "content",
        loadChildren: "./modules/content/content.module#ContentModule",
      },
      { path: "**", redirectTo: "products" },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
