export class ExistsResult<I> {
  exists: boolean;
  total: number;
  items: I[];
}
