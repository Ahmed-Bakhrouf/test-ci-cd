export class PagedResult<I> {
  total: number;
  totalPage: number;
  page: number;
  pageSize: number;
  items: I[];
}
