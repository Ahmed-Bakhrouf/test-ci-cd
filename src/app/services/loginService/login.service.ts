import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { CustomerDA } from "../../modelsDeliveryApp/customerDA";
import { map, switchMap } from "rxjs/operators";
import { environment } from "../../../environments/environment";
import { CustomerAdressDA } from "../../modelsDeliveryApp/customerAdressDA";
import { Router } from "@angular/router";
import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class LoginService {
  public token: string;
  public customer: CustomerDA;

  private customerSubject: BehaviorSubject<CustomerDA> = new BehaviorSubject<
    CustomerDA
  >(null);
  customer$: Observable<CustomerDA> = this.customerSubject.asObservable();

  authenticated$: Observable<boolean> = this.customer$.pipe(
    map((customer) => customer != null)
  );

  private tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(
    ""
  );

  constructor(private http: HttpClient, private router: Router) {
    const customer = JSON.parse(localStorage.getItem("user"));
    const token = localStorage.getItem("token");
    this.customerSubject.next(customer);
    this.tokenSubject.next(token);
  }

  public gettokenValue() {
    return this.tokenSubject.value;
  }

  getCustomerValue() {
    return this.customerSubject.value;
  }
  getUser(): CustomerDA {
    return this.getCustomerValue();
  }

  isAuth() {
    return this.getCustomerValue() != null;
  }

  login(mobileNumber: string, password: string): Observable<CustomerDA> {
    let user: CustomerDA;
    return this.getToken(mobileNumber, password).pipe(
      switchMap(({ token, customer }) => {
        user = customer;
        this.updateToken(token);
        return this.http.get<CustomerAdressDA[]>(
          `${environment.apiUrl}delivery/customer/address?customer=${customer._id}`
        );
      }),
      map((address) => {
        user.address = address;
        this.updateCustomer(user);
        return user;
      })
    );
  }

  logout() {
    this.updateCustomer(null);
    this.updateToken("");
    this.router.navigate(["/products"]);
  }

  getToken(mobileNumber: string, password: string) {
    return this.http.post<ICustomerLoginResponse>(
      `${environment.apiUrl}auth/customer/token`,
      {
        mobileNumber,
        password,
      }
    );
  }

  updateCustomer(customer: CustomerDA) {
    this.customerSubject.next(customer);
    localStorage.setItem("user", JSON.stringify(customer));
  }
  updateToken(token: string) {
    this.tokenSubject.next(token);
    localStorage.setItem("token", token);
  }
}
export interface ICustomerLoginResponse {
  customer: CustomerDA;
  token: string;
}
