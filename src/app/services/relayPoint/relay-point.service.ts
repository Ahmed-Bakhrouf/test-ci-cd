import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { map } from "rxjs/operators";
import { RelayPointDA } from "../../modelsDeliveryApp/relayPointDA";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class RelayPointService {
  constructor(private http: HttpClient) {}

  getRelayPointList(): Observable<RelayPointDA[]> {
    return (
      this.http
        .get<any>(`${environment.apiUrl}delivery/relaypoint`)
        // return this.http.get<any>(`http://localhost:3001/delivery/relaypoint` ) ///read
        .pipe(
          map((q) => {
            return q;
          })
        )
    );
  }
  getPositionFromAdress(adresse): Observable<any> {
    const header = {
      headers: new HttpHeaders().set(`Access-Control-Allow-Origin`, `*`),
    };
    return (
      this.http
        .get<any>(
          `https://maps.googleapis.com/maps/api/geocode/json?address=` +
            adresse +
            `&key=AIzaSyDRz0RDy5V9CC8mVfCOWI04EWmwW_O9W5M`,
          header
        )
        // return this.http.get<any>(`http://localhost:3001/delivery/relaypoint` ) ///read
        .pipe(
          map((q) => {
            return q;
          })
        )
    );
  }
}
