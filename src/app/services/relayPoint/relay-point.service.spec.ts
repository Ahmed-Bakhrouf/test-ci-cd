import { TestBed } from '@angular/core/testing';

import { RelayPointService } from './relay-point.service';

describe('RelayPointService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RelayPointService = TestBed.get(RelayPointService);
    expect(service).toBeTruthy();
  });
});
