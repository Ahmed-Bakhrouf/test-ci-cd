import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {Observable, Subject, Subscription} from "rxjs";
import { Paging } from "src/app/models/paging";
import { ProductPaginData } from "src/app/models/product-pagin-data";
import { ProductDA } from "../../modelsDeliveryApp/productDA";
import { map } from "rxjs/operators";
import { Product } from "../../models/product";
import { environment } from "../../../environments/environment";
import { PagedResult } from "../../models/product-paging";
import {SubOrderDA} from "../../modelsDeliveryApp/subOrderDA";
import {OrderProductDA} from "../../modelsDeliveryApp/orderProductDA";

@Injectable({
  providedIn: "root",
})
export class ProductService {
  url = `${environment.apiUrl}delivery/`;
  public newProductSubject = new Subject<any>();
  public searchProductSubject = new Subject<any>();

  constructor(private http: HttpClient) {}

  getProducts(
    page: number = 0,
    limit: number = 3
  ): Observable<PagedResult<ProductDA>> {
    const url = `${this.url}product?page=${page}&limit=${limit}`;
    return this.http.get<PagedResult<ProductDA>>(url).pipe(
      map((products) => {
        products.items.forEach((q, i, t) => (t[i] = new ProductDA(q)));
        return products;
      })
    );
  }
  getProductsMerchant(
    merchant: String,
    page: number = 0,
    limit: number = 3
  ): Observable<PagedResult<ProductDA>> {
    const url = `${this.url}product?merchant=${merchant}&page=${page}&limit=${limit}`;
    return this.http.get<PagedResult<ProductDA>>(url).pipe(
      map((products) => {
        products.items.forEach((q, i, t) => (t[i] = new ProductDA(q)));
        return products;
      })
    );
  }
  searchForProduct(
    word: any,
    page: number = 0,
    limit: number = 3
  ): Observable<PagedResult<ProductDA>> {
    return this.http
      .get<PagedResult<ProductDA>>(
        `${environment.apiUrl}delivery/product/findlike/` +
          word +
          `/` +
          page +
          `/` +
          limit
      )
      .pipe(
        map((products) => {
          products.items.forEach((q, i, t) => (t[i] = new ProductDA(q)));
          return products;
        })
      );
  }
  filterByCategory(categoryTitle: string, page: number = 0, limit: number = 4) {
    const url =
      `${this.url}product?page=${page}&limit=1000&category=` + categoryTitle;
    return this.http.get<PagedResult<ProductDA>>(url).pipe(
      map((products) => {
        products.items.forEach((q, i, t) => (t[i] = new ProductDA(q)));
        return products;
      })
    );
  }
  applyFilter(
    categoryTitle,
    page: number = 0,
    limit: number = environment.productPerPage
  ) {
    this.filterByCategory(categoryTitle, page, limit).subscribe((data) => {
      this.newProductSubject.next(data);
    });
  }
  applySearch(
    word,
    page: number = 0,
    limit: number = environment.productPerPage
  ) {
    if (word.length < 2) {
      this.getProducts(page, limit).subscribe((result) => {
        this.searchProductSubject.next(result);
      });
    } else {
      this.searchForProduct(word, page, limit).subscribe((data) => {
        this.searchProductSubject.next(data);
      });
    }
  }

  geProductByDepartmentId(paging: Paging): Observable<Product[]> {
    return this.http.post<Product[]>(
      `${this.url}department/getDepartments`,
      paging
    );
  }
  getProductList(paging: Paging): Observable<ProductPaginData> {
    return this.http.post<ProductPaginData>(
      `${this.url}product/getFilteredProducts`,
      { paging }
    );
  }
  getProductDetailsById(productId: number): Observable<Product> {
    return this.http.get<Product>(
      `${this.url}product/getProductDetails?productId=${productId}`
    );
  }
  getProductAPI(productId: string): Observable<PagedResult<ProductDA>> {
    return this.http
      .get<PagedResult<ProductDA>>(
        `${environment.apiUrl}delivery/product?_id=` + productId
      )
      .pipe(
        map((products) => {
          products.items.forEach((q, i, t) => (t[i] = new ProductDA(q)));
          return products;
        })
      );
  }
  getProductBySubcategoryAPI(
    page: number = 0,
    limit: number = environment.productPerPage,
    subCategory: string
  ): Observable<PagedResult<ProductDA>> {
    return this.http
      .get<PagedResult<ProductDA>>(
        `${environment.apiUrl}delivery/product?page=${page}&limit=1000&subcategory=` +
          subCategory
      )

      .pipe(
        map((products) => {
          products.items.forEach((q, i, t) => (t[i] = new ProductDA(q)));
          console.log("inside methode");
          return products;
        })
      );
  }
  getProductByCategoryAPI(
    page: number = 0,
    limit: number = environment.productPerPage,
    category: string
  ): Observable<PagedResult<ProductDA>> {
    return this.http
      .get<PagedResult<ProductDA>>(
        `${environment.apiUrl}delivery/product?page=${page}&limit=${limit}&category=` +
          category
      )
      .pipe(
        map((products) => {
          products.items.forEach((q, i, t) => (t[i] = new ProductDA(q)));
          return products;
        })
      );
  }

  getSubCategoryByIdAPI(subCategoryID) {}
  getProductsMerchantSubCategory(
      merchant: string,
      subCategory: string,
      page: number = 0,
      limit: number = 3
  ): Observable<PagedResult<ProductDA>> {
      const url = `${this.url}product?merchant=${merchant}&subcategory=${subCategory}&page=${page}&limit=${limit}`;
    return this.http.get<PagedResult<ProductDA>>(url).pipe(
      map((products) => {
        products.items.forEach((q, i, t) => (t[i] = new ProductDA(q)));
        return products;
      })
    );
  }

/*  getHomeProducts(
  ): Observable<ProductDA[]> {
    const url = `${this.url}product/homeProducts`;
    return this.http.get<ProductDA[]>(url).pipe(
      map((products) => {
        products.forEach((q, i, t) => (t[i] = new ProductDA(q)));
        return products;
      })
    );
  }*/
}
