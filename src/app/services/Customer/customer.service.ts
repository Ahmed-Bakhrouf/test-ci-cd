import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { environment } from "../../../environments/environment";
import { CustomerDA } from "../../modelsDeliveryApp/customerDA";
import { CreateCustomerDto } from "../../dto/createCustomerDto";
import { CreateCustomerResponse } from "../../dto/createCustomerResponse";
import { CustomerAdressDA } from "../../modelsDeliveryApp/customerAdressDA";
import { UpdateCustomer } from "../../dto/update-customer";
import { map, publishReplay, refCount } from "rxjs/operators";
import { LoginService } from "../loginService/login.service";

import { Country } from "../../modelsDeliveryApp/country";

@Injectable({
  providedIn: "root",
})
export class CustomerService {
  constructor(private http: HttpClient, private loginService: LoginService) {}

  async create(customer: CreateCustomerDto) {
    // TODO refactor
    // create rest api that take CreateCustomerDto and return CreateCustomerResponse
    const token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1ODk5NzMwOTR9.SVRKn3K-wzv6-SZlJtnjw5a0klJS2oCySMvWYApSdM4";
    const header = {
      headers: new HttpHeaders().set("Authorization", `Bearer ${token}`),
    };
    const response = new CreateCustomerResponse();
    // Create user
    const user = await this.http
      .post(`${environment.apiUrl}delivery/customer/create`, customer, header)
      .toPromise()
      .catch((message) => {
        console.error(message);
      });
    response.customer = user as CustomerDA;
    // get token
    const auth: any =
      user &&
      (await this.http
        .post(`${environment.apiUrl}auth/customer/token`, {
          mobileNumber: customer.mobile_number,
          password: customer.password,
        })
        .toPromise());
    if (auth) {
      response.token = auth && auth.token;
      localStorage.setItem("token", response.token);
    }
    // create address
    const address =
      auth &&
      (await this.http
        .post(`${environment.apiUrl}delivery/customer/address/create`, {
          customer: response.customer._id,
          status: "ACTIVE",
          ...customer.address,
        })
        .toPromise()
        .catch((message) => {
          console.error(message);
        }));
    if (address && response.customer) {
      response.customer.address = [address as CustomerAdressDA];
    }
    if (user && auth && address) {
      return response;
    }
  }

  searchByMobileNumber(mobileNumber: string): Observable<CustomerDA> {
    const token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1ODk5NzMwOTR9.SVRKn3K-wzv6-SZlJtnjw5a0klJS2oCySMvWYApSdM4";
    const header = {
      headers: new HttpHeaders().set("Authorization", `Bearer ${token}`),
    };
    // TODO remeove headers and expose api
    return this.http.get<CustomerDA>(
      `${environment.apiUrl}delivery/customer?mobile_number=${mobileNumber}`,
      header
    );
  }

  update(
    customer: UpdateCustomer,
    customerId: string
  ): Observable<UpdateCustomer> {
    return this.http.put<UpdateCustomer>(
      `${environment.apiUrl}delivery/customer/update/` + customerId,
      customer
    );
  }
  updateAdress(adress: any, adressId: string) {
    return this.http.put(
      `${environment.apiUrl}delivery/customer/address/update/` + adressId,
      adress
    );
  }
  getAdresse(customerID: string) {
    return this.http.get<CustomerAdressDA[]>(
      `${environment.apiUrl}delivery/customer/address?customer=${customerID}`
    );
  }
  getAdresseById(addressId: string) {
    return this.http.get<CustomerAdressDA[]>(
      `${environment.apiUrl}delivery/customer/address?_id=${addressId}`
    );
  }
  getCustomer(customerId: string): Observable<CustomerDA> {
    return this.http
      .get<CustomerDA>(
        `${environment.apiUrl}delivery/customer?_id=` + customerId
      )
      .pipe(
        map((q) => {
          return new CustomerDA(q[0]);
        })
      );
  }
  /*  createAddresse(adresse) {
    const address = this.http.post(`${environment.apiUrl}delivery/customer/address/create`,
      {
        customer: this.loginService.getUser()._id,
        status: 'ACTIVE',
        ...adresse
      }).toPromise().catch(message => {
      console.error(message);
      this.toaster.error(`${message.message}`, `${message.name}`);
    });
    if (address) {
      response.customer.address = [address as CustomerAdressDA];
    }
  }*/

  createAddresse(adresse: CustomerAdressDA) {
    return this.http.post<any>(
      `${environment.apiUrl}delivery/customer/address/create`,
      adresse
    );
  }
  getCountry(): Observable<Country[]> {
    return this.http
      .get<Country[]>(`${environment.apiUrl}delivery/country`)
      .pipe(
        map((data) => data),
        publishReplay(1),
        refCount()
      );
  }

  verifyUniqueness(mobile_number: string) {
    const token =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1ODk5NzMwOTR9.SVRKn3K-wzv6-SZlJtnjw5a0klJS2oCySMvWYApSdM4";
    const options = {
      headers: new HttpHeaders().set("Authorization", `Bearer ${token}`),
    };

    // return of({
    //   exists: false,
    //   total: 0,
    //   items: [],
    // });
    // return of({
    //   exists: true,
    //   total: 0,
    //   items: [],
    // });

    return this.http.get<any>(
      `${environment.apiUrl}delivery/customer/uniqness?mobile_number=${mobile_number}`,
      options
    );
  }
  updatePassword(customerId: string, password: string) {
    return this.http.post<CustomerDA>(
      `${environment.apiUrl}delivery/customer/password/` + customerId,
      { password }
    );
  }
}
