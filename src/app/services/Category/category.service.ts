import { Injectable, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, BehaviorSubject } from "rxjs";
import { Category } from "src/app/models/Category";
import { environment } from "../../../environments/environment";
import { CategoryDA } from "../../modelsDeliveryApp/categoryDA";
import { FamilyProduct } from "../../modelsDeliveryApp/FamilyProduct";
import { take, map, refCount, publishReplay } from "rxjs/operators";
import { SubCategory } from "src/app/modelsDeliveryApp/SubCategory";

@Injectable({
  providedIn: "root",
})
export class CategoryService implements OnInit {
  url = `${environment.apiUrl}delivery/`;
  categoryList: CategoryDA[];
  familyProducts: FamilyProduct[];
  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.getFamilies().pipe(take(1)).subscribe();
  }

  getCategories(): Observable<CategoryDA[]> {
    return this.http.get<CategoryDA[]>(
      `${environment.apiUrl}delivery/Category`
    );
  }

  getSubCategories(id): Observable<any[]> {
    return this.http.get<any[]>(
      `${environment.apiUrl}delivery/Category/sub-category?_id=${id}`
    );
  }

  getFamilyById(familyId: string) {
    return this.getFamilies().pipe(
      map((family) => {
        return family.find((fam) => fam._id == familyId);
      })
    );
  }

  getFamilies() {
    return this.http.get<Family[]>("assets/" + environment.categories).pipe(
      map((families) =>
        families.map((family) => ({
          ...family,
          categories: family.categories.filter((c) => c.title != null),
        }))
      ),
      publishReplay(1),
      refCount()
    );
  }

  getAllCategories() {
    return this.getFamilies().pipe(
      map((families) => {
        let cat = [];
        families.forEach((fam) => {
          cat = [...cat, ...fam.categories];
        });
        return cat;
      })
    );
  }

  getCAtegoriesByFamilyID(familyID: string) {
    return this.getFamilies().pipe(
      map((families) => {
        let cat = [];
        let family = families.find((fam) => fam._id == familyID);
        cat = [...cat, ...family.categories];
        return cat;
      })
    );
  }

  getCategoryById(categoryId) {
    return this.getFamilies().pipe(
      map((families) => {
        let category;
        families.forEach((fam) => {
          let cat = fam.categories.find((c) => c._id == categoryId);
          if (cat) category = cat;
        });
        return category;
      })
    );
  }

  getSubCategoriesByCatergoryID(catgoryID): Observable<any[]> {
    return this.http.get<any[]>(
      `${environment.apiUrl}delivery/Category/sub-category?category=${catgoryID}`
    );
  }
  fillFamilyProducts(): FamilyProduct[] {
    this.familyProducts = [];

    this.getCategories().subscribe((data) => {
      this.categoryList = data;
      const f1 = new FamilyProduct();
      f1.name = "Alimentation";
      f1.categories = this.removeDuplicates(
        this.categoryList.filter(
          (item) => environment.Alimentation.indexOf(item.title) !== -1
        )
      );
      const f2 = new FamilyProduct();
      f2.name = "Santé & Bien être";
      f2.categories = this.removeDuplicates(
        this.categoryList.filter(
          (item) => environment.Santé.indexOf(item.title) !== -1
        )
      );
      const f3 = new FamilyProduct();
      f3.name = "Maison";
      f3.categories = this.removeDuplicates(
        this.categoryList.filter(
          (item) => environment.Maison.indexOf(item.title) !== -1
        )
      );
      const f4 = new FamilyProduct();
      f4.name = "High-Tech";
      f4.categories = this.removeDuplicates(
        this.categoryList.filter(
          (item) => environment.HighTech.indexOf(item.title) !== -1
        )
      );
      this.familyProducts.push(f1, f2, f3, f4);
    });
    return this.familyProducts;
  }
  removeDuplicates(categoryList: CategoryDA[]) {
    const result = categoryList.reduce((unique, o) => {
      if (!unique.some((obj) => obj.title === o.title)) {
        unique.push(o);
      }
      return unique;
    }, []);
    return result;
  }

  getSubCategoriesbyMerchant(merchantID): Observable<SubCategory[]> {
    return this.http.get<SubCategory[]>(
      `${environment.apiUrl}delivery/category/sub-category/` + merchantID
    );
  }
}

export interface Family {
  _id: string;
  title: string;
  categories: any[];
}
