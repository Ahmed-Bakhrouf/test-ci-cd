import { TestBed } from '@angular/core/testing';

import { DeliveryMethodService } from './delivery-method.service';

describe('DeliveryMethodService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DeliveryMethodService = TestBed.get(DeliveryMethodService);
    expect(service).toBeTruthy();
  });
});
