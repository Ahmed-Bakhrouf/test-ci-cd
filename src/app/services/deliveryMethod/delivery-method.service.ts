import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { map } from "rxjs/operators";
import { DeliveryMethodDA } from "../../modelsDeliveryApp/deliveryMethodDA";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class DeliveryMethodService {
  constructor(private http: HttpClient) {}

  getDeliveryMethodList(): Observable<DeliveryMethodDA[]> {
    return this.http
      .get<any[]>(`${environment.apiUrl}delivery/deliveryMode`)
      .pipe(
        map((deliveryModes) => {
          return deliveryModes
            .map((mode) => ({
              ...mode,
              price: mode.price.$numberDecimal,
            }))
            .sort(
              (a, b) =>
                a.reference.split("_").pop() - b.reference.split("_").pop()
            );
        })
      );
  }
}
