import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import {LoginService} from '../loginService/login.service';

@Injectable()
export class AuthService implements CanActivate {

  constructor(private loginService: LoginService, private router: Router) {}

  canActivate(): boolean {
    if (!this.loginService.isAuth()) {
      this.router.navigate(['login'], {queryParams: {returnUrl: location.pathname + location.search}});
      return false;
    }
    return true;
  }
  getToken() {
    return localStorage.getItem('token');
  }


}
