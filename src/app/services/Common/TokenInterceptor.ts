import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from "@angular/common/http";
import { AuthService } from "./AuthService";
import { LoginService } from "../loginService/login.service";
import { Observable } from "rxjs";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService, private authService: LoginService) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // We retrieve the token, if any
    const token = this.authService.gettokenValue();
    let headers = request.headers;
    if (token) {
      // If we have a token, we append it to our new headers
      headers = headers.append("Authorization", `Bearer ${token}`);
    }
    const authReq = request.clone({ headers: headers });
    return next.handle(authReq);
  }
}
