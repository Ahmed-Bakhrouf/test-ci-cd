import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { CartProductDA } from "../../modelsDeliveryApp/CartProductDA";
import { map } from "rxjs/operators";
import { ProductDA } from "src/app/modelsDeliveryApp/productDA";

@Injectable({
  providedIn: "root",
})
export class DataService {
  private message = new BehaviorSubject("default");
  currentMessage = this.message.asObservable();
  private ItemCount = new BehaviorSubject(0);
  count: Observable<number> = this.ItemCount.asObservable();
  private shoppingCart = new BehaviorSubject<CartProductDA[]>([]);
  cart = this.shoppingCart.asObservable();
  cartProductNumber$ = this.cart.pipe(map((el) => el.length));
  cartPrice$: Observable<number> = this.cart.pipe(
    map((productList) => {
      let price = 0;
      productList.forEach(
        (item) => (price += parseFloat(item.price.toFixed(10)) * item.Quantity)
      );
      return parseFloat(price.toFixed(10));
    })
  );
  constructor() {
    const isEmptyCart: boolean = localStorage.getItem("Cart") == null;
    this.updateCartItemCount(
      isEmptyCart ? 0 : JSON.parse(localStorage.getItem("Cart")).length
    );
    this.updateShoppingCart(
      isEmptyCart ? [] : JSON.parse(localStorage.getItem("Cart"))
    );
  }

  getCartValue() {
    return this.shoppingCart.value;
  }

  changeMessage(message: string) {
    this.message.next(message);
  }

  // TODO to delete after updating old parts
  initCart() {
    this.shoppingCart = new BehaviorSubject([]);
    this.ItemCount = new BehaviorSubject(0);

    this.updateCartItemCount(0);
    this.updateShoppingCart([]);
    localStorage.removeItem("Cart");
  }

  updateCartItemCount(count: number) {
    this.ItemCount.next(count);
  }

  updateShoppingCart(cartItems: CartProductDA[]) {
    this.shoppingCart.next(cartItems);
    localStorage.setItem("Cart", JSON.stringify(cartItems));
  }

  getShoppingCart() {
    return this.shoppingCart.value != null
      ? this.shoppingCart.value
      : JSON.parse(localStorage.getItem("Cart"));
  }

  addProductToCart(product: ProductDA) {
    const productCard = new CartProductDA(product);
    productCard.Quantity = 1;

    let cart: CartProductDA[] = this.getCartValue();
    if (!cart) {
      cart = [];
      cart.push(productCard);
    } else {
      const currentProduct = cart.filter((a) => a._id === productCard._id);
      if (currentProduct.length > 0) {
        currentProduct[0].Quantity = currentProduct[0].Quantity + 1;
      } else {
        cart.push(productCard);
      }
    }
    this.updateShoppingCart(cart);
    localStorage.setItem("Cart", JSON.stringify(cart));
  }
  updateQuantity(type, productId) {
    let cart = this.getCartValue();
    let index = cart.findIndex((prod) => prod._id == productId);
    let prod = cart[index];
    if (index > -1 && type == 1) prod.Quantity++;
    else if (index > -1) prod.Quantity--;
    cart[index] = prod;
    this.updateShoppingCart(cart);
  }
  removeProductsFromCart(productId: string) {
    let cart = this.getCartValue().filter((prod) => prod._id !== productId);
    this.updateShoppingCart(cart);
  }
  emptyCart() {
    this.updateShoppingCart([]);
  }
}
