import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonDataService {

  // TODO define common shared data and fetch it dynamically
  private currency = 'EUR';
  private deliveryPrice = 20;

  constructor() {
  }

  public getCurrency() {
    return this.currency;
  }

  public getDeliveryPrice() {
    return this.deliveryPrice;
  }


}
