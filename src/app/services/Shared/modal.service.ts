import { Injectable, Type, ComponentFactoryResolver, ViewContainerRef } from '@angular/core';
import { BroadcasterService } from './broadcaster.service';
import { SelectModalComponent } from 'src/app/modules/layout/select-modal.component';
import { AlertModalComponent } from 'src/app/modules/layout/alert-modal.component';

export interface SelectOption {
    label: string;
    value: string;
}

@Injectable()
export class ModalService {

    viewContainerRef: ViewContainerRef;

    constructor(
        private broadcaster: BroadcasterService,
        private componentFactoryResolver: ComponentFactoryResolver
    ) { }

    open(component: Type<any>, attrs?: any): any {
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
        this.viewContainerRef.clear();

        const componentRef = this.viewContainerRef.createComponent(componentFactory);

        for (const attr in attrs) {
            if (attrs.hasOwnProperty(attr)) {
                componentRef.instance[attr] = attrs[attr];
            }
        }

        this.broadcaster.broadcast('disableScroll');

        return componentRef.instance;
    }

    async select(options: SelectOption[], selected: SelectOption, title?: string): Promise<SelectOption> {
        const instance: SelectModalComponent = this.open(SelectModalComponent, { title, options, selected });
        return new Promise(resolve => {
            instance.onSubmit.subscribe((value: SelectOption) => {
                instance.close();
                resolve(value);
            });
            instance.onCancel.subscribe(() => {
                console.log('onCancel');
                instance.close();
                resolve(null);
            });
        });
    }

    async alert(title: string, text: string, bigButtonText: string, cancelButtonText: string, icon: string): Promise<string> {
        return new Promise(resolve => {
            const instance: AlertModalComponent = this.open(AlertModalComponent, { title, text, bigButtonText, cancelButtonText, icon });
            instance.onBigButtonClick.subscribe((value: SelectOption) => {
                instance.close();
                resolve('bigButton');
            });
            instance.onCancel.subscribe((value: SelectOption) => {
                instance.close();
                resolve('cancelButton');
            });
        });
    }

    close(): void {
        this.broadcaster.broadcast('enableScroll');
    }

    setViewContainerRef(viewContainerRef: ViewContainerRef) {
        this.viewContainerRef = viewContainerRef;
    }

}
