import { Injectable } from "@angular/core";
import { environment } from "../../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { SearchResultDto } from "src/app/modelsDeliveryApp/search-resultDA";
import { map } from "rxjs/operators";
import { ExistsResult } from "src/app/models/existResultDA";

@Injectable({
  providedIn: "root",
})
export class SearchService {
  url = `${environment.apiUrl}search/`;

  constructor(private http: HttpClient) {}

  searchByKeyword(keyword): Observable<ExistsResult<SearchResultDto>> {
    return this.http
      .get<ExistsResult<SearchResultDto>>(`${this.url}${keyword}`)
      .pipe(
        map((result) => {
          console.log(result);
          return result;
        })
      );
  }
}
