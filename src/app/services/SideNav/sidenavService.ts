import {Router} from "@angular/router";

export class SidenavService {

  private opened = false;
  private mode = '';

  constructor(router: Router) {

  }

/*  getModeMenu() {
    let mode = 'side';
    const portableScreen = this.responsiveService.isOnTabletScreen();

    mode = portableScreen ? 'over' : 'side';

    if (mode !== this.mode) {
      this.mode = mode;

      this.opened = !portableScreen;
    }

    return this.mode;
  }*/

  isOpened() {
    return  this.opened;
  }

  setOpened(opened) {
    this.opened = opened;
  }

  toggleMenu() {
    this.opened = !this.opened;
  }

}
