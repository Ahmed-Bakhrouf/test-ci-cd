import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {MerchantDA} from '../../modelsDeliveryApp/merchantDA';

@Injectable({
  providedIn: 'root'
})
export class MerchantService {

  public searchMerchantSubject = new Subject<any>()

  constructor(private http: HttpClient) { }

  getMerchandAPI(merchandID: string): Observable<MerchantDA> {
    return this.http.get<any>(`${environment.apiUrl}delivery/merchant/read?_id=` + merchandID)
      .pipe(map(q => {
        return new MerchantDA(q[0]);
      }));
  }

  getMerchandList(): Observable<MerchantDA[]> {
    return this.http.get<any>(`${environment.apiUrl}delivery/merchant/read` )
    //return this.http.get<any>(`http://localhost:3001/delivery/merchant/read` )
      .pipe(map(q => {
        return q;
      }));
  }

  searchForMerchant(word: any, page: number = 0, limit: number = 3): Observable<MerchantDA[]>{
    return this.http.get<MerchantDA[]>(`${environment.apiUrl}delivery/merchant/findlike/` + word )
    //return this.http.get<MerchantDA[]>(`http://localhost:3001/delivery/merchant/findlike/` + word )
      .pipe(map(merchants => {
        return merchants;
      }));
  }


  applySearch(word , page: number = 0, limit: number = environment.productPerPage){
    if (word.length < 2) {
      this.getMerchandList().subscribe(result =>{
        this.searchMerchantSubject.next(result);
    });
    } else {
      this.searchForMerchant(word, page , limit ).subscribe(data =>{
        this.searchMerchantSubject.next(data);
      });
    }
  }



}
