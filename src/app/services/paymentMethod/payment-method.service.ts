import { Injectable, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../../environments/environment";
import { map } from "rxjs/operators";
import { PaymentMethodDA } from "../../modelsDeliveryApp/paymentMethodDA";
import { LoginService } from "../loginService/login.service";
// stripe js
declare var Stripe: any;

@Injectable({
  providedIn: "root",
})
export class PaymentMethodService implements OnInit {
  constructor(private http: HttpClient, private loginService: LoginService) {
    this.stripe = Stripe(this.stripePk);
  }

  private stripePk = environment.stripePk;

  private stripe: any;

  async ngOnInit() {}

  getPaymentMethodsAPI(): Observable<PaymentMethodDA[]> {
    return this.http
      .get<PaymentMethodDA[]>(`${environment.apiUrl}delivery/payment`)
      .pipe(
        map((list) => {
          return list.map((item) => new PaymentMethodDA(item));
        })
      );
  }

  createPayment(amount: number) {
    return this.http.post<{ code: string }>(
      `${environment.apiUrl}delivery/payment/create`,
      { amount: amount * 100 }
    );
  }

  async confirmPayment(card: any, clientSecret: string) {
    const { firstname, lastname, mobile_number } = this.loginService.getUser();
    return await this.stripe.confirmCardPayment(clientSecret, {
      payment_method: {
        card: card,
        billing_details: {
          name: `${firstname} ${lastname}`,
          phone: mobile_number.toString(),
        },
      },
    });
  }

  async createCard(cardStyle: any) {
    const elements = this.stripe.elements();
    return elements.create("card", { style: cardStyle });
  }
}
