import { Injectable } from "@angular/core";
import { OrderService } from "./Order/order.service";
import { AddressPipe } from "../pipes/address.pipe";
import { CreateOrderDTO, ProductQuantity } from "../dto";
import { CartProductDA } from "../modelsDeliveryApp/CartProductDA";
import { CustomerAdressDA } from "../modelsDeliveryApp/customerAdressDA";
import { CustomerDA } from "../modelsDeliveryApp/customerDA";
import { RelayPointDA } from "../modelsDeliveryApp/relayPointDA";
import { DeliveryMethodDA } from "../modelsDeliveryApp/deliveryMethodDA";
import { PaymentMethodDA } from "../modelsDeliveryApp/paymentMethodDA";
import { SubOrderDA } from "../modelsDeliveryApp/subOrderDA";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { OrderDA } from "../modelsDeliveryApp/orderDA";
import { DataService } from "./Shared/data.service";
import { map } from "rxjs/operators";
import { MerchantDA } from "../modelsDeliveryApp/merchantDA";

@Injectable({
  providedIn: "root",
})
export class CartService {
  selectedDeliveryMethod: DeliveryMethodDA = null;
  selectedRelayPoint: RelayPointDA = null;

  private deliveryPrice: BehaviorSubject<number> = new BehaviorSubject<number>(
    0
  );
  deliveryPrice$: Observable<number> = this.deliveryPrice.asObservable();

  private orderSubject: BehaviorSubject<OrderDA> = new BehaviorSubject<OrderDA>(
    new OrderDA()
  );
  public order$: Observable<OrderDA> = this.orderSubject.asObservable();

  code: string;

  constructor(
    private orderService: OrderService,
    private dataService: DataService
  ) {}

  getOrderValue() {
    return this.orderSubject.value;
  }

  getDeliveryPriceValue() {
    return this.deliveryPrice.value;
  }

  setOrderCustomer(customer: string) {
    this.orderSubject.next({ ...this.getOrderValue(), customer });
  }

  setOrderAddress(delivery_adress: string) {
    this.orderSubject.next({ ...this.getOrderValue(), delivery_adress });
  }

  setOrderDeliveryMode(delivery_mode: DeliveryMethodDA) {
    this.deliveryPrice.next(+delivery_mode.price);
    this.orderSubject.next({
      ...this.getOrderValue(),
      delivery_mode: delivery_mode._id,
      total_delivery_price: +delivery_mode.price,
      payment_method: "5f2ab0f79a8873aee8f4fe04",
      origin: "BOKAY_MARKETPLACE",
    });
  }

  setOrderPaymentMethod(payment_method: string) {
    this.orderSubject.next({ ...this.getOrderValue(), payment_method });
  }

  passOrder() {
    const products = this.dataService.getShoppingCart();

    const order = this.getOrderValue();

    order.total_product_price = this.calcTotalProductPrice(products);
    order.total_price =
      order.total_product_price + this.getDeliveryPriceValue();
    order.total_order_price = order.total_price;

    order.sub_order = this.productsToSubOrders(products);
    this.orderSubject.next(order);
    const orderDTO = new CreateOrderDTO();
    orderDTO.order = order;
    orderDTO.products = [];

    return this.orderService.saveOrderAPI(order).pipe(
      map((clientSecret) => {
        if (clientSecret.code.includes("pi_")) {
          this.code = clientSecret.code;
        }
        return clientSecret;
      })
    );
  }

  resetOrder() {
    this.orderSubject.next(null);
  }

  productsToSubOrders(products: CartProductDA[]): SubOrderDA[] {
    let subOrders: SubOrderDA[] = [];
    products.forEach((product) => {
      const index = subOrders.findIndex(
        (element) => element.merchant === product.merchant
      );
      const productQuantity = new ProductQuantity(product, product.Quantity);
      // if the product is in new merchant
      if (index < 0) {
        const subOrder = new SubOrderDA();
        subOrder.merchant = (product.merchant as MerchantDA)._id;
        subOrder.subOrderProduct = [productQuantity];
        subOrder.total_delivery_price = 0;

        const price = this.calcTotalProductPrice([product]);

        subOrder.total_price = price;
        subOrder.total_product_price = price;
        subOrder.total_sub_order_price = price;

        subOrders.push(subOrder);
      }
      // if the new product is in an already created suborder
      else {
        subOrders[index].subOrderProduct.push(productQuantity);

        let price = subOrders[index].total_price;

        price += this.calcTotalProductPrice([product]);
        price = parseFloat(price.toFixed(10));
        subOrders[index].total_price = price;
        subOrders[index].total_product_price = price;
        subOrders[index].total_sub_order_price = price;
      }
    });
    return subOrders;
  }
  calcTotalProductPrice(products: CartProductDA[]) {
    let price = 0;
    products.forEach(
      (item) => (price += parseFloat(item.price.toFixed(10)) * item.Quantity)
    );
    return parseFloat(price.toFixed(10));
  }
}
