import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { OrderDA } from "../../modelsDeliveryApp/orderDA";
import { CustomerAdressDA } from "../../modelsDeliveryApp/customerAdressDA";
import { environment } from "../../../environments/environment";
import { map } from "rxjs/operators";
import { CustomerDA } from "../../modelsDeliveryApp/customerDA";
import { CreateOrderDTO } from "../../dto";
import { OrderProductDA } from "../../modelsDeliveryApp/orderProductDA";
import { SubOrderDA } from "../../modelsDeliveryApp/subOrderDA";
import { LoginService } from "../loginService/login.service";
import { SubOrderHistoryDA } from "src/app/modelsDeliveryApp/subOrderHistoryDA";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class OrderService {
  constructor(private http: HttpClient, private loginService: LoginService) {}

  setOrder(order: OrderDA) {
    localStorage.setItem("order", JSON.stringify(order));
  }

  getOrder(): OrderDA {
    return JSON.parse(localStorage.getItem("order"));
  }

  createOrUpdateOrder(address?: CustomerAdressDA, customer?: CustomerDA) {
    const order = this.getOrder() || new OrderDA();
    if (address) {
      localStorage.setItem("address", JSON.stringify(address));
      order.delivery_adress = address.line1 || address.line2;
      order.delivery_latt = address.latt;
      order.delivery_long = address.long;
    }
    /*  if (customer) {
      order.customer._id = this.loginService.getUser()._id;
    }*/
    this.setOrder(order);
    return order;
  }

  getAddress(): CustomerAdressDA {
    return JSON.parse(localStorage.getItem("address"));
  }

  // saveOrderAPI(order: CreateOrderDTO): Observable<OrderDA> {
  saveOrderAPI(order: OrderDA): Observable<PaymentIntentResponse> {
    return this.http.post<PaymentIntentResponse>(
      `${environment.apiUrl}delivery/order/create`,
      order
    );
  }
  getOrderAPI(orderQuery: {
    orderId?: string;
    reference?: string;
    customerId?: string;
  }): Observable<OrderDA[]> {
    let query = "delivery/order";
    query += orderQuery.orderId ? "?_id=" + orderQuery.orderId : "";
    query += orderQuery.customerId ? "?customer=" + orderQuery.customerId : "";
    query += orderQuery.reference ? "?reference" + orderQuery.reference : "";
    console.log("order qery", query);
    return this.http.get<any>(`${environment.apiUrl}` + query).pipe(
      map((list) => {
        return list.map((order) => new OrderDA(order));
      })
    );
  }
  /*  getOrdersAPI(): Observable<OrderDA[]> {
    return this.http.get<OrderDA[]>(`${environment.apiUrl}delivery/order`)
      .pipe(map(orders => {
        orders.forEach((q, i, t) => t[i] = new OrderDA(q));
        return orders;
      }));
  }*/
  getSubOrdersAPI(): Observable<SubOrderDA[]> {
    return this.http
      .get<SubOrderDA[]>(`${environment.apiUrl}delivery/sub-order`)
      .pipe(
        map((subOrders) => {
          subOrders.forEach((q, i, t) => (t[i] = new SubOrderDA(q)));
          console.log(subOrders);
          return subOrders;
        })
      );
  }

  getSubOrderAPI(id: string): Observable<SubOrderDA> {
    return this.http
      .get<SubOrderDA>(`${environment.apiUrl}delivery/sub-order/${id}`)
      .pipe(
        map((subOrder) => {
          return new SubOrderDA(subOrder);
        })
      );
  }

  getSubOrderHistoryAPI(id: string): Observable<SubOrderHistoryDA> {
    return this.http
      .get<SubOrderHistoryDA>(
        `${environment.apiUrl}delivery/sub-order/${id}/history`
      )
      .pipe(
        map((subOrderHistory) => {
          return new SubOrderHistoryDA(subOrderHistory);
        })
      );
  }

  getOrderProducts(orderId: string): Observable<OrderProductDA[]> {
    return this.http
      .get<OrderProductDA[]>(
        `${environment.apiUrl}delivery/order/products?order=` + orderId
      )
      .pipe(
        map((ordersProdcuts) => {
          ordersProdcuts.forEach((q, i, t) => (t[i] = new OrderProductDA(q)));
          return ordersProdcuts;
        })
      );
  }
  cancelOrder(order: OrderDA): Observable<OrderDA> {
    console.log(order._id);
    return this.http
      .put<OrderDA>(
        `${environment.apiUrl}delivery/order/cancel/` + order._id,
        order
      )
      .pipe(
        map((result) => {
          return new OrderDA(result);
        })
      );
  }
}

interface PaymentIntentResponse {
  idPayment: string;
  code: string;
}
