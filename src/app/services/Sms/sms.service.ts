import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { of } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class SmsService {
  // twilio config
  baseUrl: string = environment.twilio.baseUrl;
  sid = environment.twilio.sid;
  authToken = environment.twilio.authToken;
  serverNumber = environment.twilio.serverNumber;

  //twilio auth header to append to every request
  header: HttpHeaders;
  constructor(private http: HttpClient) {
    this.header = new HttpHeaders({
      Authorization: "Basic " + btoa(this.sid + ":" + this.authToken),
    });
  }

  sendVerifCode(phoneNumber: string, verifyNumber: number) {
    return this.sendSms(
      phoneNumber,
      "Your Account verification code is " + verifyNumber
    );
  }

  sendPassword(phoneNumber: string, password: string) {
    return this.sendSms(
      phoneNumber,
      "Votre mot de passe sur BOKAY est: " + password
    );
  }
  generatePassword(numberChar: number) {
    let password = "";
    let str =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz0123456789@#$";

    for (let i = 1; i <= numberChar; i++) {
      let char = Math.floor(Math.random() * str.length + 1);

      password += str.charAt(char);
    }
    return password;
  }

  sendSms(phoneNumber: string, smsBody: string) {
    let formData = new FormData();
    formData.append("From", encodeURI(this.serverNumber));
    formData.append("To", encodeURI(phoneNumber));
    formData.append("Body", smsBody);

    return this.http.post<any>(
      this.baseUrl + "/Accounts/" + this.sid + "/Messages.json",
      formData,
      { headers: this.header }
    );
  }
}
