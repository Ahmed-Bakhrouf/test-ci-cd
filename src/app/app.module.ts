import { LayoutModule } from "./modules/layout/layout.module";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { MatInputModule } from "@angular/material/input";

import { MatChipsModule } from "@angular/material/chips";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
// tslint:disable-next-line: max-line-length
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";
import { TokenInterceptor } from "./services/Common/TokenInterceptor";
import { AuthService } from "./services/Common/AuthService";

import { MatCardModule } from "@angular/material/card";

import {
  MatButtonToggleModule,
  MatDialogModule,
  MatSelectModule,
  MatSidenavModule,
  MatStepperModule,
  MatProgressSpinnerModule,
  MatAutocompleteModule,
} from "@angular/material";
import { MatRadioModule } from "@angular/material/radio";
import { DatePipe } from "@angular/common";

import { MatTabsModule } from "@angular/material/tabs";

import { DeviceDetectorModule } from "ngx-device-detector";

import { SidenavService } from "./services/SideNav/sidenavService";
import { SmsService } from "./services/Sms/sms.service";

import { BroadcasterService } from "./services/Shared/broadcaster.service";
import { ModalService } from "./services/Shared/modal.service";
import { SharedModule } from "./modules/shared/shared.module";

@NgModule({
  declarations: [
    AppComponent,
    // StripeCardComponent,
  ],
  imports: [
    BrowserModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,

    MatTabsModule,

    MatStepperModule,
    MatRadioModule,
    AppRoutingModule,
    MatChipsModule,

    HttpClientModule,
    DeviceDetectorModule.forRoot(),
    FormsModule,
    MatInputModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,

    MatCardModule,

    MatButtonToggleModule,
    MatSidenavModule,
    MatSelectModule,
    MatProgressSpinnerModule,

    LayoutModule,
    MatAutocompleteModule,
    SharedModule,
  ],
  providers: [
    AuthService,
    SidenavService,
    BroadcasterService,
    ModalService,
    DatePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
    SmsService,
  ],
  entryComponents: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
