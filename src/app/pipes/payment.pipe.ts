import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "payment",
})
export class PaymentPipe implements PipeTransform {
  transform(paymentMethodTitle: string): any {
    if (paymentMethodTitle == "CREDITCARD") return "Carte bancaire";
    else if (paymentMethodTitle == "PAYPAL") return "Paypal (prochainement)";
    else if (!paymentMethodTitle) return "";
    else return paymentMethodTitle;
  }
}
