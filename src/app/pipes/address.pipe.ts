import { Pipe, PipeTransform } from "@angular/core";
import { CustomerAdressDA } from "../modelsDeliveryApp/customerAdressDA";

@Pipe({
  name: "address",
})
export class AddressPipe implements PipeTransform {
  constructor() {}
  transform(address: CustomerAdressDA, line?: number) {
    if (line && line == 1)
      return address && `${address.line1}, ${address.line2 && address.line2}`;
    if (line && line == 2)
      return (
        address &&
        `${address.postalCode} ${address.town}, ${address.country.name}`
      );
    if (!line)
      return (
        address &&
        `${address.line1}, ${address.line2 && address.line2 + ", "}${
          address.postalCode
        } ${address.town}, ${address.country.name}`
      );
    // else {
    //   return (
    //     address &&
    //     `${address.line1} \n ${address.line2 && address.line2 + ""} \n${
    //       address.postalCode
    //     } ${address.town}, ${address.country.name}`
    //   );
    // }
  }
}
