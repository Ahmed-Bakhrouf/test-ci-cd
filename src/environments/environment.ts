export const environment = {
  production: false,
  apiUrl: "http://51.38.168.26:3001/",
  //apiUrl: "http://localhost:3001/",
  productPerPage: 12,
  categories: "categories.json",
  Alimentation: [
    "Fruits",
    "Légumes",
    "Boissons",
    "Crémerie",
    "Épicerie Salée",
    "Épicerie Sucrée",
    "Boulangerie",
    "Boucherie",
    "Fruits de mer",
  ],
  Santé: ["Hygiène", "Beauté"],
  Maison: ["Éclairage", "Cuisine", "Nettoyage", "Linge"],
  HighTech: ["Ordinateurs", "Accessoires"],

  twilio: {
    baseUrl: "https://api.twilio.com/2010-04-01",
    sid: "AC3ce9b4f23ddb9e7002056b089849428a",
    authToken: "7baf7cedc523436d49882663261dd555",
    serverNumber: "+33644605449",
  },
  stripePk:
    "pk_test_51GvNnBLYjletbWeuCCNzbWds0NEAG3Q9x7UZIkZrmsjcQMEEICZANpxovYWK25HB5e4C1kGcZFTAP081bogsw1Qw00p9p5gmMu",
};
